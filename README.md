#ECharts - C#类库

ECharts - C#类库 
    [百度Echarts，数据可视化][1]是一款开源、功能强大的数据可视化产品,紧跟着大数据时代的步伐!
    无意中发现的，感觉不错，就网上学习了下，发现网上有java版的后台处理类库，可惜没C#版，于是就有了。。。我的C#版的ECharts的Option构造类库。
    项目是我根据开源项目[java版（版本2.2.0.2）][2]编写移植的。。。
一个供C#开发使用的ECharts的开发包，主要目的是方便在C#中构造ECharts中可能用到的全部数据结构，如完整的结构Option。

----------分割线----------
    Option中的数据Series，包含Bar-柱状图，Line-折线图，Pie-饼图，Chord-和弦图等，支持ECharts中的所有图表。支持所有的Style类，如AreaStyle,ChordStyle,ItemStyle,LineStyle,LinkStyle等等。支持多种Data数据类型，一个通用的Data数据，以及PieData,MapData,ScatterData,KData等针对性的数据结构。
    你可以使用本项目直接构造一个`Option`对象，转换为JSON后直接用js设置`myChart.setOption(option)`，或者你也可以在前端构造基本的`Option`对象，然后使用本项目来构造其中需要的任意某部分的数据，如使用`Series`支持的图表类型创建`Series`数据。

**为了便于使用对应版本的ECcharts，本项目的版本号会使用和ECharts相同的版本号。**

##Option说明

1. `Option`正式代码中使用，不需要任何依赖。
2. `Option`类 重写了`toString()`方法,序列化:(`Option`类实例).ToString()...,支持两种`function`形式
3. Json序列化引用了Newtonsoft.Json 

##function说明

两种`function`形式:

```javascript
{
    formatter:function(value){
        return value.substring(0,8);
    }
}

//和

{
    formatter:(function(){
        return 'Temperature : <br/>{b}km : {c}°C';
    })()
}
```

 序列化,处理字符串中的function和(function(){})()，除{}中的代码外，其他地方不允许有空格


##文档可以参考java版地址  

>http://git.oschina.net/free/ECharts/wikis/Home  

##项目支持

###图表类型  

 - Line - 折线（面积）图  
 - Bar - 柱状（条形）图
 - Scatter - 散点（气泡）图  
 - K - K线图
 - Pie - 饼（圆环）图
 - Radar - 雷达（面积）图
 - Chord - 和弦图
 - Force - 力导向布局图
 - Map - 地图
 - Gauge - 仪表盘  
 - Funnel - 漏斗图 
 - Island - 孤岛图（官方未提供，这里只有数据Island对象）  


###ECharts组件  

 - Axis - 坐标轴 
 - Grid - 网格  
 - Title - 标题  
 - Tooltip - 提示  
 - Legend - 图例 
 - DataZoom - 数据区域缩放 
 - DataRange - 值域漫游
 - Toolbox - 工具箱
 - Timeline - 时间线

##ECharts网址

http://echarts.baidu.com/


###例子，简单的柱状图 C#版

```C#
 Option option = new Option();
            option.Title("ECharts2 vs ECharts1", "Chrome下测试数据");
            option.Tooltip(Trigger.axis);
            option.Legend(
                "ECharts1 - 2k数据", "ECharts1 - 2w数据", "ECharts1 - 20w数据", "",
                "ECharts2 - 2k数据", "ECharts2 - 2w数据", "ECharts2 - 20w数据");
            option.Toolbox().show(true)
                .feature(
                    Tool.mark, Tool.dataView,
                    new MagicType(Magic.line, Magic.bar),
                    Tool.restore, Tool.saveAsImage);
            option.Calculable(true);
            option.Grid().y(70).y2(30).x2(20);
            option.XAxis(
                new CategoryAxis().data("Line", "Bar", "Scatter", "K", "Map"),
                new CategoryAxis()
                    .axisLine(new AxisLine().show(false))
                    .axisTick(new AxisTick().show(false))
                    .axisLabel(new AxisLabel().show(false))
                    .splitArea(new SplitArea().show(false))
                    .axisLine(new AxisLine().show(false))
                    .data("Line", "Bar", "Scatter", "K", "Map")
                );
            option.YAxis(new ValueAxis().axisLabel(new AxisLabel().formatter("{value} ms")));

            Bar b1 = new Bar("ECharts2 - 2k数据");
            b1.itemStyle().normal().color("rgba(193,35,43,1)").label().show(true);
            b1.data(40, 155, 95, 75, 0);

            Bar b2 = new Bar("ECharts2 - 2w数据");
            b2.itemStyle().normal().color("rgba(181,195,52,1)").label().show(true).textStyle().color("#27727B");
            b2.data(100, 200, 105, 100, 156);

            Bar b3 = new Bar("ECharts2 - 20w数据");
            b3.itemStyle().normal().color("rgba(252,206,16,1)").label().show(true).textStyle().color("#E87C25");
            b3.data(906, 911, 908, 778, 0);

            Bar b4 = new Bar("ECharts1 - 2k数据");
            b4.itemStyle()
                .normal()
                .color("rgba(193,35,43,0.5)")
                .label()
                .show(true)
                .formatter("function(a,b,c){return c>0 ? (c +'\n'):'';}");
            b4.data(96, 224, 164, 124, 0).xAxisIndex(1);

            Bar b5 = new Bar("ECharts1 - 2w数据");
            b5.itemStyle().normal().color("rgba(181,195,52,0.5)").label().show(true);
            b5.data(491, 2035, 389, 955, 347).xAxisIndex(1);

            Bar b6 = new Bar("ECharts1 - 20w数据");
            b6.itemStyle()
                .normal()
                .color("rgba(252,206,16,0.5)")
                .label()
                .show(true)
                .formatter("function(a,b,c){return c>0 ? (c +'+'):'';}");
            b6.data(3000, 3000, 2817, 3000, 0, 1242).xAxisIndex(1);

            option.Series(b1, b2, b3, b4, b5, b6);
            
           return option.ToString();
```

代码生成的json结果：

```javascript
{
    "grid": {
        "x2": 20,
        "y2": 30,
        "y": 70
    },
    "legend": {
        "data": [
            "ECharts1 - 2k数据",
            "ECharts1 - 2w数据",
            "ECharts1 - 20w数据",
            "",
            "ECharts2 - 2k数据",
            "ECharts2 - 2w数据",
            "ECharts2 - 20w数据"
        ],
        "orient": "horizontal"
    },
    "title": {
        "text": "ECharts2 vs ECharts1",
        "subtext": "Chrome下测试数据",
        "textAlign": "center"
    },
    "toolbox": {
        "feature": {
            "mark": {
                "show": true,
                "title": {
                    "mark": "辅助线开关",
                    "markUndo": "删除辅助线",
                    "markClear": "清空辅助线"
                },
                "lineStyle": {
                    "color": "#1e90ff",
                    "type": "dashed",
                    "width": 2
                }
            },
            "dataView": {
                "show": true,
                "title": "数据视图",
                "readOnly": false,
                "lang": [
                    "数据视图",
                    "关闭",
                    "刷新"
                ]
            },
            "magicType": {
                "show": true,
                "title": {
                    "line": "折线图切换",
                    "bar": "柱形图切换",
                    "stack": "堆积",
                    "tiled": "平铺"
                },
                "type": [
                    "line",
                    "bar"
                ]
            },
            "restore": {
                "show": true,
                "title": "还原"
            },
            "saveAsImage": {
                "show": true,
                "title": "保存为图片",
                "type": "png",
                "lang": [
                    "点击保存"
                ]
            }
        },
        "orient": "horizontal",
        "show": true
    },
    "tooltip": {
        "trigger": "axis"
    },
    "calculable": true,
    "series": [
        {
            "itemStyle": {
                "normal": {
                    "label": {
                        "show": true
                    },
                    "color": "rgba(193,35,43,1)"
                }
            },
            "name": "ECharts2 - 2k数据",
            "type": "bar",
            "data": [
                40,
                155,
                95,
                75,
                0
            ]
        },
        {
            "itemStyle": {
                "normal": {
                    "label": {
                        "textStyle": {
                            "color": "#27727B",
                            "align": "center",
                            "fontStyle": "normal"
                        },
                        "show": true
                    },
                    "color": "rgba(181,195,52,1)"
                }
            },
            "name": "ECharts2 - 2w数据",
            "type": "bar",
            "data": [
                100,
                200,
                105,
                100,
                156
            ]
        },
        {
            "itemStyle": {
                "normal": {
                    "label": {
                        "textStyle": {
                            "color": "#E87C25",
                            "align": "center",
                            "fontStyle": "normal"
                        },
                        "show": true
                    },
                    "color": "rgba(252,206,16,1)"
                }
            },
            "name": "ECharts2 - 20w数据",
            "type": "bar",
            "data": [
                906,
                911,
                908,
                778,
                0
            ]
        },
        {
            "itemStyle": {
                "normal": {
                    "label": {
                        "show": true,
                        "formatter": function(a, b,c){
                            returnc>0?(c+'\n'): '';
                        }
                    },
                    "color": "rgba(193,35,43,0.5)"
                }
            },
            "name": "ECharts1 - 2k数据",
            "type": "bar",
            "data": [
                96,
                224,
                164,
                124,
                0
            ]
        },
        {
            "itemStyle": {
                "normal": {
                    "label": {
                        "show": true
                    },
                    "color": "rgba(181,195,52,0.5)"
                }
            },
            "name": "ECharts1 - 2w数据",
            "type": "bar",
            "data": [
                491,
                2035,
                389,
                955,
                347
            ]
        },
        {
            "itemStyle": {
                "normal": {
                    "label": {
                        "show": true,
                        "formatter": function(a,b,c){
                            returnc>0?(c+'+'): '';
                        }
                    },
                    "color": "rgba(252,206,16,0.5)"
                }
            },
            "name": "ECharts1 - 20w数据",
            "type": "bar",
            "data": [
                3000,
                3000,
                2817,
                3000,
                0,
                1242
            ]
        }
    ],
    "yAxis": [
        {
            "axisLabel": {
                "formatter": "{value} ms"
            },
            "nameLocation": "start",
            "type": "value"
        }
    ],
    "xAxis": [
        {
            "nameLocation": "start",
            "type": "category",
            "data": [
                "Line",
                "Bar",
                "Scatter",
                "K",
                "Map"
            ]
        },
        {
            "axisLabel": {
                "show": false
            },
            "axisLine": {
                "show": false
            },
            "axisTick": {
                "show": false
            },
            "nameLocation": "start",
            "splitArea": {
                "show": false
            },
            "type": "category",
            "data": [
                "Line",
                "Bar",
                "Scatter",
                "K",
                "Map"
            ]
        }
    ]
}
```

查看测试可把生产的json字符串复制到http://echarts.baidu.com/doc/example/bar1.html,替换`option`对象,点击`刷新`按钮就可看效果


  [1]: http://echarts.baidu.com/
  [2]: http://git.oschina.net/free/ECharts#git-readme