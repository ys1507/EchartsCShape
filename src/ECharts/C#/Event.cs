/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using System;
using System.ComponentModel;
using System.Reflection;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     Description: Event
    ///     @author Yongjin.C
    /// </summary>
    public enum Event
    {
        // -------全局通用

        REFRESH,

        RESTORE,

        RESIZE,
        CLICK,

        DBLCLICK,
        HOVER,
        //MOUSEWHEEL("mousewheel"),
        // -------业务交互逻辑

        DATA_CHANGED,

        DATA_ZOOM,

        DATA_RANGE,

        LEGEND_SELECTED,

        MAP_SELECTED,

        PIE_SELECTED,

        MAGIC_TYPE_CHANGED,

        DATA_VIEW_CHANGED,

        TIMELINE_CHANGED,

        MAP_ROAM,
        // -------内部通信

        TOOLTIP_HOVER,

        TOOLTIP_IN_GRID,

        TOOLTIP_OUT_GRID
    }

    public static class EnumExtensionMethods
    {
        /// <summary>
        ///     获取描述
        /// </summary>
        public static String GetDescription(this Event e)
        {
            FieldInfo enumInfo = e.GetType().GetField(e.ToString());
            var enumAttributes =
                (DescriptionAttribute[]) enumInfo.GetCustomAttributes(typeof (DescriptionAttribute), false);
            return enumAttributes.Length > 0 ? enumAttributes[0].Description : e.ToString();
        }
    }
}