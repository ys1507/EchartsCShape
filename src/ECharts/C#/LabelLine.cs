using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     标签视觉引导线l
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class LabelLine
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     线长 ，从外圆边缘起计算，可为负值
        /// </summary>
        private int? length_Renamed;

        /// <summary>
        ///     线条样式，详见lineStyle
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        private LineStyle lineStyle_Renamed;

        /// <summary>
        ///     饼图标签视觉引导线显示策略，可选为：true（显示） | false（隐藏）
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     获取lineStyle值
        /// </summary>
        public virtual LineStyle LineStyle
        {
            get { return lineStyle_Renamed; }
            set { lineStyle_Renamed = value; }
        }


        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }


        public virtual int? Length
        {
            get { return length_Renamed; }
            set { length_Renamed = value; }
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual LabelLine show(bool? show)
        {
            show_Renamed = show;
            return this;
        }

        /// <summary>
        ///     获取length值
        /// </summary>
        public virtual int? length()
        {
            return length_Renamed;
        }

        /// <summary>
        ///     设置length值     *
        /// </summary>
        /// <param name="length"> </param>
        public virtual LabelLine length(int? length)
        {
            length_Renamed = length;
            return this;
        }

        /// <summary>
        ///     线条样式，详见lineStyle
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        public virtual LineStyle lineStyle()
        {
            if (lineStyle_Renamed == null)
            {
                lineStyle_Renamed = new LineStyle();
            }
            return lineStyle_Renamed;
        }

        /// <summary>
        ///     设置style值     *
        /// </summary>
        /// <param name="style"> </param>
        public virtual LabelLine lineStyle(LineStyle style)
        {
            lineStyle_Renamed = style;
            return this;
        }
    }
}