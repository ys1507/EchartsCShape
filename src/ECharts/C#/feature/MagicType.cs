using System.Collections;
using System.Collections.Generic;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.series;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.feature
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    public class MagicType : Feature
    {
        private Option option_Renamed;

        /// <summary>
        ///     构造函数,参数:magics
        /// </summary>
        /// <param name="magics"> </param>
        public MagicType(params Magic[] magics)
        {
            show(true);
            IDictionary title = new Dictionary<string, string>();
            title["line"] = "折线图切换";
            title["bar"] = "柱形图切换";
            title["stack"] = "堆积";
            title["tiled"] = "平铺";
            this.title(title);
            if (magics == null || magics.Length == 0)
            {
                type(new object[] {Magic.bar, Magic.line, Magic.stack, Magic.tiled});
            }
            else
            {
                type(magics);
            }
        }


        /// <summary>
        ///     设置Option
        /// </summary>
        /// <param name="option">
        ///     @return
        /// </param>
        public virtual Feature option(Option option)
        {
            option_Renamed = option;
            return this;
        }

        /// <summary>
        ///     获取Option
        ///     @return
        /// </summary>
        public virtual Option option()
        {
            return option_Renamed;
        }

        /// <summary>
        ///     获取option值     *
        ///     @return
        /// </summary>
        public virtual Option getOption()
        {
            return option_Renamed;
        }

        /// <summary>
        ///     设置option
        /// </summary>
        /// <param name="option"> </param>
        public virtual void setOption(Option option)
        {
            option_Renamed = option;
        }

        /// <summary>
        ///     内部籿Option
        /// </summary>
        public class Option
        {
            internal Funnel funnel_Renamed;

            public virtual Funnel Funnel
            {
                get { return funnel_Renamed; }
                set { funnel_Renamed = value; }
            }

            public virtual Option funnel(Funnel funnel)
            {
                funnel_Renamed = funnel;
                return this;
            }

            public virtual Funnel funnel()
            {
                return funnel_Renamed;
            }
        }
    }
}