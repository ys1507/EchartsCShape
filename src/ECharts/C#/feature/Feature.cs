using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.feature
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Feature
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     获取DataView值
        /// </summary>
        public static readonly Feature dataView = new DataView();

        /// <summary>
        ///     获取DataZoom值
        /// </summary>
        public static readonly Feature dataZoom = new DataZoom();

        /// <summary>
        ///     获取Mark值
        /// </summary>
        public static readonly Feature mark = new Mark();

        /// <summary>
        ///     获取SaveAsImage值
        /// </summary>
        public static readonly Feature saveAsImage = new SaveAsImage();

        /// <summary>
        ///     获取MagicType值
        /// </summary>
        public static readonly Feature magicType = new MagicType();

        /// <summary>
        ///     获取Restore值
        /// </summary>
        public static readonly Feature restore = new Restore();

        /// <summary>
        ///     图标，image://开夿
        /// </summary>
        private string icon_Renamed;

        /// <summary>
        ///     lang 非IE浏览器支持点击下载，有保存话术，默认是“点击保存”，可修政
        /// </summary>
        private object lang_Renamed;

        /// <summary>
        ///     线条颜色
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        private LineStyle lineStyle_Renamed;

        /// <summary>
        ///     只读
        /// </summary>
        private bool? readOnly_Renamed;

        /// <summary>
        ///     是否显示
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     文字颜色
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        private TextStyle textStyle_Renamed;

        /// <summary>
        ///     标题
        /// </summary>
        private object title_Renamed;

        /// <summary>
        ///     类型
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Magic
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.code.LineType
        /// </seealso>
        private object type_Renamed;

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }


        /// <summary>
        ///     获取title值
        /// </summary>
        public virtual object Title
        {
            get { return title_Renamed; }
            set { title_Renamed = value; }
        }


        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual object Type
        {
            get { return type_Renamed; }
            set { type_Renamed = value; }
        }


        /// <summary>
        ///     获取readOnly值
        /// </summary>
        public virtual bool? ReadOnly
        {
            get { return readOnly_Renamed; }
            set { readOnly_Renamed = value; }
        }


        /// <summary>
        ///     获取lang值
        /// </summary>
        public virtual object Lang
        {
            get { return lang_Renamed; }
            set { lang_Renamed = value; }
        }


        /// <summary>
        ///     获取lineStyle值
        /// </summary>
        public virtual LineStyle LineStyle
        {
            get { return lineStyle_Renamed; }
            set { lineStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取icon值
        /// </summary>
        public virtual string Icon
        {
            get { return icon_Renamed; }
            set { icon_Renamed = value; }
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual Feature show(bool? show)
        {
            show_Renamed = show;
            return this;
        }

        /// <summary>
        ///     获取title值
        /// </summary>
        public virtual object title()
        {
            return title_Renamed;
        }

        /// <summary>
        ///     设置title值     *
        /// </summary>
        /// <param name="title"> </param>
        public virtual Feature title(object title)
        {
            title_Renamed = title;
            return this;
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual object type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual Feature type(object type)
        {
            type_Renamed = type;
            return this;
        }

        /// <summary>
        ///     获取readOnly值
        /// </summary>
        public virtual bool? readOnly()
        {
            return readOnly_Renamed;
        }

        /// <summary>
        ///     设置readOnly值     *
        /// </summary>
        /// <param name="readOnly"> </param>
        public virtual Feature readOnly(bool? readOnly)
        {
            readOnly_Renamed = readOnly;
            return this;
        }

        /// <summary>
        ///     获取lang值
        /// </summary>
        public virtual object lang()
        {
            return lang_Renamed;
        }

        /// <summary>
        ///     设置lang值     *
        /// </summary>
        /// <param name="lang"> </param>
        public virtual Feature lang(object lang)
        {
            lang_Renamed = lang;
            return this;
        }

        /// <summary>
        ///     获取lineStyle值
        /// </summary>
        public virtual LineStyle lineStyle()
        {
            return lineStyle_Renamed;
        }

        /// <summary>
        ///     设置lineStyle值     *
        /// </summary>
        /// <param name="lineStyle"> </param>
        public virtual Feature lineStyle(LineStyle lineStyle)
        {
            lineStyle_Renamed = lineStyle;
            return this;
        }

        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle textStyle()
        {
            return textStyle_Renamed;
        }

        /// <summary>
        ///     设置textStyle值     *
        /// </summary>
        /// <param name="textStyle"> </param>
        public virtual Feature textStyle(TextStyle textStyle)
        {
            textStyle_Renamed = textStyle;
            return this;
        }

        /// <summary>
        ///     获取icon值
        /// </summary>
        public virtual string icon()
        {
            return icon_Renamed;
        }

        /// <summary>
        ///     设置icon值     *
        /// </summary>
        /// <param name="icon"> </param>
        public virtual Feature icon(string icon)
        {
            icon_Renamed = icon;
            return this;
        }
    }
}