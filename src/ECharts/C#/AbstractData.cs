/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     Data接口 - 添加数据
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public abstract class AbstractData<T> : Data<T> where T : class
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     是否可点击，默认开吿
        /// </summary>
        private bool? clickable_Renamed;

        /// <summary>
        ///     标线图形数据
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.data.PointData
        /// </seealso>
        protected internal List<object> data_Renamed;

        /// <summary>
        ///     非数值显示（如仅用于显示标注标线时），可以通过hoverable:false关闭区域悬浮高亮
        ///     @since 2.2.0
        /// </summary>
        private bool? hoverable_Renamed;

        /// <summary>
        ///     获取clickable值
        /// </summary>
        public virtual bool? Clickable
        {
            get { return clickable_Renamed; }
            set { clickable_Renamed = value; }
        }

        /// <summary>
        ///     获取hoverable值
        /// </summary>
        public virtual bool? Hoverable
        {
            get { return hoverable_Renamed; }
            set { hoverable_Renamed = value; }
        }


        /// <summary>
        ///     获取data值
        /// </summary>
        public virtual List<object> Data
        {
            get { return data_Renamed; }
            set { data_Renamed = value; }
        }

        /// <summary>
        ///     添加元素
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual T data(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this as T;
            }
            data().AddRange(values.ToList());
            return this as T;
        }

        /// <summary>
        ///     获取data值
        /// </summary>
        public virtual List<object> data()
        {
            if (data_Renamed == null)
            {
                data_Renamed = new List<object>();
            }
            return data_Renamed;
        }


        /// <summary>
        ///     获取clickable值
        /// </summary>
        public virtual bool? clickable()
        {
            return clickable_Renamed;
        }

        /// <summary>
        ///     设置clickable值     *
        /// </summary>
        /// <param name="clickable"> </param>
        public virtual T clickable(bool? clickable)
        {
            clickable_Renamed = clickable;
            return this as T;
        }


        /// <summary>
        ///     获取hoverable值
        /// </summary>
        public virtual bool? hoverable()
        {
            return hoverable_Renamed;
        }

        /// <summary>
        ///     设置hoverable值     *
        /// </summary>
        /// <param name="hoverable"> </param>
        public virtual T hoverable(bool? hoverable)
        {
            hoverable_Renamed = hoverable;
            return this as T;
        }
    }
}