/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     数据区域缩放。与toolbox.feature.dataZoom同步，仅对直角坐标系图表有效
    ///     @author Yongjin.C
    /// </summary>
    public class DataZoom : Basic<DataZoom>, Component
    {
        /// <summary>
        ///     默认#ccc，数据缩略背景颜艿
        /// </summary>
        private string dataBackgroundColor_Renamed;

        /// <summary>
        ///     数据缩放，选择结束比例，默认为100＿），到最后一个数据选择结束
        /// </summary>
        private int? end_Renamed;

        /// <summary>
        ///     默认值rgba(144,197,237,0.2)，选择区域填充颜色
        /// </summary>
        private string fillerColor_Renamed;

        /// <summary>
        ///     默认值rgba(70,130,180,0.8)，控制手柄颜艿
        /// </summary>
        private string handleColor_Renamed;

        /// <summary>
        ///     布局方式，默认为水平布局，可选为＿horizontal' | 'vertical'
        /// </summary>
        private Orient orient_Renamed;

        /// <summary>
        ///     缩放变化是否实时显示，建议性能较低的浏览器或数据量巨大时不启动实时效果
        /// </summary>
        private bool? realtime_Renamed;

        /// <summary>
        ///     缩放变化是否显示定位详情
        /// </summary>
        private bool? showDetail_Renamed;

        /// <summary>
        ///     数据缩放，选择起始比例，默认为0＿），从首个数据起选择
        /// </summary>
        private int? start_Renamed;

        /// <summary>
        ///     当不指定时默认控制所有横向类目，可通过数组指定多个需要控制的横向类目坐标轴Index，仅一个时可直接为数字
        /// </summary>
        private object xAxisIndex_Renamed;

        /// <summary>
        ///     当不指定时默认控制所有纵向类目，可通过数组指定多个需要控制的纵向类目坐标轴Index，仅一个时可直接为数字
        /// </summary>
        private object yAxisIndex_Renamed;

        /// <summary>
        ///     数据缩放锁，默认为false，当设置为true时选择区域不能伸缩，即(end - start)值保持不变，仅能做数据漫渿
        /// </summary>
        private bool? zoomLock_Renamed;

        /// <summary>
        ///     获取orient值
        /// </summary>
        public virtual Orient Orient
        {
            get { return orient_Renamed; }
            set { orient_Renamed = value; }
        }


        /// <summary>
        ///     获取dataBackgroundColor值
        /// </summary>
        public virtual string DataBackgroundColor
        {
            get { return dataBackgroundColor_Renamed; }
            set { dataBackgroundColor_Renamed = value; }
        }


        /// <summary>
        ///     获取fillerColor值
        /// </summary>
        public virtual string FillerColor
        {
            get { return fillerColor_Renamed; }
            set { fillerColor_Renamed = value; }
        }


        /// <summary>
        ///     获取handleColor值
        /// </summary>
        public virtual string HandleColor
        {
            get { return handleColor_Renamed; }
            set { handleColor_Renamed = value; }
        }

        /// <summary>
        ///     获取start值
        /// </summary>
        public virtual int? Start
        {
            get { return start_Renamed; }
            set { start_Renamed = value; }
        }


        /// <summary>
        ///     获取end值
        /// </summary>
        public virtual int? End
        {
            get { return end_Renamed; }
            set { end_Renamed = value; }
        }


        /// <summary>
        ///     获取realtime值
        /// </summary>
        public virtual bool? Realtime
        {
            get { return realtime_Renamed; }
            set { realtime_Renamed = value; }
        }


        /// <summary>
        ///     获取zoomLock值
        /// </summary>
        public virtual bool? ZoomLock
        {
            get { return zoomLock_Renamed; }
            set { zoomLock_Renamed = value; }
        }


        /// <summary>
        ///     获取showDetail值
        /// </summary>
        public virtual bool? ShowDetail
        {
            get { return showDetail_Renamed; }
            set { showDetail_Renamed = value; }
        }

        /// <summary>
        ///     获取orient值
        /// </summary>
        public virtual Orient orient()
        {
            return orient_Renamed;
        }

        /// <summary>
        ///     设置orient值     *
        /// </summary>
        /// <param name="orient"> </param>
        public virtual DataZoom orient(Orient orient)
        {
            orient_Renamed = orient;
            return this;
        }

        /// <summary>
        ///     获取dataBackgroundColor值
        /// </summary>
        public virtual string dataBackgroundColor()
        {
            return dataBackgroundColor_Renamed;
        }

        /// <summary>
        ///     设置dataBackgroundColor值     *
        /// </summary>
        /// <param name="dataBackgroundColor"> </param>
        public virtual DataZoom dataBackgroundColor(string dataBackgroundColor)
        {
            dataBackgroundColor_Renamed = dataBackgroundColor;
            return this;
        }

        /// <summary>
        ///     获取fillerColor值
        /// </summary>
        public virtual string fillerColor()
        {
            return fillerColor_Renamed;
        }

        /// <summary>
        ///     设置fillerColor值     *
        /// </summary>
        /// <param name="fillerColor"> </param>
        public virtual DataZoom fillerColor(string fillerColor)
        {
            fillerColor_Renamed = fillerColor;
            return this;
        }

        /// <summary>
        ///     获取handleColor值
        /// </summary>
        public virtual string handleColor()
        {
            return handleColor_Renamed;
        }

        /// <summary>
        ///     设置handleColor值     *
        /// </summary>
        /// <param name="handleColor"> </param>
        public virtual DataZoom handleColor(string handleColor)
        {
            handleColor_Renamed = handleColor;
            return this;
        }

        /// <summary>
        ///     获取xAxisIndex值
        /// </summary>
        public virtual object xAxisIndex()
        {
            return xAxisIndex_Renamed;
        }

        /// <summary>
        ///     设置xAxisIndex值     *
        /// </summary>
        /// <param name="xAxisIndex"> </param>
        public virtual DataZoom xAxisIndex(object xAxisIndex)
        {
            xAxisIndex_Renamed = xAxisIndex;
            return this;
        }

        /// <summary>
        ///     获取yAxisIndex值
        /// </summary>
        public virtual object yAxisIndex()
        {
            return yAxisIndex_Renamed;
        }

        /// <summary>
        ///     设置yAxisIndex值     *
        /// </summary>
        /// <param name="yAxisIndex"> </param>
        public virtual DataZoom yAxisIndex(object yAxisIndex)
        {
            yAxisIndex_Renamed = yAxisIndex;
            return this;
        }

        /// <summary>
        ///     获取start值
        /// </summary>
        public virtual int? start()
        {
            return start_Renamed;
        }

        /// <summary>
        ///     设置start值     *
        /// </summary>
        /// <param name="start"> </param>
        public virtual DataZoom start(int? start)
        {
            start_Renamed = start;
            return this;
        }

        /// <summary>
        ///     获取end值
        /// </summary>
        public virtual int? end()
        {
            return end_Renamed;
        }

        /// <summary>
        ///     设置end值     *
        /// </summary>
        /// <param name="end"> </param>
        public virtual DataZoom end(int? end)
        {
            end_Renamed = end;
            return this;
        }

        /// <summary>
        ///     获取realtime值
        /// </summary>
        public virtual bool? realtime()
        {
            return realtime_Renamed;
        }

        /// <summary>
        ///     设置realtime值     *
        /// </summary>
        /// <param name="realtime"> </param>
        public virtual DataZoom realtime(bool? realtime)
        {
            realtime_Renamed = realtime;
            return this;
        }

        /// <summary>
        ///     获取zoomLock值
        /// </summary>
        public virtual bool? zoomLock()
        {
            return zoomLock_Renamed;
        }

        /// <summary>
        ///     设置zoomLock值     *
        /// </summary>
        /// <param name="zoomLock"> </param>
        public virtual DataZoom zoomLock(bool? zoomLock)
        {
            zoomLock_Renamed = zoomLock;
            return this;
        }

        /// <summary>
        ///     获取showDetail值
        /// </summary>
        public virtual bool? showDetail()
        {
            return showDetail_Renamed;
        }

        /// <summary>
        ///     设置showDetail值     *
        /// </summary>
        /// <param name="showDetail"> </param>
        public virtual DataZoom showDetail(bool? showDetail)
        {
            showDetail_Renamed = showDetail;
            return this;
        }


        /// <summary>
        ///     获取xAxisIndex值
        /// </summary>
        public virtual object getxAxisIndex()
        {
            return xAxisIndex_Renamed;
        }

        /// <summary>
        ///     设置xAxisIndex值     *
        /// </summary>
        /// <param name="xAxisIndex"> </param>
        public virtual void setxAxisIndex(object xAxisIndex)
        {
            xAxisIndex_Renamed = xAxisIndex;
        }

        /// <summary>
        ///     获取yAxisIndex值
        /// </summary>
        public virtual object getyAxisIndex()
        {
            return yAxisIndex_Renamed;
        }

        /// <summary>
        ///     设置yAxisIndex值     *
        /// </summary>
        /// <param name="yAxisIndex"> </param>
        public virtual void setyAxisIndex(object yAxisIndex)
        {
            yAxisIndex_Renamed = yAxisIndex;
        }
    }
}