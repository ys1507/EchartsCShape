using System;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     Description: Label
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public abstract class AbstractLabel<T> where T : class
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     [Axis有效]坐标轴文本标签是否可点击
        /// </summary>
        private bool? clickable_Renamed;

        /// <summary>
        ///     颜色
        /// </summary>
        private string color_Renamed;

        /// <summary>
        ///     间隔名称格式器：{string}（Template＿| {Function}
        /// </summary>
        private object formatter_Renamed;

        /// <summary>
        ///     挑选间隔，默认丿auto'，可选为＿auto'（自动隐藏显示不下的＿| 0（全部显示） | {number}
        /// </summary>
        private object interval_Renamed;

        /// <summary>
        ///     坐标轴文本标签与坐标轴的间距
        /// </summary>
        private int? margin_Renamed;

        /// <summary>
        ///     位置
        /// </summary>
        private object position_Renamed;

        /// <summary>
        ///     rotate : 旋转角度，默认为0，不旋转，正值为逆时针，负值为顺时针，可选为＿90 ~ 90
        /// </summary>
        private int? rotate_Renamed;

        /// <summary>
        ///     是否显示，在Timeline中默认true
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     文字样式（详见{@link Yongjin.Cshape.echarts.style.TextStyle}＿
        /// </summary>
        private TextStyle textStyle_Renamed;

        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }


        /// <summary>
        ///     获取position值
        /// </summary>
        public virtual object Position
        {
            get { return position_Renamed; }
            set { position_Renamed = value; }
        }


        /// <summary>
        ///     获取interval值
        /// </summary>
        public virtual object Interval
        {
            get { return interval_Renamed; }
            set { interval_Renamed = value; }
        }


        /// <summary>
        ///     获取rotate值
        /// </summary>
        public virtual int? Rotate
        {
            get { return rotate_Renamed; }
            set { rotate_Renamed = value; }
        }


        /// <summary>
        ///     获取clickable值
        /// </summary>
        public virtual bool? Clickable
        {
            get { return clickable_Renamed; }
            set { clickable_Renamed = value; }
        }


        /// <summary>
        ///     获取formatter值
        /// </summary>
        public virtual object Formatter
        {
            get { return formatter_Renamed; }
            set { formatter_Renamed = value; }
        }


        /// <summary>
        ///     获取margin值
        /// </summary>
        public virtual int? Margin
        {
            get { return margin_Renamed; }
            set { margin_Renamed = value; }
        }


        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }

        /// <summary>
        ///     设置textStyle值     *
        /// </summary>
        /// <param name="textStyle"> </param>
        public virtual T textStyle(TextStyle textStyle)
        {
            textStyle_Renamed = textStyle;

            return this as T;
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual T show(bool? show)
        {
            show_Renamed = show;
            return this as T;
        }

        /// <summary>
        ///     获取position值
        /// </summary>
        public virtual object position()
        {
            return position_Renamed;
        }

        /// <summary>
        ///     设置position值     *
        /// </summary>
        /// <param name="position"> </param>
        public virtual T position(object position)
        {
            position_Renamed = position;
            return this as T;
        }

        /// <summary>
        ///     设置position值     *
        /// </summary>
        /// <param name="position"> </param>
        public virtual T position(Position position)
        {
            position_Renamed = position;
            return this as T;
        }

        /// <summary>
        ///     获取interval值
        /// </summary>
        public virtual object interval()
        {
            return interval_Renamed;
        }

        /// <summary>
        ///     设置interval值     *
        /// </summary>
        /// <param name="interval"> </param>
        public virtual T interval(object interval)
        {
            interval_Renamed = interval;
            return this as T;
        }

        /// <summary>
        ///     获取rotate值
        /// </summary>
        public virtual int? rotate()
        {
            return rotate_Renamed;
        }

        /// <summary>
        ///     设置rotate值     *
        /// </summary>
        /// <param name="rotate"> </param>
        public virtual T rotate(int? rotate)
        {
            rotate_Renamed = rotate;
            return this as T;
        }

        /// <summary>
        ///     获取clickable值
        /// </summary>
        public virtual bool? clickable()
        {
            return clickable_Renamed;
        }

        /// <summary>
        ///     设置clickable值     *
        /// </summary>
        /// <param name="clickable"> </param>
        public virtual T clickable(bool? clickable)
        {
            clickable_Renamed = clickable;
            return this as T;
        }

        /// <summary>
        ///     获取formatter值
        /// </summary>
        public virtual object formatter()
        {
            return formatter_Renamed;
        }

        /// <summary>
        ///     设置formatter值     *
        /// </summary>
        /// <param name="formatter"> </param>
        public virtual T formatter(object formatter)
        {
            formatter_Renamed = formatter;
            return this as T;
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual T color(string color)
        {
            color_Renamed = color;
            return this as T;
        }

        /// <summary>
        ///     文字样式（详见{@link Yongjin.Cshape.echarts.style.TextStyle}＿
        /// </summary>
        public virtual TextStyle textStyle()
        {
            if (textStyle_Renamed == null)
            {
                textStyle_Renamed = new TextStyle();
            }
            return textStyle_Renamed;
        }

        /// <summary>
        ///     获取margin值
        /// </summary>
        public virtual int? margin()
        {
            return margin_Renamed;
        }

        /// <summary>
        ///     设置margin值     *
        /// </summary>
        /// <param name="margin"> </param>
        public virtual T margin(int? margin)
        {
            margin_Renamed = margin;
            return this as T;
        }
    }
}