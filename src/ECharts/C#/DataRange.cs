/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System.Collections.Generic;
using System.Linq;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     值域选择，每个图表最多仅有一个值域控件
    ///     @author Yongjin.C
    /// </summary>
    public class DataRange : Basic<DataRange>, Component
    {
        /// <summary>
        ///     是否启用值域漫游，启用后无视splitNumber，值域显示为线性渐叿
        /// </summary>
        private bool? calculable_Renamed;

        /// <summary>
        ///     值域颜色标识，颜色数组长度必顿=2，颜色代表从数值高到低的变化，即颜色数组低位代表数值高的颜色标诿
        /// </summary>
        private List<string> color_Renamed;

        /// <summary>
        ///     内容格式器：{string}（Template＿| {Function}，模板变量为'{value}'咿{value2}'，代表数值起始值和结束值，函数参数两个，含义同模板变量
        /// </summary>
        private object formatter_Renamed;

        /// <summary>
        ///     是否启用地图hover时的联动响应
        /// </summary>
        private bool? hoverLink_Renamed;

        /// <summary>
        ///     默认值4，值域控件图形高度
        /// </summary>
        private int? itemHeight_Renamed;

        /// <summary>
        ///     默认值0，值域控件图形宽度
        /// </summary>
        private int? itemWidth_Renamed;

        /// <summary>
        ///     指定的最大值，eg: 100，默认无，必须参敿
        /// </summary>
        private int? max_Renamed;

        /// <summary>
        ///     指定的最小值，eg: 0，默认无，必须参敿
        /// </summary>
        private int? min_Renamed;

        /// <summary>
        ///     布局方式，默认为垂直布局，可选为＿horizontal' | 'vertical'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Orient
        /// </seealso>
        private Orient orient_Renamed;

        /// <summary>
        ///     小数精度，默认为0，无小数点，彿min ~ max 间在当前精度下无法整除splitNumber份时，精度会自动提高以满足均分，不支持不等划刿
        /// </summary>
        private int? precision_Renamed;

        /// <summary>
        ///     值域漫游是否实时显示
        /// </summary>
        private bool? realtime_Renamed;

        /// <summary>
        ///     分割段数，默认为5，为0时为线性渐变，calculable为true是默认均刿00仿
        /// </summary>
        private int? splitNumber_Renamed;

        /// <summary>
        ///     默认只设定了值域控件文字颜色
        /// </summary>
        private TextStyle textStyle_Renamed;

        /// <summary>
        ///     值域文字显示，splitNumber生效时默认以计算所得数值作为值域文字显示，可指定长度丿的文本数组显示简介的值域文本，如['髿, '使]＿\n'指定换行
        /// </summary>
        private List<string> text_Renamed;

        /// <summary>
        ///     获取orient值
        /// </summary>
        public virtual Orient Orient
        {
            get { return orient_Renamed; }
            set { orient_Renamed = value; }
        }


        /// <summary>
        ///     获取itemWidth值
        /// </summary>
        public virtual int? ItemWidth
        {
            get { return itemWidth_Renamed; }
            set { itemWidth_Renamed = value; }
        }


        /// <summary>
        ///     获取itemHeight值
        /// </summary>
        public virtual int? ItemHeight
        {
            get { return itemHeight_Renamed; }
            set { itemHeight_Renamed = value; }
        }


        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual int? Min
        {
            get { return min_Renamed; }
            set { min_Renamed = value; }
        }


        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual int? Max
        {
            get { return max_Renamed; }
            set { max_Renamed = value; }
        }


        /// <summary>
        ///     获取precision值
        /// </summary>
        public virtual int? Precision
        {
            get { return precision_Renamed; }
            set { precision_Renamed = value; }
        }


        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? SplitNumber
        {
            get { return splitNumber_Renamed; }
            set { splitNumber_Renamed = value; }
        }


        /// <summary>
        ///     获取calculable值
        /// </summary>
        public virtual bool? Calculable
        {
            get { return calculable_Renamed; }
            set { calculable_Renamed = value; }
        }


        /// <summary>
        ///     获取realtime值
        /// </summary>
        public virtual bool? Realtime
        {
            get { return realtime_Renamed; }
            set { realtime_Renamed = value; }
        }


        /// <summary>
        ///     获取formatter值
        /// </summary>
        public virtual object Formatter
        {
            get { return formatter_Renamed; }
            set { formatter_Renamed = value; }
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual List<string> Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }


        /// <summary>
        ///     获取text值
        /// </summary>
        public virtual List<string> Text
        {
            get { return text_Renamed; }
            set { text_Renamed = value; }
        }


        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取hoverLink值
        /// </summary>
        public virtual bool? HoverLink
        {
            get { return hoverLink_Renamed; }
            set { hoverLink_Renamed = value; }
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual DataRange color(List<string> color)
        {
            color_Renamed = color;
            return this;
        }

        /// <summary>
        ///     设置text值     *
        /// </summary>
        /// <param name="text"> </param>
        public virtual DataRange text(List<string> text)
        {
            text_Renamed = text;
            return this;
        }

        /// <summary>
        ///     获取hoverLink值
        /// </summary>
        public virtual bool? hoverLink()
        {
            return hoverLink_Renamed;
        }

        /// <summary>
        ///     设置hoverLink值     *
        /// </summary>
        /// <param name="hoverLink"> </param>
        public virtual DataRange hoverLink(bool? hoverLink)
        {
            hoverLink_Renamed = hoverLink;
            return this;
        }


        /// <summary>
        ///     设置textStyle值     *
        /// </summary>
        /// <param name="textStyle"> </param>
        public virtual DataRange textStyle(TextStyle textStyle)
        {
            textStyle_Renamed = textStyle;
            return this;
        }

        /// <summary>
        ///     获取orient值
        /// </summary>
        public virtual Orient orient()
        {
            return orient_Renamed;
        }

        /// <summary>
        ///     设置orient值     *
        /// </summary>
        /// <param name="orient"> </param>
        public virtual DataRange orient(Orient orient)
        {
            orient_Renamed = orient;
            return this;
        }

        /// <summary>
        ///     获取itemWidth值
        /// </summary>
        public virtual int? itemWidth()
        {
            return itemWidth_Renamed;
        }

        /// <summary>
        ///     设置itemWidth值     *
        /// </summary>
        /// <param name="itemWidth"> </param>
        public virtual DataRange itemWidth(int? itemWidth)
        {
            itemWidth_Renamed = itemWidth;
            return this;
        }

        /// <summary>
        ///     获取itemHeight值
        /// </summary>
        public virtual int? itemHeight()
        {
            return itemHeight_Renamed;
        }

        /// <summary>
        ///     设置itemHeight值     *
        /// </summary>
        /// <param name="itemHeight"> </param>
        public virtual DataRange itemHeight(int? itemHeight)
        {
            itemHeight_Renamed = itemHeight;
            return this;
        }

        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual int? min()
        {
            return min_Renamed;
        }

        /// <summary>
        ///     设置min值     *
        /// </summary>
        /// <param name="min"> </param>
        public virtual DataRange min(int? min)
        {
            min_Renamed = min;
            return this;
        }

        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual int? max()
        {
            return max_Renamed;
        }

        /// <summary>
        ///     设置max值     *
        /// </summary>
        /// <param name="max"> </param>
        public virtual DataRange max(int? max)
        {
            max_Renamed = max;
            return this;
        }

        /// <summary>
        ///     获取precision值
        /// </summary>
        public virtual int? precision()
        {
            return precision_Renamed;
        }

        /// <summary>
        ///     设置precision值     *
        /// </summary>
        /// <param name="precision"> </param>
        public virtual DataRange precision(int? precision)
        {
            precision_Renamed = precision;
            return this;
        }

        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? splitNumber()
        {
            return splitNumber_Renamed;
        }

        /// <summary>
        ///     设置splitNumber值     *
        /// </summary>
        /// <param name="splitNumber"> </param>
        public virtual DataRange splitNumber(int? splitNumber)
        {
            splitNumber_Renamed = splitNumber;
            return this;
        }

        /// <summary>
        ///     获取calculable值
        /// </summary>
        public virtual bool? calculable()
        {
            return calculable_Renamed;
        }

        /// <summary>
        ///     设置calculable值     *
        /// </summary>
        /// <param name="calculable"> </param>
        public virtual DataRange calculable(bool? calculable)
        {
            calculable_Renamed = calculable;
            return this;
        }

        /// <summary>
        ///     获取realtime值
        /// </summary>
        public virtual bool? realtime()
        {
            return realtime_Renamed;
        }

        /// <summary>
        ///     设置realtime值     *
        /// </summary>
        /// <param name="realtime"> </param>
        public virtual DataRange realtime(bool? realtime)
        {
            realtime_Renamed = realtime;
            return this;
        }

        /// <summary>
        ///     值域颜色标识，颜色数组长度必顿=2，颜色代表从数值高到低的变化，即颜色数组低位代表数值高的颜色标诿
        /// </summary>
        public virtual List<string> color()
        {
            if (color_Renamed == null)
            {
                color_Renamed = new List<string>();
            }
            return color_Renamed;
        }

        /// <summary>
        ///     值域颜色标识，颜色数组长度必顿=2，颜色代表从数值高到低的变化，即颜色数组低位代表数值高的颜色标诿     *
        /// </summary>
        /// <param name="colors">
        ///     @return
        /// </param>
        public virtual DataRange color(params string[] colors)
        {
            if (colors == null || colors.Length == 0)
            {
                return this;
            }
            color().AddRange(colors.ToList());
            return this;
        }

        /// <summary>
        ///     获取formatter值
        /// </summary>
        public virtual object formatter()
        {
            return formatter_Renamed;
        }

        /// <summary>
        ///     设置formatter值     *
        /// </summary>
        /// <param name="formatter"> </param>
        public virtual DataRange formatter(object formatter)
        {
            formatter_Renamed = formatter;
            return this;
        }

        /// <summary>
        ///     值域文字显示，splitNumber生效时默认以计算所得数值作为值域文字显示，可指定长度丿的文本数组显示简介的值域文本，如['髿, '使]＿\n'指定换行
        /// </summary>
        public virtual List<string> text()
        {
            if (text_Renamed == null)
            {
                text_Renamed = new List<string>();
            }
            return text_Renamed;
        }

        /// <summary>
        ///     值域文字显示，splitNumber生效时默认以计算所得数值作为值域文字显示，可指定长度丿的文本数组显示简介的值域文本，如['髿, '使]＿\n'指定换行
        /// </summary>
        /// <param name="texts">
        ///     @return
        /// </param>
        public virtual DataRange text(params string[] texts)
        {
            if (texts == null || texts.Length == 0)
            {
                return this;
            }
            text().AddRange(texts.ToList());
            return this;
        }

        /// <summary>
        ///     默认只设定了值域控件文字颜色
        /// </summary>
        public virtual TextStyle textStyle()
        {
            if (textStyle_Renamed == null)
            {
                textStyle_Renamed = new TextStyle();
            }
            return textStyle_Renamed;
        }
    }
}