using System;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     坐标轴指示器，坐标轴触发有效
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class AxisPointer
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     设置十字准星指示噿
        /// </summary>
        private CrossStyle crossStyle_Renamed;

        /// <summary>
        ///     设置直线指示噿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        private LineStyle lineStyle_Renamed;

        /// <summary>
        ///     设置阴影指示噿
        /// </summary>
        private ShadowStyle shadowStyle_Renamed;

        /// <summary>
        ///     默认为直线，可选为＿line' | 'shadow' | 'cross'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.PointerType
        /// </seealso>
        private PointerType type_Renamed;

        /// <summary>
        ///     获取lineStyle值
        /// </summary>
        public virtual LineStyle LineStyle
        {
            get { return lineStyle_Renamed; }
            set { lineStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取crossStyle值
        /// </summary>
        public virtual CrossStyle CrossStyle
        {
            get { return crossStyle_Renamed; }
            set { crossStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取shadowStyle值
        /// </summary>
        public virtual ShadowStyle ShadowStyle
        {
            get { return shadowStyle_Renamed; }
            set { shadowStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual PointerType Type
        {
            get { return type_Renamed; }
            set { type_Renamed = value; }
        }

        /// <summary>
        ///     设置lineStyle值     *
        /// </summary>
        /// <param name="lineStyle"> </param>
        public virtual AxisPointer lineStyle(LineStyle lineStyle)
        {
            lineStyle_Renamed = lineStyle;
            return this;
        }

        /// <summary>
        ///     设置crossStyle值     *
        /// </summary>
        /// <param name="crossStyle"> </param>
        public virtual AxisPointer crossStyle(CrossStyle crossStyle)
        {
            crossStyle_Renamed = crossStyle;
            return this;
        }

        /// <summary>
        ///     设置shadowStyle值     *
        /// </summary>
        /// <param name="shadowStyle"> </param>
        public virtual AxisPointer shadowStyle(ShadowStyle shadowStyle)
        {
            shadowStyle_Renamed = shadowStyle;
            return this;
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual PointerType type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual AxisPointer type(PointerType type)
        {
            type_Renamed = type;
            return this;
        }

        /// <summary>
        ///     设置直线指示噿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        public virtual LineStyle lineStyle()
        {
            if (lineStyle_Renamed == null)
            {
                lineStyle_Renamed = new LineStyle();
            }
            return lineStyle_Renamed;
        }

        /// <summary>
        ///     设置十字准星指示噿
        /// </summary>
        public virtual CrossStyle crossStyle()
        {
            if (crossStyle_Renamed == null)
            {
                crossStyle_Renamed = new CrossStyle();
            }
            return crossStyle_Renamed;
        }

        /// <summary>
        ///     设置阴影指示噿
        /// </summary>
        public virtual ShadowStyle shadowStyle()
        {
            if (shadowStyle_Renamed == null)
            {
                shadowStyle_Renamed = new ShadowStyle();
            }
            return shadowStyle_Renamed;
        }
    }
}