/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using System;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     配置信息
    ///     @author Yongjin.C
    /// </summary>
    public abstract class Config
    {
        public String CHART_TYPE_BAR = "bar";
        public String CHART_TYPE_CHORD = "chord";
        public String CHART_TYPE_FORCE = "force";
        public String CHART_TYPE_FUNNEL = "funnel";
        public String CHART_TYPE_GAUGE = "gauge";
        public String CHART_TYPE_ISLAND = "island";
        public String CHART_TYPE_K = "k";
        public String CHART_TYPE_LINE = "line";
        public String CHART_TYPE_MAP = "map";
        public String CHART_TYPE_PIE = "pie";
        public String CHART_TYPE_RADAR = "radar";
        public String CHART_TYPE_SCATTER = "scatter";
        public String COMPONENT_TYPE_AXIS = "axis";
        public String COMPONENT_TYPE_AXIS_CATEGORY = "categoryAxis";
        public String COMPONENT_TYPE_AXIS_VALUE = "valueAxis";
        public String COMPONENT_TYPE_DATARANGE = "dataRange";
        public String COMPONENT_TYPE_DATAVIEW = "dataView";
        public String COMPONENT_TYPE_DATAZOOM = "dataZoom";
        public String COMPONENT_TYPE_GRID = "grid";
        public String COMPONENT_TYPE_LEGEND = "legend";
        public String COMPONENT_TYPE_POLAR = "polar";
        public String COMPONENT_TYPE_TIMELINE = "timeline";
        public String COMPONENT_TYPE_TITLE = "title";
        public String COMPONENT_TYPE_TOOLBOX = "toolbox";
        public String COMPONENT_TYPE_TOOLTIP = "tooltip";
        public String COMPONENT_TYPE_X_AXIS = "xAxis";
        public String COMPONENT_TYPE_Y_AXIS = "yAxis";

        public Int32 DRAG_ENABLE_TIME = 120; // 降低图表内元素拖拽敏感度，单位ms，不建议外部干预
        public Int32 EFFECT_ZLEVEL = 7;
        public Boolean addDataAnimation = true; // 动态数据接口是否开启动画效果
        public Boolean animation = true;
        public Int32 animationDuration = 2000;
        public String animationEasing = "ExponentialOut"; //BounceOut
        public Int32 animationThreshold = 2000; // 动画元素阀值，产生的图形原素超过2000不出动画
        public String backgroundColor = "rgba(0,0,0,0)";

        public Boolean calculable = false; // 默认关闭可计算特性
        public String calculableColor = "rgba(255,165,0,0.6)"; // 拖拽提示边框颜色
        public String calculableHolderColor = "#ccc"; // 可计算占位提示颜色

        public String[] color =
        {
            "#ff7f50", "#87cefa", "#da70d6", "#32cd32", "#6495ed",
            "#ff69b4", "#ba55d3", "#cd5c5c", "#ffa500", "#40e0d0",
            "#1e90ff", "#ff6347", "#7b68ee", "#00fa9a", "#ffd700",
            "#6699FF", "#ff6666", "#3cb371", "#b8860b", "#30e0e0"
        };

        public String loadingText = "Loading...";

        public String nameConnector = " & ";

        public String[] symbolList =
        {
            "circle", "rectangle", "triangle", "diamond",
            "emptyCircle", "emptyRectangle", "emptyTriangle", "emptyDiamond"
        };

        public String valueConnector = "=";
    }
}