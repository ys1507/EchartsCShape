/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//using System.Web.UI.DataVisualization.Charting;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     Description: Option
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Option
    {
        #region property

        private const long serialVersionUID = 1L;

        /// <summary>
        ///     是否开启动画，默认开启，（详觿animation，相关的还有 addDataAnimation＿animationThreshold＿animationDuration＿animationEasing＿
        /// </summary>
        private bool? animation_Renamed;

        /// <summary>
        ///     全图默认背景，（详见backgroundColor），默认为无，透明
        /// </summary>
        private object backgroundColor_Renamed;

        /// <summary>
        ///     是否启用拖拽重计算特性，默认关闭，（详见calculable，相关的还有 calculableColor＿calculableHolderColor＿nameConnector＿valueConnector＿
        /// </summary>
        private bool? calculable_Renamed;

        /// <summary>
        ///     数值系列的颜色列表，（详见color），可配数组，eg：['#87cefa', 'rgba(123,123,123,0.5)','...']，当系列数量个数比颜色列表长度大时将循环选取
        /// </summary>
        private List<object> color_Renamed;

        /// <summary>
        ///     值域选择（详见dataRange＿值域范围
        /// </summary>
        private DataRange dataRange_Renamed;

        /// <summary>
        ///     数据区域缩放（详见dataZoom＿数据展现范围选择
        /// </summary>
        private DataZoom dataZoom_Renamed;

        /// <summary>
        ///     直角坐标系内绘图网格（详见grid＿
        /// </summary>
        private Grid grid_Renamed;

        private ItemStyle itemStyle_Renamed;


        /// <summary>
        ///     图例（详见legend），每个图表最多仅有一个图例，混搭图表共享
        /// </summary>
        private Legend legend_Renamed;

        /// <summary>
        ///     当使用timeline时，每一组数据要放到单独的option丿
        /// </summary>
        private List<Option> options_Renamed;

        /// <summary>
        ///     极坐栿
        /// </summary>
        private List<Polar> polar_Renamed;

        /// <summary>
        ///     非IE8-支持渲染为图片，（详见renderAsImage＿     * {boolean | string} false，非IE8-支持渲染为图片，可设为true或指定图片格式（png |
        ///     jpeg），渲染为图片后实例依然可用（如setOption，resize等），但各种交互失效
        /// </summary>
        private object renderAsImage_Renamed;

        /// <summary>
        ///     驱动图表生成的数据内容（详见series），数组中每一项代表一个系列的特殊选项及数捿
        /// </summary>
        private List<object> series_Renamed;

        /// <summary>
        ///     时间轴（详见timeline），每个图表最多仅有一个时间轴控件
        /// </summary>
        private Timeline timeline_Renamed;

        /// <summary>
        ///     标题（详见title），每个图表最多仅有一个标题控仿
        /// </summary>
        private Title title_Renamed;

        /// <summary>
        ///     工具箱（详见toolbox），每个图表最多仅有一个工具箱
        /// </summary>
        private Toolbox toolbox_Renamed;

        /// <summary>
        ///     提示框（详见tooltip），鼠标悬浮交互时的信息提示
        /// </summary>
        private Tooltip tooltip_Renamed;

        /// <summary>
        ///     直角坐标系中横轴数组（详见xAxis），数组中每一项代表一条横轴坐标轴，标准（1.0）中规定最多同时存圿条横轿
        /// </summary>
        private List<object> xAxis_Renamed;

        /// <summary>
        ///     直角坐标系中纵轴数组（详见yAxis），数组中每一项代表一条纵轴坐标轴，标准（1.0）中规定最多同时存圿条纵轿
        /// </summary>
        private List<object> yAxis_Renamed;

        #endregion

        #region property get set

        /// <summary>
        ///     值域选择（详见dataRange＿值域范围
        /// </summary>
        public DataRange dataRange
        {
            set { dataRange_Renamed = value; }
            get
            {
                //if (dataRange_Renamed == null)
                //{
                //    dataRange_Renamed = new DataRange();
                //}
                return dataRange_Renamed;
            }
        }

        /// <summary>
        ///     数据区域缩放（详见dataZoom＿数据展现范围选择
        /// </summary>
        public DataZoom dataZoom
        {
            set { dataZoom_Renamed = value; }
            get
            {
                //if (dataZoom_Renamed == null)
                //{
                //    dataZoom_Renamed = new DataZoom();
                //}
                return dataZoom_Renamed;
            }
        }

        /// <summary>
        ///     直角坐标系内绘图网格（详见grid＿
        /// </summary>
        public Grid grid
        {
            set { grid_Renamed = value; }
            get
            {
                //if (grid_Renamed == null)
                //{
                //    grid_Renamed = new Grid();
                //}
                return grid_Renamed;
            }
        }

        /// <summary>
        ///     图例（详见legend），每个图表最多仅有一个图例，混搭图表共享
        /// </summary>
        public Legend legend
        {
            set { legend_Renamed = value; }
            get
            {
                //if (legend_Renamed == null)
                //{
                //    legend_Renamed = new Legend();
                //}
                return legend_Renamed;
            }
        }

        /// <summary>
        ///     时间轴（详见timeline），每个图表最多仅有一个时间轴控件
        /// </summary>
        public Timeline timeline
        {
            set { timeline_Renamed = value; }
            get
            {
                //{
                //    if (timeline_Renamed == null)
                //    {
                //        timeline_Renamed = new Timeline();
                //    }
                return timeline_Renamed;
            }
        }

        /// <summary>
        ///     标题（详见title），每个图表最多仅有一个标题控仿
        /// </summary>
        public Title title
        {
            set { title_Renamed = value; }
            get
            {
                //    if (title_Renamed == null)
                //    {
                //        title_Renamed = new Title();
                //    }
                return title_Renamed;
            }
        }

        /// <summary>
        ///     工具箱（详见toolbox），每个图表最多仅有一个工具箱
        /// </summary>
        public Toolbox toolbox
        {
            set { toolbox_Renamed = value; }
            get
            {
                //if (toolbox_Renamed == null)
                //{
                //    toolbox_Renamed = new Toolbox();
                //}
                return toolbox_Renamed;
            }
        }

        /// <summary>
        ///     提示框（详见tooltip），鼠标悬浮交互时的信息提示
        /// </summary>
        public Tooltip tooltip
        {
            set { tooltip_Renamed = value; }
            get
            {
                //if (tooltip_Renamed == null)
                //{
                //    tooltip_Renamed = new Tooltip();
                //}
                return tooltip_Renamed;
            }
        }

        /// <summary>
        ///     获取backgroundColor值
        /// </summary>
        public virtual object backgroundColor
        {
            get { return backgroundColor_Renamed; }
            set { backgroundColor_Renamed = value; }
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual List<object> color
        {
            get
            {
                //if (color_Renamed == null)
                //{
                //    color_Renamed = new List<object>();
                //}
                return color_Renamed;
            }
            set { color_Renamed = value; }
        }

        /// <summary>
        ///     获取renderAsImage值
        /// </summary>
        public virtual object renderAsImage
        {
            get { return renderAsImage_Renamed; }
            set { renderAsImage_Renamed = value; }
        }


        /// <summary>
        ///     获取calculable值
        /// </summary>
        public virtual bool? calculable
        {
            get { return calculable_Renamed; }
            set { calculable_Renamed = value; }
        }


        /// <summary>
        ///     获取animation值
        /// </summary>
        public virtual bool? animation
        {
            get { return animation_Renamed; }
            set { animation_Renamed = value; }
        }

        /// <summary>
        ///     驱动图表生成的数据内容（详见series），数组中每一项代表一个系列的特殊选项及数捿
        /// </summary>
        public virtual List<object> series
        {
            get
            {
                //if (series_Renamed == null)
                //{
                //    series_Renamed = new List<object>();
                //}
                return series_Renamed;
            }
            set { series_Renamed = value; }
        }

        /// <summary>
        ///     当使用timeline时，每一组数据要放到单独的option丿
        /// </summary>
        public virtual List<Option> options
        {
            get
            {
                //if (options_Renamed == null)
                //{
                //    options_Renamed = new List<Option>();
                //}
                return options_Renamed;
            }
            set { options_Renamed = value; }
        }

        /// <summary>
        ///     获取polar值
        /// </summary>
        public virtual List<Polar> polar
        {
            get
            {
                //if (polar_Renamed == null)
                //{
                //    polar_Renamed = new List<Polar>();
                //}
                return polar_Renamed;
            }
            set { polar_Renamed = value; }
        }

        /// <summary>
        ///     获取itemStyle值
        /// </summary>
        public virtual ItemStyle itemStyle
        {
            get
            {
                //if (itemStyle_Renamed == null)
                //{
                //    itemStyle_Renamed = new ItemStyle();
                //}
                return itemStyle_Renamed;
            }
            set { itemStyle_Renamed = value; }
        }

        public List<object> yAxis
        {
            get
            {
                //if (yAxis_Renamed == null)
                //{
                //    yAxis_Renamed = new List<object>();
                //}
                return yAxis_Renamed;
            }
            set { yAxis_Renamed = value; }
        }

        public List<object> xAxis
        {
            get
            {
                //if (xAxis_Renamed == null)
                //{
                //    xAxis_Renamed = new List<object>();
                //}
                return xAxis_Renamed;
            }
            set { xAxis_Renamed = value; }
        }

        #endregion

        #region property with new

        /**
     * 直角坐标系内绘图网格（详见grid）
     */

        public Grid Grid()
        {
            return grid ?? (grid = new Grid());
        }

        public DataRange DataRange()
        {
            return dataRange_Renamed ?? (dataRange_Renamed = new DataRange());
        }

        public DataZoom DataZoom()
        {
            return dataZoom_Renamed ?? (dataZoom_Renamed = new DataZoom());
        }

        public Legend Legend()
        {
            return legend_Renamed ?? (legend_Renamed = new Legend());
        }

        public Timeline Timeline()
        {
            return timeline_Renamed ?? (timeline_Renamed = new Timeline());
        }

        public Title Title()
        {
            return title_Renamed ?? (title_Renamed = new Title());
        }

        public Toolbox Toolbox()
        {
            return toolbox_Renamed ?? (toolbox_Renamed = new Toolbox());
        }

        public Tooltip Tooltip()
        {
            return tooltip_Renamed ?? (tooltip_Renamed = new Tooltip());
        }

        public virtual List<object> Color()
        {
            return color_Renamed ?? (color_Renamed = new List<object>());
        }

        public virtual List<object> Series()
        {
            return series_Renamed ?? (series_Renamed = new List<object>());
        }

        public virtual List<Option> Options()
        {
            return options_Renamed ?? (options_Renamed = new List<Option>());
        }

        public virtual List<Polar> Polar()
        {
            return polar_Renamed ?? (polar_Renamed = new List<Polar>());
        }

        public virtual ItemStyle ItemStyle()
        {
            return itemStyle_Renamed ?? (itemStyle_Renamed = new ItemStyle());
        }

        #endregion

        #region property Option to line operate

        /// <summary>
        ///     设置itemStyle值     *
        /// </summary>
        /// <param name="itemStyle"> </param>
        public virtual Option ItemStyle(ItemStyle itemStyle)
        {
            itemStyle_Renamed = itemStyle;
            return this;
        }


        /// <summary>
        ///     设置polar值     *
        /// </summary>
        /// <param name="polar"> </param>
        public virtual Option Polar(List<Polar> polar)
        {
            polar_Renamed = polar;
            return this;
        }

        /// <summary>
        ///     设置values值     *
        /// </summary>
        /// <param name="values"> </param>
        public virtual Option Polar(params Polar[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            Polar().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     设置timeline值     *
        /// </summary>
        /// <param name="timeline"> </param>
        public virtual Option Timeline(Timeline timeline)
        {
            timeline_Renamed = timeline;
            return this;
        }

        /// <summary>
        ///     设置title值     *
        /// </summary>
        /// <param name="title"> </param>
        public virtual Option Title(Title title)
        {
            title_Renamed = title;
            return this;
        }

        /// <summary>
        ///     标题
        /// </summary>
        /// <param name="text">
        ///     @return
        /// </param>
        public virtual Option Title(string text)
        {
            Title().text(text);
            return this;
        }

        /// <summary>
        ///     标题和副标题
        /// </summary>
        /// <param name="text"> </param>
        /// <param name="subtext">
        ///     @return
        /// </param>
        public virtual Option Title(string text, string subtext)
        {
            Title().text(text).subtext(subtext);
            return this;
        }

        /// <summary>
        ///     设置toolbox值     *
        /// </summary>
        /// <param name="toolbox"> </param>
        public virtual Option Toolbox(Toolbox toolbox)
        {
            toolbox_Renamed = toolbox;
            return this;
        }

        /// <summary>
        ///     设置tooltip值     *
        /// </summary>
        /// <param name="tooltip"> </param>
        public virtual Option Tooltip(Tooltip tooltip)
        {
            tooltip_Renamed = tooltip;
            return this;
        }

        /// <summary>
        ///     设置trigger值     *
        /// </summary>
        /// <param name="trigger"> </param>
        public virtual Option Tooltip(Trigger trigger)
        {
            Tooltip().trigger(trigger);
            return this;
        }

        /// <summary>
        ///     设置legend值     *
        /// </summary>
        /// <param name="legend"> </param>
        public virtual Option Legend(Legend legend)
        {
            legend_Renamed = legend;
            return this;
        }

        /// <summary>
        ///     设置dataRange值     *
        /// </summary>
        /// <param name="dataRange"> </param>
        public virtual Option DataRange(DataRange dataRange)
        {
            dataRange_Renamed = dataRange;
            return this;
        }

        /// <summary>
        ///     设置dataZoom值     *
        /// </summary>
        /// <param name="dataZoom"> </param>
        public virtual Option DataZoom(DataZoom dataZoom)
        {
            dataZoom_Renamed = dataZoom;
            return this;
        }


        /// <summary>
        ///     设置grid值     *
        /// </summary>
        /// <param name="grid"> </param>
        public virtual Option Grid(Grid grid)
        {
            grid_Renamed = grid;
            return this;
        }

        /// <summary>
        ///     设置xAxis值     *
        /// </summary>
        /// <param name="xAxis"> </param>
        public virtual Option XAxis(List<object> xAxis)
        {
            xAxis_Renamed = xAxis;
            return this;
        }

        /// <summary>
        ///     设置yAxis值     *
        /// </summary>
        /// <param name="yAxis"> </param>
        public virtual Option YAxis(List<object> yAxis)
        {
            yAxis_Renamed = yAxis;
            return this;
        }


        /// <summary>
        ///     设置series值     *
        /// </summary>
        /// <param name="series"> </param>
        public virtual Option Series(List<object> series)
        {
            series_Renamed = series;
            return this;
        }

        /// <summary>
        ///     设置options值     *
        /// </summary>
        /// <param name="options"> </param>
        public virtual Option Options(List<Option> options)
        {
            options_Renamed = options;
            return this;
        }


        /// <summary>
        ///     设置backgroundColor值     *
        /// </summary>
        /// <param name="backgroundColor"> </param>
        public virtual Option BackgroundColor(object backgroundColor)
        {
            backgroundColor_Renamed = backgroundColor;
            return this;
        }


        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="colors"> </param>
        public virtual Option Color(params object[] colors)
        {
            if (colors == null || colors.Length == 0)
            {
                return this;
            }
            Color().AddRange(colors.ToList());
            return this;
        }


        /// <summary>
        ///     设置renderAsImage值     *
        /// </summary>
        /// <param name="renderAsImage"> </param>
        public virtual Option RenderAsImage(object renderAsImage)
        {
            renderAsImage_Renamed = renderAsImage;
            return this;
        }


        /// <summary>
        ///     设置calculable值     *
        /// </summary>
        /// <param name="calculable"> </param>
        public virtual Option Calculable(bool? calculable)
        {
            calculable_Renamed = calculable;
            return this;
        }


        /// <summary>
        ///     设置animation值     *
        /// </summary>
        /// <param name="animation"> </param>
        public virtual Option Animation(bool? animationvalue)
        {
            animation_Renamed = animationvalue;
            return this;
        }

        /// <summary>
        ///     添加图例（详见legend），每个图表最多仅有一个图例，混搭图表共享
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Option Legend(params object[] values)
        {
            Legend().data(values);
            return this;
        }


        /// <summary>
        ///     直角坐标系中横轴数组（详见xAxis），数组中每一项代表一条横轴坐标轴，标准（1.0）中规定最多同时存圿条横轿
        /// </summary>
        public virtual List<object> XAxis()
        {
            if (xAxis_Renamed == null)
            {
                xAxis_Renamed = new List<object>();
            }
            return xAxis_Renamed;
        }

        /// <summary>
        ///     添加x轿     *
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Option XAxis(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            if (XAxis().Count == 2)
            {
                throw new Exception("xAxis已经存在2个，无法继续添加!");
            }
            if (XAxis().Count + values.Length > 2)
            {
                throw new Exception("添加的xAxis超出了最大允许的范围:2!");
            }
            XAxis().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     直角坐标系中横轴数组（详见xAxis），数组中每一项代表一条横轴坐标轴，标准（1.0）中规定最多同时存圿条横轿
        /// </summary>
        public virtual List<object> YAxis()
        {
            if (yAxis_Renamed == null)
            {
                yAxis_Renamed = new List<object>();
            }
            return yAxis_Renamed;
        }

        /// <summary>
        ///     添加y轿     *
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Option YAxis(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            if (YAxis().Count == 2)
            {
                throw new Exception("yAxis已经存在2个，无法继续添加!");
            }
            if (YAxis().Count + values.Length > 2)
            {
                throw new Exception("添加的yAxis超出了最大允许的范围:2!");
            }
            YAxis().AddRange(values.ToList());
            return this;
        }


        /// <summary>
        ///     添加数据
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Option Series(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            Series().AddRange(values.ToList());
            return this;
        }


        /// <summary>
        ///     添加Option数据
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Option Options(params Option[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            Options().AddRange(values.ToList());
            return this;
        }

        #endregion

        /// <summary>
        /// 序列化,处理字符串中的function和(function(){})()，除{}中的代码外，其他地方不允许有空格
        /// </summary>
        /// <returns>json str</returns>
        public override string ToString()
        {
            var jSetting = new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore}; //去空值
            jSetting.Converters.Add(new StringEnumConverter()); //添加序列化 枚举转化
            jSetting.ContractResolver = new CamelCasePropertyNamesContractResolver(); //首字母小写
            return ReplaceFunctionFix(JsonConvert.SerializeObject(this, jSetting));
        }

        private static String ReplaceFunctionFix(String json)
        {
            Regex regexfunction = new Regex("(\\\")(\\(|)function\\(.*?\\){(.*?)(}|}\\)\\(\\))(\\\")");
            return regexfunction.Replace(json, match => match.Value.Trim('\"'));
        }
    }
}