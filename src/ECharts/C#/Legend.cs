/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System.Collections.Generic;
using System.Linq;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    public class Legend : Basic<Legend>, Data<Legend>, Component
    {
        /// <summary>
        ///     图例内容数组，数组项通常为{string}，每一项代表一个系列的name?     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.data.LegendData
        /// </seealso>
        private List<object> data_Renamed;

        /// <summary>
        ///     图例图形高度
        /// </summary>
        private int? itemHeight_Renamed;

        /// <summary>
        ///     图例图形宽度
        /// </summary>
        private int? itemWidth_Renamed;

        /// <summary>
        ///     布局方式，默认为水平布局，可选为＿horizontal' | 'vertical'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Orient
        /// </seealso>
        private Orient orient_Renamed;

        /// <summary>
        ///     选择模式，默认开启图例开兿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.SelectedMode
        /// </seealso>
        private object selectedMode_Renamed;

        /// <summary>
        ///     文字样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        private TextStyle textStyle_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Legend()
        {
        }

        /// <summary>
        ///     构造函敿参数:values
        /// </summary>
        /// <param name="values"> </param>
        public Legend(params object[] values)
        {
            data(values);
        }

        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取data值
        /// </summary>
        public virtual List<object> Data
        {
            get { return data_Renamed; }
            set { data_Renamed = value; }
        }


        /// <summary>
        ///     获取orient值
        /// </summary>
        public virtual Orient Orient
        {
            get { return orient_Renamed; }
            set { orient_Renamed = value; }
        }


        /// <summary>
        ///     获取itemWidth值
        /// </summary>
        public virtual int? ItemWidth
        {
            get { return itemWidth_Renamed; }
            set { itemWidth_Renamed = value; }
        }


        /// <summary>
        ///     获取itemHeight值
        /// </summary>
        public virtual int? ItemHeight
        {
            get { return itemHeight_Renamed; }
            set { itemHeight_Renamed = value; }
        }


        /// <summary>
        ///     获取selectedMode值
        /// </summary>
        public virtual object SelectedMode
        {
            get { return selectedMode_Renamed; }
            set { selectedMode_Renamed = value; }
        }

        /// <summary>
        ///     添加图例属怿     *
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Legend data(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            data().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     设置textStyle值     *
        /// </summary>
        /// <param name="textStyle"> </param>
        public virtual Legend textStyle(TextStyle textStyle)
        {
            textStyle_Renamed = textStyle;
            return this;
        }

        /// <summary>
        ///     设置data值     *
        /// </summary>
        /// <param name="data"> </param>
        public virtual Legend data(List<object> data)
        {
            data_Renamed = data;
            return this;
        }

        /// <summary>
        ///     获取orient值
        /// </summary>
        public virtual Orient orient()
        {
            return orient_Renamed;
        }

        /// <summary>
        ///     设置orient值     *
        /// </summary>
        /// <param name="orient"> </param>
        public virtual Legend orient(Orient orient)
        {
            orient_Renamed = orient;
            return this;
        }

        /// <summary>
        ///     获取itemWidth值
        /// </summary>
        public virtual int? itemWidth()
        {
            return itemWidth_Renamed;
        }

        /// <summary>
        ///     设置itemWidth值     *
        /// </summary>
        /// <param name="itemWidth"> </param>
        public virtual Legend itemWidth(int? itemWidth)
        {
            itemWidth_Renamed = itemWidth;
            return this;
        }

        /// <summary>
        ///     获取itemHeight值
        /// </summary>
        public virtual int? itemHeight()
        {
            return itemHeight_Renamed;
        }

        /// <summary>
        ///     设置itemHeight值     *
        /// </summary>
        /// <param name="itemHeight"> </param>
        public virtual Legend itemHeight(int? itemHeight)
        {
            itemHeight_Renamed = itemHeight;
            return this;
        }

        /// <summary>
        ///     文字样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        public virtual TextStyle textStyle()
        {
            if (textStyle_Renamed == null)
            {
                textStyle_Renamed = new TextStyle();
            }
            return textStyle_Renamed;
        }

        /// <summary>
        ///     获取selectedMode值
        /// </summary>
        public virtual object selectedMode()
        {
            return selectedMode_Renamed;
        }

        /// <summary>
        ///     设置selectedMode值     *
        /// </summary>
        /// <param name="selectedMode"> </param>
        public virtual Legend selectedMode(object selectedMode)
        {
            selectedMode_Renamed = selectedMode;
            return this;
        }

        /// <summary>
        ///     获取data值
        /// </summary>
        public virtual List<object> data()
        {
            if (data_Renamed == null)
            {
                data_Renamed = new List<object>();
            }
            return data_Renamed;
        }
    }
}