/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    public class Tooltip : Basic<Tooltip>, Component
    {
        /// <summary>
        ///     坐标轴指示器，坐标轴触发有效
        /// </summary>
        private AxisPointer axisPointer_Renamed;

        /// <summary>
        ///     提示边框圆角，单位px，默认为4
        /// </summary>
        private int? borderRadius_Renamed;

        /// <summary>
        ///     2.1.9新增属性，默认true，含义未矿     *
        ///     @since 2.1.9
        /// </summary>
        private bool? enterable_Renamed;

        /// <summary>
        ///     位置指定，传入{Array}，如[x, y]＿固定位置[x, y]；传入{Function}，如function([x, y]) {return [newX,newY]}，默认显示坐标为输入参数，用户指定的新坐标为输出返回?
        /// </summary>
        private object formatter_Renamed;

        /// <summary>
        ///     默认100，隐藏延迟，单位ms
        /// </summary>
        private int? hideDelay_Renamed;

        /// <summary>
        ///     内容格式器：{string}（Template＿| {Function}，支持异步回调见表格下方
        /// </summary>
        private string islandFormatter_Renamed;

        /// 
        private object position_Renamed;

        /// <summary>
        ///     tooltip主体内容显示策略，只需tooltip触发事件或显示axisPointer而不需要显示内容时可配置该项为falase＿     * 可选为：true（显示） | false（隐藏）
        /// </summary>
        private bool? showContent_Renamed;

        /// <summary>
        ///     默认20，显示延迟，添加显示延迟可以避免频繁切换，特别是在详情内容需要异步获取的场景，单位ms
        /// </summary>
        private int? showDelay_Renamed;

        /// <summary>
        ///     文本样式，默认为白色字体
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        private TextStyle textStyle_Renamed;

        /// <summary>
        ///     动画变换时长，单位s，如果你希望tooltip的跟随实时响应，showDelay设置丿是关键，同时transitionDuration访也会有交互体验上的差刿
        /// </summary>
        private double? transitionDuration_Renamed;

        /// <summary>
        ///     触发类型，默认数据触发，见下图，可选为＿item' | 'axis'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Trigger
        /// </seealso>
        private Trigger trigger_Renamed;

        /// <summary>
        ///     获取showContent值
        /// </summary>
        public virtual bool? ShowContent
        {
            get { return showContent_Renamed; }
            set { showContent_Renamed = value; }
        }


        /// <summary>
        ///     获取trigger值
        /// </summary>
        public virtual Trigger Trigger
        {
            get { return trigger_Renamed; }
            set { trigger_Renamed = value; }
        }


        /// <summary>
        ///     获取position值
        /// </summary>
        public virtual object Position
        {
            get { return position_Renamed; }
            set { position_Renamed = value; }
        }


        /// <summary>
        ///     获取formatter值
        /// </summary>
        public virtual object Formatter
        {
            get { return formatter_Renamed; }
            set { formatter_Renamed = value; }
        }


        /// <summary>
        ///     获取islandFormatter值
        /// </summary>
        public virtual string IslandFormatter
        {
            get { return islandFormatter_Renamed; }
            set { islandFormatter_Renamed = value; }
        }


        /// <summary>
        ///     获取showDelay值
        /// </summary>
        public virtual int? ShowDelay
        {
            get { return showDelay_Renamed; }
            set { showDelay_Renamed = value; }
        }


        /// <summary>
        ///     获取hideDelay值
        /// </summary>
        public virtual int? HideDelay
        {
            get { return hideDelay_Renamed; }
            set { hideDelay_Renamed = value; }
        }


        /// <summary>
        ///     获取transitionDuration值
        /// </summary>
        public virtual double? TransitionDuration
        {
            get { return transitionDuration_Renamed; }
            set { transitionDuration_Renamed = value; }
        }


        public virtual bool? Enterable
        {
            get { return enterable_Renamed; }
            set { enterable_Renamed = value; }
        }


        /// <summary>
        ///     获取borderRadius值
        /// </summary>
        public virtual int? BorderRadius
        {
            get { return borderRadius_Renamed; }
            set { borderRadius_Renamed = value; }
        }

        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }

        /// <summary>
        ///     设置axisPointer值     *
        /// </summary>
        /// <param name="axisPointer"> </param>
        public virtual Tooltip axisPointer(AxisPointer axisPointer)
        {
            axisPointer_Renamed = axisPointer;
            return this;
        }

        /// <summary>
        ///     设置textStyle值     *
        /// </summary>
        /// <param name="textStyle"> </param>
        public virtual Tooltip textStyle(TextStyle textStyle)
        {
            textStyle_Renamed = textStyle;
            return this;
        }

        /// <summary>
        ///     获取showContent值
        /// </summary>
        public virtual bool? showContent()
        {
            return showContent_Renamed;
        }

        /// <summary>
        ///     设置showContent值     *
        /// </summary>
        /// <param name="showContent"> </param>
        public virtual Tooltip showContent(bool? showContent)
        {
            showContent_Renamed = showContent;
            return this;
        }

        /// <summary>
        ///     获取trigger值
        /// </summary>
        public virtual Trigger trigger()
        {
            return trigger_Renamed;
        }

        /// <summary>
        ///     设置trigger值     *
        /// </summary>
        /// <param name="trigger"> </param>
        public virtual Tooltip trigger(Trigger trigger)
        {
            trigger_Renamed = trigger;
            return this;
        }

        /// <summary>
        ///     获取position值
        /// </summary>
        public virtual object position()
        {
            return position_Renamed;
        }

        /// <summary>
        ///     设置position值     *
        /// </summary>
        /// <param name="position"> </param>
        public virtual Tooltip position(object position)
        {
            position_Renamed = position;
            return this;
        }

        /// <summary>
        ///     获取formatter值
        /// </summary>
        public virtual object formatter()
        {
            return formatter_Renamed;
        }

        /// <summary>
        ///     设置formatter值     *
        /// </summary>
        /// <param name="formatter"> </param>
        public virtual Tooltip formatter(object formatter)
        {
            formatter_Renamed = formatter;
            return this;
        }

        /// <summary>
        ///     获取islandFormatter值
        /// </summary>
        public virtual string islandFormatter()
        {
            return islandFormatter_Renamed;
        }

        /// <summary>
        ///     设置islandFormatter值     *
        /// </summary>
        /// <param name="islandFormatter"> </param>
        public virtual Tooltip islandFormatter(string islandFormatter)
        {
            islandFormatter_Renamed = islandFormatter;
            return this;
        }

        /// <summary>
        ///     获取showDelay值
        /// </summary>
        public virtual int? showDelay()
        {
            return showDelay_Renamed;
        }

        /// <summary>
        ///     设置showDelay值     *
        /// </summary>
        /// <param name="showDelay"> </param>
        public virtual Tooltip showDelay(int? showDelay)
        {
            showDelay_Renamed = showDelay;
            return this;
        }

        /// <summary>
        ///     获取hideDelay值
        /// </summary>
        public virtual int? hideDelay()
        {
            return hideDelay_Renamed;
        }

        /// <summary>
        ///     设置hideDelay值     *
        /// </summary>
        /// <param name="hideDelay"> </param>
        public virtual Tooltip hideDelay(int? hideDelay)
        {
            hideDelay_Renamed = hideDelay;
            return this;
        }

        /// <summary>
        ///     获取transitionDuration值
        /// </summary>
        public virtual double? transitionDuration()
        {
            return transitionDuration_Renamed;
        }

        /// <summary>
        ///     设置transitionDuration值     *
        /// </summary>
        /// <param name="transitionDuration"> </param>
        public virtual Tooltip transitionDuration(double? transitionDuration)
        {
            transitionDuration_Renamed = transitionDuration;
            return this;
        }

        public virtual bool? enterable()
        {
            return enterable_Renamed;
        }

        public virtual Tooltip enterable(bool? enterable)
        {
            enterable_Renamed = enterable;
            return this;
        }

        /// <summary>
        ///     获取borderRadius值
        /// </summary>
        public virtual int? borderRadius()
        {
            return borderRadius_Renamed;
        }

        /// <summary>
        ///     设置borderRadius值     *
        /// </summary>
        /// <param name="borderRadius"> </param>
        public virtual Tooltip borderRadius(int? borderRadius)
        {
            borderRadius_Renamed = borderRadius;
            return this;
        }

        /// <summary>
        ///     坐标轴指示器，坐标轴触发有效
        /// </summary>
        public virtual AxisPointer axisPointer()
        {
            if (axisPointer_Renamed == null)
            {
                axisPointer_Renamed = new AxisPointer();
            }
            return axisPointer_Renamed;
        }

        /// <summary>
        ///     文本样式，默认为白色字体
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        public virtual TextStyle textStyle()
        {
            if (textStyle_Renamed == null)
            {
                textStyle_Renamed = new TextStyle();
            }
            return textStyle_Renamed;
        }


        /// <summary>
        ///     获取axisPointer值
        /// </summary>
        public virtual AxisPointer getAxisPointer()
        {
            return axisPointer_Renamed;
        }

        /// <summary>
        ///     设置axisPointer值     *
        /// </summary>
        /// <param name="axisPointer"> </param>
        public virtual void setAxisPointer(AxisPointer axisPointer)
        {
            axisPointer_Renamed = axisPointer;
        }
    }
}