using System;
using Yongjin.Cshape.echarts.code;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     地图位置设置，默认只适应上下左右居中可配x，y，width，height，任意参数为空都将根据其他参数自适应
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class MapLocation
    {
        private const long serialVersionUID = 1L;
        private object height_Renamed;
        private object width_Renamed;

        private object x_Renamed;
        private object y_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public MapLocation()
        {
        }

        /// <summary>
        ///     构造函敿参数:x,y
        /// </summary>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        public MapLocation(object x, object y)
        {
            x_Renamed = x;
            y_Renamed = y;
        }

        /// <summary>
        ///     构造函敿参数:x,y,width
        /// </summary>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        /// <param name="width"> </param>
        public MapLocation(object x, object y, object width)
        {
            x_Renamed = x;
            y_Renamed = y;
            width_Renamed = width;
        }

        /// <summary>
        ///     构造函敿参数:x,y,width,height
        /// </summary>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        /// <param name="width"> </param>
        /// <param name="height"> </param>
        public MapLocation(object x, object y, object width, object height)
        {
            x_Renamed = x;
            y_Renamed = y;
            width_Renamed = width;
            height_Renamed = height;
        }

        /// <summary>
        ///     构造函敿参数:x,y
        /// </summary>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        public MapLocation(X x, Y y)
        {
            x_Renamed = x;
            y_Renamed = y;
        }

        /// <summary>
        ///     构造函敿参数:x,y,width
        /// </summary>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        /// <param name="width"> </param>
        public MapLocation(X x, Y y, object width)
        {
            x_Renamed = x;
            y_Renamed = y;
            width_Renamed = width;
        }

        /// <summary>
        ///     构造函敿参数:x,y,width,height
        /// </summary>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        /// <param name="width"> </param>
        /// <param name="height"> </param>
        public MapLocation(X x, Y y, object width, object height)
        {
            x_Renamed = x;
            y_Renamed = y;
            width_Renamed = width;
            height_Renamed = height;
        }

        /// <summary>
        ///     获取x值
        /// </summary>
        public virtual object X
        {
            get { return x_Renamed; }
            set { x_Renamed = value; }
        }


        /// <summary>
        ///     获取y值
        /// </summary>
        public virtual object Y
        {
            get { return y_Renamed; }
            set { y_Renamed = value; }
        }


        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object Width
        {
            get { return width_Renamed; }
            set { width_Renamed = value; }
        }


        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual object Height
        {
            get { return height_Renamed; }
            set { height_Renamed = value; }
        }

        /// <summary>
        ///     获取x值
        /// </summary>
        public virtual object x()
        {
            return x_Renamed;
        }

        /// <summary>
        ///     设置x值     *
        /// </summary>
        /// <param name="x"> </param>
        public virtual MapLocation x(object x)
        {
            x_Renamed = x;
            return this;
        }

        /// <summary>
        ///     设置x值     *
        /// </summary>
        /// <param name="x"> </param>
        public virtual MapLocation x(X x)
        {
            x_Renamed = x;
            return this;
        }

        /// <summary>
        ///     获取y值
        /// </summary>
        public virtual object y()
        {
            return y_Renamed;
        }

        /// <summary>
        ///     设置y值     *
        /// </summary>
        /// <param name="y"> </param>
        public virtual MapLocation y(Y y)
        {
            y_Renamed = y;
            return this;
        }

        /// <summary>
        ///     设置y值     *
        /// </summary>
        /// <param name="y"> </param>
        public virtual MapLocation y(object y)
        {
            y_Renamed = y;
            return this;
        }

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object width()
        {
            return width_Renamed;
        }

        /// <summary>
        ///     设置width值     *
        /// </summary>
        /// <param name="width"> </param>
        public virtual MapLocation width(object width)
        {
            width_Renamed = width;
            return this;
        }

        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual object height()
        {
            return height_Renamed;
        }

        /// <summary>
        ///     设置height值     *
        /// </summary>
        /// <param name="height"> </param>
        public virtual MapLocation height(object height)
        {
            height_Renamed = height;
            return this;
        }
    }
}