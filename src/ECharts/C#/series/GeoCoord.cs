/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Collections.Generic;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     地图特有，标线图形定位坐栿 *
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class GeoCoord : Dictionary<string, Decimal[]>
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     设置key,x,y值     *
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        public virtual GeoCoord put(string key, string x, string y)
        {
            base[key] = new[] {Convert.ToDecimal(x), Convert.ToDecimal(y)};
            return this;
        }
    }
}