/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     Description: MarkLine
    ///     @author Yongjin.C
    /// </summary>
    public class MarkLine : AbstractData<MarkLine>
    {
        /// <summary>
        ///     边捆绿     *
        ///     @since 2.2.0
        /// </summary>
        private Bundling bundling_Renamed;

        /// <summary>
        ///     标线图形炫光特效
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Effect
        /// </seealso>
        private Effect effect_Renamed;

        /// <summary>
        ///     地图特有，标线图形定位坐栿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Map# geoCoord
        /// </seealso>
        private GeoCoord geoCoord_Renamed;

        /// <summary>
        ///     标线图形样式属怿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.ItemStyle
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Series# itemStyle
        /// </seealso>
        private ItemStyle itemStyle_Renamed;

        /// <summary>
        ///     小数精度，默访
        ///     @since 2.2.0
        /// </summary>
        private int? precision_Renamed;

        /// <summary>
        ///     平滑曲线
        /// </summary>
        private bool? smooth_Renamed;

        /// <summary>
        ///     平滑度，默认0.2
        ///     @since 2.2.0
        /// </summary>
        private double? smoothness_Renamed;

        /// <summary>
        ///     标线起始和结束的symbol旋转控制，同series中的symbolRotate
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Series# symbolRoate
        /// </seealso>
        private object symbolRoate_Renamed;

        /// <summary>
        ///     标线起始和结束的symbol大小，半宽（半径）参数，如果都一样，可以直接传number或function，同series中的symbolSize
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Series# symbolSize
        /// </seealso>
        private object symbolSize_Renamed;

        /// <summary>
        ///     标线起始和结束的symbol介绍类型，如果都一样，可以直接传string，同series中的symbol
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Symbol
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Series# symbol
        /// </seealso>
        private object symbol_Renamed;

        /// <summary>
        ///     获取itemStyle值
        /// </summary>
        public virtual ItemStyle ItemStyle
        {
            get { return itemStyle_Renamed; }
            set { itemStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object Symbol
        {
            get { return symbol_Renamed; }
            set { symbol_Renamed = value; }
        }


        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object SymbolSize
        {
            get { return symbolSize_Renamed; }
            set { symbolSize_Renamed = value; }
        }


        /// <summary>
        ///     获取symbolRoate值
        /// </summary>
        public virtual object SymbolRoate
        {
            get { return symbolRoate_Renamed; }
            set { symbolRoate_Renamed = value; }
        }

        /// <summary>
        ///     获取smooth值
        /// </summary>
        public virtual bool? Smooth
        {
            get { return smooth_Renamed; }
            set { smooth_Renamed = value; }
        }


        public virtual double? Smoothness
        {
            get { return smoothness_Renamed; }
            set { smoothness_Renamed = value; }
        }


        public virtual int? Precision
        {
            get { return precision_Renamed; }
            set { precision_Renamed = value; }
        }

        /// <summary>
        ///     获取边捆绿
        /// </summary>
        public virtual Bundling bundling()
        {
            if (bundling_Renamed == null)
            {
                bundling_Renamed = new Bundling();
            }
            return bundling_Renamed;
        }

        /// <summary>
        ///     设置边捆绿     *
        /// </summary>
        /// <param name="bundling"> </param>
        public virtual MarkLine bundling(Bundling bundling)
        {
            bundling_Renamed = bundling;
            return this;
        }

        /// <summary>
        ///     获取平滑庿
        /// </summary>
        public virtual double? smoothness()
        {
            return smoothness_Renamed;
        }

        /// <summary>
        ///     设置平滑庿     *
        /// </summary>
        /// <param name="smoothness"> </param>
        public virtual MarkLine smoothness(double? smoothness)
        {
            smoothness_Renamed = smoothness;
            return this;
        }

        /// <summary>
        ///     获取小数精度
        /// </summary>
        public virtual int? precision()
        {
            return precision_Renamed;
        }

        /// <summary>
        ///     设置小数精度
        /// </summary>
        /// <param name="precision"> </param>
        public virtual MarkLine precision(int? precision)
        {
            precision_Renamed = precision;
            return this;
        }

        /// <summary>
        ///     设置effect值     *
        /// </summary>
        /// <param name="effect"> </param>
        public virtual MarkLine effect(Effect effect)
        {
            effect_Renamed = effect;
            return this;
        }

        /// <summary>
        ///     设置itemStyle值     *
        /// </summary>
        /// <param name="itemStyle"> </param>
        public virtual MarkLine itemStyle(ItemStyle itemStyle)
        {
            itemStyle_Renamed = itemStyle;
            return this;
        }

        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object symbol()
        {
            return symbol_Renamed;
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual MarkLine symbol(object symbol)
        {
            symbol_Renamed = symbol;
            return this;
        }

        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object symbolSize()
        {
            return symbolSize_Renamed;
        }

        /// <summary>
        ///     设置symbolSize值     *
        /// </summary>
        /// <param name="symbolSize"> </param>
        public virtual MarkLine symbolSize(object symbolSize)
        {
            symbolSize_Renamed = symbolSize;
            return this;
        }

        /// <summary>
        ///     获取symbolRoate值
        /// </summary>
        public virtual object symbolRoate()
        {
            return symbolRoate_Renamed;
        }

        /// <summary>
        ///     设置symbolRoate值     *
        /// </summary>
        /// <param name="symbolRoate"> </param>
        public virtual MarkLine symbolRoate(object symbolRoate)
        {
            symbolRoate_Renamed = symbolRoate;
            return this;
        }

        /// <summary>
        ///     标线图形炫光特效
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Effect
        /// </seealso>
        public virtual Effect effect()
        {
            if (effect_Renamed == null)
            {
                effect_Renamed = new Effect();
            }
            return effect_Renamed;
        }

        /// <summary>
        ///     标线图形样式属怿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.ItemStyle
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Series# itemStyle
        /// </seealso>
        public virtual ItemStyle itemStyle()
        {
            if (itemStyle_Renamed == null)
            {
                itemStyle_Renamed = new ItemStyle();
            }
            return itemStyle_Renamed;
        }

        /// <summary>
        ///     获取geoCoord值
        /// </summary>
        public virtual GeoCoord geoCoord()
        {
            if (geoCoord_Renamed == null)
            {
                geoCoord_Renamed = new GeoCoord();
            }
            return geoCoord_Renamed;
        }

        /// <summary>
        ///     设置name,x,y值     *
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        public virtual MarkLine geoCoord(string name, string x, string y)
        {
            geoCoord().put(name, x, y);
            return this;
        }

        /// <summary>
        ///     获取smooth值
        /// </summary>
        public virtual bool? smooth()
        {
            return smooth_Renamed;
        }

        /// <summary>
        ///     设置smooth值     *
        /// </summary>
        /// <param name="smooth"> </param>
        public virtual MarkLine smooth(bool? smooth)
        {
            smooth_Renamed = smooth;
            return this;
        }

        /// <summary>
        ///     获取effect值
        /// </summary>
        public virtual Effect getEffect()
        {
            return effect_Renamed;
        }

        /// <summary>
        ///     设置effect值     *
        /// </summary>
        /// <param name="effect"> </param>
        public virtual void setEffect(Effect effect)
        {
            effect_Renamed = effect;
        }


        /// <summary>
        ///     获取geoCoord值
        /// </summary>
        public virtual GeoCoord getGeoCoord()
        {
            return geoCoord_Renamed;
        }

        /// <summary>
        ///     设置geoCoord值     *
        /// </summary>
        /// <param name="geoCoord"> </param>
        public virtual void setGeoCoord(GeoCoord geoCoord)
        {
            geoCoord_Renamed = geoCoord;
        }


        public virtual Bundling getBundling()
        {
            return bundling_Renamed;
        }

        public virtual void setBundling(Bundling bundling)
        {
            bundling_Renamed = bundling;
        }

        /// <summary>
        ///     边捆绿     *
        ///     @since 2.2.0
        /// </summary>
        public class Bundling
        {
            internal bool? enable_Renamed;
            internal int? maxTurningAngle_Renamed;

            public virtual bool? Enable
            {
                get { return enable_Renamed; }
                set { enable_Renamed = value; }
            }


            public virtual int? MaxTurningAngle
            {
                get { return maxTurningAngle_Renamed; }
                set { maxTurningAngle_Renamed = value; }
            }

            public virtual bool? enable()
            {
                return enable_Renamed;
            }

            public virtual Bundling enable(bool? enable)
            {
                enable_Renamed = enable;
                return this;
            }

            public virtual int? maxTurningAngle()
            {
                return maxTurningAngle_Renamed;
            }

            public virtual Bundling maxTurningAngle(int? maxTurningAngle)
            {
                maxTurningAngle_Renamed = maxTurningAngle;
                return this;
            }
        }
    }
}