/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    public class Line : Series<Line>
    {
        /// <summary>
        ///     平滑曲线
        /// </summary>
        private bool? smooth_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Line()
        {
            type(SeriesType.line);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Line(string name) : base(name)
        {
            type(SeriesType.line);
        }

        /// <summary>
        ///     获取smooth值
        /// </summary>
        public virtual bool? Smooth
        {
            get { return smooth_Renamed; }
            set { smooth_Renamed = value; }
        }

        /// <summary>
        ///     获取smooth值
        /// </summary>
        public virtual bool? smooth()
        {
            return smooth_Renamed;
        }

        /// <summary>
        ///     设置smooth值     *
        /// </summary>
        /// <param name="smooth"> </param>
        public virtual Line smooth(bool? smooth)
        {
            smooth_Renamed = smooth;
            return this;
        }
    }
}