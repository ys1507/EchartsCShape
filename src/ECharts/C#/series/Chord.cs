/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System.Collections.Generic;
using System.Linq;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.series.force;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     Description: Chord
    ///     @author Yongjin.C
    /// </summary>
    public class Chord : Series<Chord>
    {
        /// <summary>
        ///     力导向图中节点的分类
        /// </summary>
        private List<Category> categories_Renamed;

        /// <summary>
        ///     显示是否顺时钿
        /// </summary>
        private bool? clockWise_Renamed;

        /// <summary>
        ///     力导向图的边数据
        /// </summary>
        private List<Link> links_Renamed;

        /// <summary>
        ///     关系数据，用二维数组表示，项 [i][j] 的数值表礿i 刿j 的关系数捿
        /// </summary>
        private object[][] matrix_Renamed;

        /// <summary>
        ///     顶点数据映射成圆半径后的最大半徿
        /// </summary>
        private int? maxRadius_Renamed;

        /// <summary>
        ///     顶点数据映射成圆半径后的最小半徿
        /// </summary>
        private int? minRadius_Renamed;

        /// <summary>
        ///     力导向图的顶点数捿
        /// </summary>
        private List<Node> nodes_Renamed;

        /// <summary>
        ///     每个sector之间的间跿用角度表礿
        /// </summary>
        private int? padding_Renamed;

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        private object radius_Renamed;

        /// <summary>
        ///     ribbonType的和弦图节点使用扇形绘制，边使用有大小端的ribbon绘制，可以表示出边的权重，图的节点边之间必须是双向边，非ribbonType的和弦图节点使用symbol绘制，边使用贝塞尔曲线，不能表示边的权重，但是可以使用单向边
        /// </summary>
        private bool? ribbonType_Renamed;

        /// <summary>
        ///     是否显示刻度文字
        /// </summary>
        private bool? showScaleText_Renamed;

        /// <summary>
        ///     是否显示刻度
        /// </summary>
        private bool? showScale_Renamed;

        /// <summary>
        ///     数据排序（弦）， 可以取none, ascending, descending
        /// </summary>
        private Sort sortSub_Renamed;

        /// <summary>
        ///     数据排序＿可以取none, ascending, descending
        /// </summary>
        private Sort sort_Renamed;

        /// <summary>
        ///     开始角庿 有效输入范围：[-180,180]
        /// </summary>
        private int? startAngle_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Chord()
        {
            type(SeriesType.chord);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Chord(string name) : base(name)
        {
            type(SeriesType.chord);
        }

        public virtual List<Category> Categories
        {
            get { return categories_Renamed; }
            set { categories_Renamed = value; }
        }


        public virtual List<Node> Nodes
        {
            get { return nodes_Renamed; }
            set { nodes_Renamed = value; }
        }


        public virtual List<Link> Links
        {
            get { return links_Renamed; }
            set { links_Renamed = value; }
        }


        public virtual bool? RibbonType
        {
            get { return ribbonType_Renamed; }
            set { ribbonType_Renamed = value; }
        }


        /// <summary>
        ///     获取padding值
        /// </summary>
        public virtual int? Padding
        {
            get { return padding_Renamed; }
            set { padding_Renamed = value; }
        }


        /// <summary>
        ///     获取radius值
        /// </summary>
        public virtual object Radius
        {
            get { return radius_Renamed; }
            set { radius_Renamed = value; }
        }


        /// <summary>
        ///     获取startAngle值
        /// </summary>
        public virtual int? StartAngle
        {
            get { return startAngle_Renamed; }
            set { startAngle_Renamed = value; }
        }


        /// <summary>
        ///     获取sort值
        /// </summary>
        public virtual Sort Sort
        {
            get { return sort_Renamed; }
            set { sort_Renamed = value; }
        }


        /// <summary>
        ///     获取sortSub值
        /// </summary>
        public virtual Sort SortSub
        {
            get { return sortSub_Renamed; }
            set { sortSub_Renamed = value; }
        }


        /// <summary>
        ///     获取showScale值
        /// </summary>
        public virtual bool? ShowScale
        {
            get { return showScale_Renamed; }
            set { showScale_Renamed = value; }
        }


        /// <summary>
        ///     获取showScaleText值
        /// </summary>
        public virtual bool? ShowScaleText
        {
            get { return showScaleText_Renamed; }
            set { showScaleText_Renamed = value; }
        }


        /// <summary>
        ///     获取clockWise值
        /// </summary>
        public virtual bool? ClockWise
        {
            get { return clockWise_Renamed; }
            set { clockWise_Renamed = value; }
        }


        /// <summary>
        ///     获取matrix值
        /// </summary>
        public virtual object[][] Matrix
        {
            get { return matrix_Renamed; }
            set { matrix_Renamed = value; }
        }


        /// <summary>
        ///     获取minRadius值
        /// </summary>
        public virtual int? MinRadius
        {
            get { return minRadius_Renamed; }
            set { minRadius_Renamed = value; }
        }


        /// <summary>
        ///     获取maxRadius值     * @return
        /// </summary>
        public virtual int? MaxRadius
        {
            get { return maxRadius_Renamed; }
            set { maxRadius_Renamed = value; }
        }

        /// <summary>
        ///     设置categories值     *
        /// </summary>
        /// <param name="categories"> </param>
        public virtual Chord categories(List<Category> categories)
        {
            categories_Renamed = categories;
            return this;
        }

        /// <summary>
        ///     设置nodes值     *
        /// </summary>
        /// <param name="nodes"> </param>
        public virtual Chord nodes(List<Node> nodes)
        {
            nodes_Renamed = nodes;
            return this;
        }

        /// <summary>
        ///     设置links值     *
        /// </summary>
        /// <param name="links"> </param>
        public virtual Chord links(List<Link> links)
        {
            links_Renamed = links;
            return this;
        }

        /// <summary>
        ///     力导向图中节点的分类
        /// </summary>
        public virtual List<Category> categories()
        {
            if (categories_Renamed == null)
            {
                categories_Renamed = new List<Category>();
            }
            return categories_Renamed;
        }

        /// <summary>
        ///     添加节点分类
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Chord categories(params Category[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            categories().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     添加节点分类，使用分类名
        /// </summary>
        /// <param name="names">
        ///     @return
        /// </param>
        public virtual Chord categories(params string[] names)
        {
            if (names == null || names.Length == 0)
            {
                return this;
            }
            foreach (string name in names)
            {
                categories().Add(new Category(name));
            }
            return this;
        }

        /// <summary>
        ///     添加节点分类，使用分类名
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Chord categories(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            foreach (object value in values)
            {
                if (value is string)
                {
                    categories().Add(new Category((string) value));
                }
                else if (value is Category)
                {
                    categories().Add((Category) value);
                }
                //其他忽略
            }
            return this;
        }

        /// <summary>
        ///     力导向图的顶点数捿
        /// </summary>
        public virtual List<Node> nodes()
        {
            if (nodes_Renamed == null)
            {
                nodes_Renamed = new List<Node>();
            }
            return nodes_Renamed;
        }

        /// <summary>
        ///     添加力导向图的顶点数捿     *
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Chord nodes(params Node[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            nodes().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     力导向图的边数据
        /// </summary>
        public virtual List<Link> links()
        {
            if (links_Renamed == null)
            {
                links_Renamed = new List<Link>();
            }
            return links_Renamed;
        }

        /// <summary>
        ///     添加力导向图的边数据
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Chord links(params Link[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            links().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     获取ribbonType值     *
        ///     @return
        /// </summary>
        public virtual bool? ribbonType()
        {
            return ribbonType_Renamed;
        }

        /// <summary>
        ///     设置ribbonType值     *
        /// </summary>
        /// <param name="ribbonType">
        ///     @return
        /// </param>
        public virtual Chord ribbonType(bool? ribbonType)
        {
            ribbonType_Renamed = ribbonType;
            return this;
        }

        /// <summary>
        ///     获取padding值
        /// </summary>
        public virtual int? padding()
        {
            return padding_Renamed;
        }

        /// <summary>
        ///     设置padding值     *
        /// </summary>
        /// <param name="padding"> </param>
        public virtual Chord padding(int? padding)
        {
            padding_Renamed = padding;
            return this;
        }

        /// <summary>
        ///     获取radius值
        /// </summary>
        public virtual object radius()
        {
            return radius_Renamed;
        }

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        /// <param name="value">
        ///     @return
        /// </param>
        public virtual Chord radius(object value)
        {
            radius_Renamed = value;
            return this;
        }

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        /// <param name="width"> </param>
        /// <param name="height">
        ///     @return
        /// </param>
        public virtual Chord radius(object width, object height)
        {
            radius_Renamed = new[] {width, height};
            return this;
        }

        /// <summary>
        ///     获取startAngle值
        /// </summary>
        public virtual int? startAngle()
        {
            return startAngle_Renamed;
        }

        /// <summary>
        ///     设置startAngle值     *
        /// </summary>
        /// <param name="startAngle"> </param>
        public virtual Chord startAngle(int? startAngle)
        {
            startAngle_Renamed = startAngle;
            return this;
        }

        /// <summary>
        ///     获取sort值
        /// </summary>
        public virtual Sort sort()
        {
            return sort_Renamed;
        }

        /// <summary>
        ///     设置sort值     *
        /// </summary>
        /// <param name="sort"> </param>
        public virtual Chord sort(Sort sort)
        {
            sort_Renamed = sort;
            return this;
        }

        /// <summary>
        ///     获取sortSub值
        /// </summary>
        public virtual Sort sortSub()
        {
            return sortSub_Renamed;
        }

        /// <summary>
        ///     设置sortSub值     *
        /// </summary>
        /// <param name="sortSub"> </param>
        public virtual Chord sortSub(Sort sortSub)
        {
            sortSub_Renamed = sortSub;
            return this;
        }

        /// <summary>
        ///     获取showScale值
        /// </summary>
        public virtual bool? showScale()
        {
            return showScale_Renamed;
        }

        /// <summary>
        ///     设置showScale值     *
        /// </summary>
        /// <param name="showScale"> </param>
        public virtual Chord showScale(bool? showScale)
        {
            showScale_Renamed = showScale;
            return this;
        }

        /// <summary>
        ///     获取showScaleText值
        /// </summary>
        public virtual bool? showScaleText()
        {
            return showScaleText_Renamed;
        }

        /// <summary>
        ///     设置showScaleText值     *
        /// </summary>
        /// <param name="showScaleText"> </param>
        public virtual Chord showScaleText(bool? showScaleText)
        {
            showScaleText_Renamed = showScaleText;
            return this;
        }

        /// <summary>
        ///     获取clockWise值
        /// </summary>
        public virtual bool? clockWise()
        {
            return clockWise_Renamed;
        }

        /// <summary>
        ///     设置clockWise值     *
        /// </summary>
        /// <param name="clockWise"> </param>
        public virtual Chord clockWise(bool? clockWise)
        {
            clockWise_Renamed = clockWise;
            return this;
        }

        /// <summary>
        ///     获取minRadius值
        /// </summary>
        public virtual int? minRadius()
        {
            return minRadius_Renamed;
        }

        /// <summary>
        ///     设置minRadius值     *
        /// </summary>
        /// <param name="minRadius"> </param>
        public virtual Chord minRadius(int? minRadius)
        {
            minRadius_Renamed = minRadius;
            return this;
        }

        /// <summary>
        ///     获取maxRadius值
        /// </summary>
        public virtual int? maxRadius()
        {
            return maxRadius_Renamed;
        }

        /// <summary>
        ///     设置maxRadius值     *
        /// </summary>
        /// <param name="maxRadius"> </param>
        public virtual Chord maxRadius(int? maxRadius)
        {
            maxRadius_Renamed = maxRadius;
            return this;
        }

        /// <summary>
        ///     获取matrix值
        /// </summary>
        public virtual object[][] matrix()
        {
            return matrix_Renamed;
        }

        /// <summary>
        ///     设置matrix值     *
        /// </summary>
        /// <param name="matrix"> </param>
        public virtual Chord matrix(object[][] matrix)
        {
            matrix_Renamed = matrix;
            return this;
        }
    }
}