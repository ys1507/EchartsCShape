/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    public class Island : Series<Island>
    {
        /// <summary>
        ///     滚轮可计算步长0.1 = 10%
        /// </summary>
        private object calculateStep_Renamed;

        private object r_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Island()
        {
            type(SeriesType.island);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Island(string name) : base(name)
        {
            type(SeriesType.island);
        }

        /// <summary>
        ///     获取r值
        /// </summary>
        public virtual object R
        {
            get { return r_Renamed; }
            set { r_Renamed = value; }
        }


        /// <summary>
        ///     获取calculateStep值
        /// </summary>
        public virtual object CalculateStep
        {
            get { return calculateStep_Renamed; }
            set { calculateStep_Renamed = value; }
        }

        /// <summary>
        ///     获取r值
        /// </summary>
        public virtual object r()
        {
            return r_Renamed;
        }

        /// <summary>
        ///     设置r值     *
        /// </summary>
        /// <param name="r"> </param>
        public virtual Island r(object r)
        {
            r_Renamed = r;
            return this;
        }

        /// <summary>
        ///     获取calculateStep值
        /// </summary>
        public virtual object calculateStep()
        {
            return calculateStep_Renamed;
        }

        /// <summary>
        ///     设置calculateStep值     *
        /// </summary>
        /// <param name="calculateStep"> </param>
        public virtual Island calculateStep(object calculateStep)
        {
            calculateStep_Renamed = calculateStep;
            return this;
        }
    }
}