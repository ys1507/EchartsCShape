using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     图形炫光特效
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Effect
    {
        public enum Type
        {
            scale,
            bounce
        }

        private const long serialVersionUID = 1L;

        /// <summary>
        ///     跳动距离，单位为px，type为bounce时有敿
        /// </summary>
        private int? bounceDistance_Renamed;

        /// <summary>
        ///     炫光颜色，默认跟随markPoint itemStyle定义颜色
        /// </summary>
        private string color_Renamed;

        /// <summary>
        ///     循环动画，默认开吿
        /// </summary>
        private bool? loop_Renamed;

        /// <summary>
        ///     运动周期，无单位，值越大越慢，默认丿5
        /// </summary>
        private int? period_Renamed;

        /// <summary>
        ///     放大倍数，以markPoint symbolSize为基凿
        /// </summary>
        private int? scaleSize_Renamed;

        /// <summary>
        ///     光影模糊度，默认丿
        /// </summary>
        private int? shadowBlur_Renamed;

        /// <summary>
        ///     光影颜色，默认跟随color
        /// </summary>
        private string shadowColor_Renamed;

        /// <summary>
        ///     是否开启，默认关闭
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     特效类型，默认为'scale'（放大），可选还朿bounce'（跳动）
        ///     @since 2.2.0
        /// </summary>
        private Type type_Renamed;

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }


        /// <summary>
        ///     获取loop值
        /// </summary>
        public virtual bool? Loop
        {
            get { return loop_Renamed; }
            set { loop_Renamed = value; }
        }


        /// <summary>
        ///     获取period值
        /// </summary>
        public virtual int? Period
        {
            get { return period_Renamed; }
            set { period_Renamed = value; }
        }


        /// <summary>
        ///     获取scaleSize值
        /// </summary>
        public virtual int? ScaleSize
        {
            get { return scaleSize_Renamed; }
            set { scaleSize_Renamed = value; }
        }


        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }


        /// <summary>
        ///     获取shadowColor值
        /// </summary>
        public virtual string ShadowColor
        {
            get { return shadowColor_Renamed; }
            set { shadowColor_Renamed = value; }
        }


        /// <summary>
        ///     获取shadowBlur值
        /// </summary>
        public virtual int? ShadowBlur
        {
            get { return shadowBlur_Renamed; }
            set { shadowBlur_Renamed = value; }
        }

        /// <summary>
        ///     获取bounceDistance值
        /// </summary>
        public virtual int? BounceDistance
        {
            get { return bounceDistance_Renamed; }
            set { bounceDistance_Renamed = value; }
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual Type type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual Effect type(Type type)
        {
            type_Renamed = type;
            return this;
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual Effect show(bool? show)
        {
            show_Renamed = show;
            return this;
        }

        /// <summary>
        ///     获取loop值
        /// </summary>
        public virtual bool? loop()
        {
            return loop_Renamed;
        }

        /// <summary>
        ///     设置loop值     *
        /// </summary>
        /// <param name="loop"> </param>
        public virtual Effect loop(bool? loop)
        {
            loop_Renamed = loop;
            return this;
        }

        /// <summary>
        ///     获取period值
        /// </summary>
        public virtual int? period()
        {
            return period_Renamed;
        }

        /// <summary>
        ///     设置period值     *
        /// </summary>
        /// <param name="period"> </param>
        public virtual Effect period(int? period)
        {
            period_Renamed = period;
            return this;
        }

        /// <summary>
        ///     获取scaleSize值
        /// </summary>
        public virtual int? scaleSize()
        {
            return scaleSize_Renamed;
        }

        /// <summary>
        ///     设置scaleSize值     *
        /// </summary>
        /// <param name="scaleSize"> </param>
        public virtual Effect scaleSize(int? scaleSize)
        {
            scaleSize_Renamed = scaleSize;
            return this;
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual Effect color(string color)
        {
            color_Renamed = color;
            return this;
        }

        /// <summary>
        ///     获取shadowColor值
        /// </summary>
        public virtual string shadowColor()
        {
            return shadowColor_Renamed;
        }

        /// <summary>
        ///     设置shadowColor值     *
        /// </summary>
        /// <param name="shadowColor"> </param>
        public virtual Effect shadowColor(string shadowColor)
        {
            shadowColor_Renamed = shadowColor;
            return this;
        }

        /// <summary>
        ///     获取shadowBlur值
        /// </summary>
        public virtual int? shadowBlur()
        {
            return shadowBlur_Renamed;
        }

        /// <summary>
        ///     设置shadowBlur值     *
        /// </summary>
        /// <param name="shadowBlur"> </param>
        public virtual Effect shadowBlur(int? shadowBlur)
        {
            shadowBlur_Renamed = shadowBlur;
            return this;
        }

        /// <summary>
        ///     获取bounceDistance值
        /// </summary>
        public virtual int? bounceDistance()
        {
            return bounceDistance_Renamed;
        }

        /// <summary>
        ///     设置bounceDistance值     *
        /// </summary>
        /// <param name="bounceDistance"> </param>
        public virtual Effect bounceDistance(int? bounceDistance)
        {
            bounceDistance_Renamed = bounceDistance;
            return this;
        }


        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual Type getType()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual void setType(Type type)
        {
            type_Renamed = type;
        }
    }
}