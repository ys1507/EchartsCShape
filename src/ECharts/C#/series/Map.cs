/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     地图
    ///     @author Yongjin.C
    /// </summary>
    public class Map : Series<Map>
    {
        /// <summary>
        ///     是否启用值域漫游组件（dataRange）hover时的联动响应
        /// </summary>
        private bool? dataRangeHoverLink_Renamed;

        /// <summary>
        ///     通过绝对经纬度指定地区的名称文本位置，如{'Islands':[113.95, 22.26]}，香港离岛区名称显示定位到东绿13.95，北线2.26丿
        /// </summary>
        private GeoCoord geoCoord_Renamed;

        /// <summary>
        ///     地图位置设置，默认只适应上下左右居中可配x，y，width，height，任意参数为空都将根据其他参数自适应
        /// </summary>
        private MapLocation mapLocation_Renamed;

        /// <summary>
        ///     地图类型，支持world，china及全囿4个省市自治区。省市自治区的mapType直接使用简体中斿     * 支持子区域模式，通过主地图类型扩展出所包含的子区域地图，格式为'主地图类型|子区域名秿，如
        ///     'world|Brazil'＿china|广东'
        /// </summary>
        private string mapType_Renamed;

        /// <summary>
        ///     地图数值计算方式，默认为加和，可选为＿sum'（总数＿| 'average'（均值）
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Calculation
        /// </seealso>
        private Calculation mapValueCalculation_Renamed;

        /// <summary>
        ///     地图数值计算结果小数精度，mapValueCalculation为average时有效，默认为取整，需要小数精度时设置大于0的整敿
        /// </summary>
        private int? mapValuePrecision_Renamed;

        /// <summary>
        ///     自定义地区的名称映射，如{'China' : '中国'}
        /// </summary>
        private object nameMap_Renamed;

        /// <summary>
        ///     是否开启滚轮缩放和拖拽漫游
        /// </summary>
        private bool? roam_Renamed;

        /// <summary>
        ///     滚轮缩放的极限控制，可指定{max:number, min:number}，其中max为放大系数，有效值应大于1，min为缩小系数，有效值应小于1
        /// </summary>
        private ScaleLimit scaleLimit_Renamed;

        /// <summary>
        ///     选中模式，默认关闭，可选single，multiple
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.SelectedMode
        /// </seealso>
        private object selectedMode_Renamed;

        /// <summary>
        ///     显示图例颜色标识（系列标识的小圆点），存在legend时生敿
        /// </summary>
        private bool? showLegendSymbol_Renamed;

        /// <summary>
        ///     地区的名称文本位置修正，数值单位为px，正值为左下偏移，负值为右上偏移，如{'China' : [10, -10]}
        /// </summary>
        private object textFixed_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Map()
        {
            type(SeriesType.map);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Map(string name) : base(name)
        {
            type(SeriesType.map);
        }

        /// <summary>
        ///     获取selectedMode值
        /// </summary>
        public virtual object SelectedMode
        {
            get { return selectedMode_Renamed; }
            set { selectedMode_Renamed = value; }
        }


        /// <summary>
        ///     获取mapType值
        /// </summary>
        public virtual string MapType
        {
            get { return mapType_Renamed; }
            set { mapType_Renamed = value; }
        }

        /// <summary>
        ///     获取mapValueCalculation值
        /// </summary>
        public virtual Calculation MapValueCalculation
        {
            get { return mapValueCalculation_Renamed; }
            set { mapValueCalculation_Renamed = value; }
        }


        /// <summary>
        ///     获取mapValuePrecision值
        /// </summary>
        public virtual int? MapValuePrecision
        {
            get { return mapValuePrecision_Renamed; }
            set { mapValuePrecision_Renamed = value; }
        }


        /// <summary>
        ///     获取showLegendSymbol值
        /// </summary>
        public virtual bool? ShowLegendSymbol
        {
            get { return showLegendSymbol_Renamed; }
            set { showLegendSymbol_Renamed = value; }
        }


        /// <summary>
        ///     获取dataRangeHoverLink值
        /// </summary>
        public virtual bool? DataRangeHoverLink
        {
            get { return dataRangeHoverLink_Renamed; }
            set { dataRangeHoverLink_Renamed = value; }
        }


        /// <summary>
        ///     获取roam值
        /// </summary>
        public virtual bool? Roam
        {
            get { return roam_Renamed; }
            set { roam_Renamed = value; }
        }

        /// <summary>
        ///     获取nameMap值
        /// </summary>
        public virtual object NameMap
        {
            get { return nameMap_Renamed; }
            set { nameMap_Renamed = value; }
        }


        /// <summary>
        ///     获取textFixed值
        /// </summary>
        public virtual object TextFixed
        {
            get { return textFixed_Renamed; }
            set { textFixed_Renamed = value; }
        }

        /// <summary>
        ///     设置mapLocation值     *
        /// </summary>
        /// <param name="mapLocation"> </param>
        public virtual Map mapLocation(MapLocation mapLocation)
        {
            mapLocation_Renamed = mapLocation;
            return this;
        }

        /// <summary>
        ///     获取selectedMode值
        /// </summary>
        public virtual object selectedMode()
        {
            return selectedMode_Renamed;
        }

        /// <summary>
        ///     设置selectedMode值     *
        /// </summary>
        /// <param name="selectedMode"> </param>
        public virtual Map selectedMode(object selectedMode)
        {
            selectedMode_Renamed = selectedMode;
            return this;
        }

        /// <summary>
        ///     设置selectedMode值     *
        /// </summary>
        /// <param name="selectedMode"> </param>
        public virtual Map selectedMode(SelectedMode selectedMode)
        {
            selectedMode_Renamed = selectedMode;
            return this;
        }

        /// <summary>
        ///     获取mapType值
        /// </summary>
        public virtual string mapType()
        {
            return mapType_Renamed;
        }

        /// <summary>
        ///     设置mapType值     *
        /// </summary>
        /// <param name="mapType"> </param>
        public virtual Map mapType(string mapType)
        {
            mapType_Renamed = mapType;
            return this;
        }

        /// <summary>
        ///     地图位置设置，默认只适应上下左右居中可配x，y，width，height，任意参数为空都将根据其他参数自适应
        /// </summary>
        public virtual MapLocation mapLocation()
        {
            if (mapLocation_Renamed == null)
            {
                mapLocation_Renamed = new MapLocation();
            }
            return mapLocation_Renamed;
        }

        /// <summary>
        ///     获取mapValueCalculation值
        /// </summary>
        public virtual Calculation mapValueCalculation()
        {
            return mapValueCalculation_Renamed;
        }

        /// <summary>
        ///     设置mapValueCalculation值     *
        /// </summary>
        /// <param name="mapValueCalculation"> </param>
        public virtual Map mapValueCalculation(Calculation mapValueCalculation)
        {
            mapValueCalculation_Renamed = mapValueCalculation;
            return this;
        }

        /// <summary>
        ///     获取mapValuePrecision值
        /// </summary>
        public virtual int? mapValuePrecision()
        {
            return mapValuePrecision_Renamed;
        }

        /// <summary>
        ///     设置mapValuePrecision值     *
        /// </summary>
        /// <param name="mapValuePrecision"> </param>
        public virtual Map mapValuePrecision(int? mapValuePrecision)
        {
            mapValuePrecision_Renamed = mapValuePrecision;
            return this;
        }

        /// <summary>
        ///     获取showLegendSymbol值
        /// </summary>
        public virtual bool? showLegendSymbol()
        {
            return showLegendSymbol_Renamed;
        }

        /// <summary>
        ///     设置showLegendSymbol值     *
        /// </summary>
        /// <param name="showLegendSymbol"> </param>
        public virtual Map showLegendSymbol(bool? showLegendSymbol)
        {
            showLegendSymbol_Renamed = showLegendSymbol;
            return this;
        }

        /// <summary>
        ///     获取dataRangeHoverLink值
        /// </summary>
        public virtual bool? dataRangeHoverLink()
        {
            return dataRangeHoverLink_Renamed;
        }

        /// <summary>
        ///     设置dataRangeHoverLink值     *
        /// </summary>
        /// <param name="dataRangeHoverLink"> </param>
        public virtual Map dataRangeHoverLink(bool? dataRangeHoverLink)
        {
            dataRangeHoverLink_Renamed = dataRangeHoverLink;
            return this;
        }

        /// <summary>
        ///     获取roam值
        /// </summary>
        public virtual bool? roam()
        {
            return roam_Renamed;
        }

        /// <summary>
        ///     设置roam值     *
        /// </summary>
        /// <param name="roam"> </param>
        public virtual Map roam(bool? roam)
        {
            roam_Renamed = roam;
            return this;
        }

        /// <summary>
        ///     获取scaleLimit值
        /// </summary>
        public virtual ScaleLimit scaleLimit()
        {
            if (scaleLimit_Renamed == null)
            {
                scaleLimit_Renamed = new ScaleLimit();
            }
            return scaleLimit_Renamed;
        }

        /// <summary>
        ///     设置scaleLimit值     *
        /// </summary>
        /// <param name="scaleLimit"> </param>
        public virtual Map scaleLimit(ScaleLimit scaleLimit)
        {
            scaleLimit_Renamed = scaleLimit;
            return this;
        }

        /// <summary>
        ///     获取nameMap值
        /// </summary>
        public virtual object nameMap()
        {
            return nameMap_Renamed;
        }

        /// <summary>
        ///     设置nameMap值     *
        /// </summary>
        /// <param name="nameMap"> </param>
        public virtual Map nameMap(object nameMap)
        {
            nameMap_Renamed = nameMap;
            return this;
        }

        /// <summary>
        ///     获取textFixed值
        /// </summary>
        public virtual object textFixed()
        {
            return textFixed_Renamed;
        }

        /// <summary>
        ///     设置textFixed值     *
        /// </summary>
        /// <param name="textFixed"> </param>
        public virtual Map textFixed(object textFixed)
        {
            textFixed_Renamed = textFixed;
            return this;
        }

        /// <summary>
        ///     获取geoCoord值
        /// </summary>
        public virtual GeoCoord geoCoord()
        {
            if (geoCoord_Renamed == null)
            {
                geoCoord_Renamed = new GeoCoord();
            }
            return geoCoord_Renamed;
        }

        /// <summary>
        ///     设置name,x,y值     *
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        public virtual Map geoCoord(string name, string x, string y)
        {
            geoCoord().put(name, x, y);
            return this;
        }


        /// <summary>
        ///     获取mapLocation值
        /// </summary>
        public virtual MapLocation getMapLocation()
        {
            return mapLocation_Renamed;
        }

        /// <summary>
        ///     设置mapLocation值     *
        /// </summary>
        /// <param name="mapLocation"> </param>
        public virtual void setMapLocation(MapLocation mapLocation)
        {
            mapLocation_Renamed = mapLocation;
        }


        /// <summary>
        ///     获取scaleLimit值
        /// </summary>
        public virtual ScaleLimit getScaleLimit()
        {
            return scaleLimit_Renamed;
        }

        /// <summary>
        ///     设置scaleLimit值     *
        /// </summary>
        /// <param name="scaleLimit"> </param>
        public virtual void setScaleLimit(ScaleLimit scaleLimit)
        {
            scaleLimit_Renamed = scaleLimit;
        }


        /// <summary>
        ///     获取geoCoord值
        /// </summary>
        public virtual GeoCoord getGeoCoord()
        {
            return geoCoord_Renamed;
        }

        /// <summary>
        ///     设置geoCoord值     *
        /// </summary>
        /// <param name="geoCoord"> </param>
        public virtual void setGeoCoord(GeoCoord geoCoord)
        {
            geoCoord_Renamed = geoCoord;
        }
    }
}