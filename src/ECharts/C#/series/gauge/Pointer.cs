using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.series.gauge
{
    /// <summary>
    ///     仪表盿- 指针样式
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Pointer
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     属性color控制指针颜色
        /// </summary>
        private string color_Renamed;

        /// <summary>
        ///     属性length控制线长，百分比相对的是仪表盘的外半徿
        /// </summary>
        private object length_Renamed;

        /// <summary>
        ///     属性width控制指针最宽处＿
        /// </summary>
        private object width_Renamed;

        /// <summary>
        ///     获取length值
        /// </summary>
        public virtual object Length
        {
            get { return length_Renamed; }
            set { length_Renamed = value; }
        }


        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object Width
        {
            get { return width_Renamed; }
            set { width_Renamed = value; }
        }


        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }

        /// <summary>
        ///     获取length值
        /// </summary>
        public virtual object length()
        {
            return length_Renamed;
        }

        /// <summary>
        ///     设置length值     *
        /// </summary>
        /// <param name="length"> </param>
        public virtual Pointer length(object length)
        {
            length_Renamed = length;
            return this;
        }

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object width()
        {
            return width_Renamed;
        }

        /// <summary>
        ///     设置width值     *
        /// </summary>
        /// <param name="width"> </param>
        public virtual Pointer width(object width)
        {
            width_Renamed = width;
            return this;
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual Pointer color(string color)
        {
            color_Renamed = color;
            return this;
        }
    }
}