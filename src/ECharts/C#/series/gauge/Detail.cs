using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.series.gauge
{
    /// <summary>
    ///     仪表盘详惿 *
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Detail
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     标题背景颜色，默认透明
        /// </summary>
        private string backgroundColor_Renamed;

        /// <summary>
        ///     标题边框颜色
        /// </summary>
        private string borderColor_Renamed;

        /// <summary>
        ///     borderWidth
        /// </summary>
        private int? borderWidth_Renamed;

        /// <summary>
        ///     属性formatter可以格式化文朿
        /// </summary>
        private string formatter_Renamed;

        /// <summary>
        ///     属性height控制详情高度
        /// </summary>
        private object height_Renamed;

        /// <summary>
        ///     属性offsetCenter用于详情定位，数组为横纵相对仪表盘圆心坐标偏移，支持百分比（相对外半径）
        /// </summary>
        private object offsetCenter_Renamed;

        /// <summary>
        ///     属性show控制显示与否
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     属性textStyle（详见textStyle）控制文本样弿
        /// </summary>
        private TextStyle textStyle_Renamed;

        /// <summary>
        ///     属性width控制详情宽度
        /// </summary>
        private object width_Renamed;

        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }


        /// <summary>
        ///     获取backgroundColor值
        /// </summary>
        public virtual string BackgroundColor
        {
            get { return backgroundColor_Renamed; }
            set { backgroundColor_Renamed = value; }
        }


        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string BorderColor
        {
            get { return borderColor_Renamed; }
            set { borderColor_Renamed = value; }
        }


        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? BorderWidth
        {
            get { return borderWidth_Renamed; }
            set { borderWidth_Renamed = value; }
        }


        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object Width
        {
            get { return width_Renamed; }
            set { width_Renamed = value; }
        }


        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual object Height
        {
            get { return height_Renamed; }
            set { height_Renamed = value; }
        }


        /// <summary>
        ///     获取offsetCenter值
        /// </summary>
        public virtual object OffsetCenter
        {
            get { return offsetCenter_Renamed; }
            set { offsetCenter_Renamed = value; }
        }


        /// <summary>
        ///     获取formatter值
        /// </summary>
        public virtual string Formatter
        {
            get { return formatter_Renamed; }
            set { formatter_Renamed = value; }
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual Detail show(bool? show)
        {
            show_Renamed = show;
            return this;
        }

        /// <summary>
        ///     获取backgroundColor值
        /// </summary>
        public virtual string backgroundColor()
        {
            return backgroundColor_Renamed;
        }

        /// <summary>
        ///     设置backgroundColor值     *
        /// </summary>
        /// <param name="backgroundColor"> </param>
        public virtual Detail backgroundColor(string backgroundColor)
        {
            backgroundColor_Renamed = backgroundColor;
            return this;
        }

        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string borderColor()
        {
            return borderColor_Renamed;
        }

        /// <summary>
        ///     设置borderColor值     *
        /// </summary>
        /// <param name="borderColor"> </param>
        public virtual Detail borderColor(string borderColor)
        {
            borderColor_Renamed = borderColor;
            return this;
        }

        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? borderWidth()
        {
            return borderWidth_Renamed;
        }

        /// <summary>
        ///     设置borderWidth值     *
        /// </summary>
        /// <param name="borderWidth"> </param>
        public virtual Detail borderWidth(int? borderWidth)
        {
            borderWidth_Renamed = borderWidth;
            return this;
        }

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object width()
        {
            return width_Renamed;
        }

        /// <summary>
        ///     设置width值     *
        /// </summary>
        /// <param name="width"> </param>
        public virtual Detail width(object width)
        {
            width_Renamed = width;
            return this;
        }

        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual object height()
        {
            return height_Renamed;
        }

        /// <summary>
        ///     设置height值     *
        /// </summary>
        /// <param name="height"> </param>
        public virtual Detail height(object height)
        {
            height_Renamed = height;
            return this;
        }

        /// <summary>
        ///     获取offsetCenter值
        /// </summary>
        public virtual object offsetCenter()
        {
            return offsetCenter_Renamed;
        }

        /// <summary>
        ///     设置offsetCenter值     *
        /// </summary>
        /// <param name="offsetCenter"> </param>
        public virtual Detail offsetCenter(object offsetCenter)
        {
            offsetCenter_Renamed = offsetCenter;
            return this;
        }

        /// <summary>
        ///     获取formatter值
        /// </summary>
        public virtual string formatter()
        {
            return formatter_Renamed;
        }

        /// <summary>
        ///     设置formatter值     *
        /// </summary>
        /// <param name="formatter"> </param>
        public virtual Detail formatter(string formatter)
        {
            formatter_Renamed = formatter;
            return this;
        }

        /// <summary>
        ///     属性textStyle（详见textStyle）控制文本样弿
        /// </summary>
        public virtual TextStyle textStyle()
        {
            if (textStyle_Renamed == null)
            {
                textStyle_Renamed = new TextStyle();
            }
            return textStyle_Renamed;
        }
    }
}