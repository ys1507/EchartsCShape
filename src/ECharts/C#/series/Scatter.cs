/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     散点图、气泡图
    ///     @author Yongjin.C
    /// </summary>
    public class Scatter : Series<Scatter>
    {
        /// <summary>
        ///     大规模阀值，large为true且数据量>largeThreshold才启用大规模模式
        /// </summary>
        private long? largeThreshold_Renamed;

        /// <summary>
        ///     大规模散点图
        /// </summary>
        private bool? large_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Scatter()
        {
            type(SeriesType.scatter);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Scatter(string name) : base(name)
        {
            type(SeriesType.scatter);
        }

        /// <summary>
        ///     获取large值
        /// </summary>
        public virtual bool? Large
        {
            get { return large_Renamed; }
            set { large_Renamed = value; }
        }


        /// <summary>
        ///     获取largeThreshold值
        /// </summary>
        public virtual long? LargeThreshold
        {
            get { return largeThreshold_Renamed; }
            set { largeThreshold_Renamed = value; }
        }

        /// <summary>
        ///     获取large值
        /// </summary>
        public virtual bool? large()
        {
            return large_Renamed;
        }

        /// <summary>
        ///     设置large值     *
        /// </summary>
        /// <param name="large"> </param>
        public virtual Scatter large(bool? large)
        {
            large_Renamed = large;
            return this;
        }

        /// <summary>
        ///     获取largeThreshold值
        /// </summary>
        public virtual long? largeThreshold()
        {
            return largeThreshold_Renamed;
        }

        /// <summary>
        ///     设置largeThreshold值     *
        /// </summary>
        /// <param name="largeThreshold"> </param>
        public virtual Scatter largeThreshold(long? largeThreshold)
        {
            largeThreshold_Renamed = largeThreshold;
            return this;
        }
    }
}