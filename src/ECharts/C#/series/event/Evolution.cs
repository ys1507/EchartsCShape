using System;

namespace Yongjin.Cshape.echarts.series.@event
{
    /// <summary>
    ///     同一事件的的演化过程，每一个数组项为Object {}，内容如丿 *
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Evolution
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     事件的详细信恿
        /// </summary>
        private Detail detail_Renamed;

        //	private static readonly SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

        private string time_Renamed;
        private int? value_Renamed;

        /// <summary>
        ///     构造方泿
        /// </summary>
        public Evolution()
        {
        }

        /// <summary>
        ///     构造方泿     *
        /// </summary>
        /// <param name="time"> </param>
        /// <param name="value"> </param>
        public Evolution(string time, int? value)
        {
            time_Renamed = time;
            value_Renamed = value;
        }

        /// <summary>
        ///     获取time值
        /// </summary>
        public virtual string Time
        {
            get { return time_Renamed; }
            set { time_Renamed = value; }
        }


        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual int? Value
        {
            get { return value_Renamed; }
            set { value_Renamed = value; }
        }

        /// <summary>
        ///     获取time值
        /// </summary>
        public virtual string time()
        {
            return time_Renamed;
        }

        /// <summary>
        ///     设置time值     *
        /// </summary>
        /// <param name="time"> </param>
        public virtual Evolution time(string time)
        {
            time_Renamed = time;
            return this;
        }

        /// <summary>
        ///     设置time值，默认yyyy-MM-dd,其他情况建议使用字符串类型的时间
        /// </summary>
        /// <param name="time"> </param>
        public virtual Evolution time(DateTime time)
        {
            time_Renamed = time.ToString("yyyy-MM-dd");
            return this;
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual int? value()
        {
            return value_Renamed;
        }

        /// <summary>
        ///     设置value值     *
        /// </summary>
        /// <param name="value"> </param>
        public virtual Evolution value(int? value)
        {
            value_Renamed = value;
            return this;
        }

        /// <summary>
        ///     获取detail值
        /// </summary>
        public virtual Detail detail()
        {
            return detail_Renamed;
        }

        /// <summary>
        ///     设置detail值     *
        /// </summary>
        /// <param name="detail"> </param>
        public virtual Evolution detail(Detail detail)
        {
            detail_Renamed = detail;
            return this;
        }

        /// <summary>
        ///     设置detail值     *
        /// </summary>
        /// <param name="link"> </param>
        /// <param name="text"> </param>
        public virtual Evolution detail(string link, string text)
        {
            detail_Renamed = new Detail(link, text);
            return this;
        }

        /// <summary>
        ///     设置detail值     *
        /// </summary>
        /// <param name="link"> </param>
        /// <param name="text"> </param>
        /// <param name="img"> </param>
        public virtual Evolution detail(string link, string text, string img)
        {
            detail_Renamed = new Detail(link, text, img);
            return this;
        }


        /// <summary>
        ///     获取detail值
        /// </summary>
        public virtual Detail getDetail()
        {
            return detail_Renamed;
        }

        /// <summary>
        ///     设置detail值     *
        /// </summary>
        /// <param name="detail"> </param>
        public virtual void setDetail(Detail detail)
        {
            detail_Renamed = detail;
        }
    }
}