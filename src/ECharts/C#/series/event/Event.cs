using System;
using System.Collections.Generic;
using System.Linq;

namespace Yongjin.Cshape.echarts.series.@event
{
    /// <summary>
    ///     事件列表，每一个数组项为Object {}，内容如丿 *
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Event
    {
        private const long serialVersionUID = 1L;
        private List<Evolution> evolution_Renamed;

        private string name_Renamed;

        private int? weight_Renamed;

        /// <summary>
        ///     构造方泿
        /// </summary>
        public Event()
        {
        }

        /// <summary>
        ///     构造方泿     *
        /// </summary>
        /// <param name="name"> </param>
        public Event(string name)
        {
            name_Renamed = name;
        }

        /// <summary>
        ///     构造方泿     *
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="weight"> </param>
        public Event(string name, int? weight)
        {
            name_Renamed = name;
            weight_Renamed = weight;
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string Name
        {
            get { return name_Renamed; }
            set { name_Renamed = value; }
        }


        /// <summary>
        ///     获取weight值
        /// </summary>
        public virtual int? Weight
        {
            get { return weight_Renamed; }
            set { weight_Renamed = value; }
        }


        /// <summary>
        ///     获取evolution值
        /// </summary>
        public virtual List<Evolution> Evolution
        {
            get { return evolution_Renamed; }
            set { evolution_Renamed = value; }
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string name()
        {
            return name_Renamed;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual Event name(string name)
        {
            name_Renamed = name;
            return this;
        }

        /// <summary>
        ///     获取weight值
        /// </summary>
        public virtual int? weight()
        {
            return weight_Renamed;
        }

        /// <summary>
        ///     设置weight值     *
        /// </summary>
        /// <param name="weight"> </param>
        public virtual Event weight(int? weight)
        {
            weight_Renamed = weight;
            return this;
        }

        /// <summary>
        ///     获取evolution值
        /// </summary>
        public virtual List<Evolution> evolution()
        {
            if (evolution_Renamed == null)
            {
                evolution_Renamed = new List<Evolution>();
            }
            return evolution_Renamed;
        }

        /// <summary>
        ///     设置evolution值     *
        /// </summary>
        /// <param name="evolution"> </param>
        public virtual Event evolution(List<Evolution> evolution)
        {
            evolution_Renamed = evolution;
            return this;
        }

        /// <summary>
        ///     添加evolution值     *
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Event evolution(params Evolution[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            evolution().AddRange(values.ToList());
            return this;
        }
    }
}