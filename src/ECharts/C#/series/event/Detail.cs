using System;

namespace Yongjin.Cshape.echarts.series.@event
{
    /// <summary>
    ///     事件的详细信恿 *
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Detail
    {
        private const long serialVersionUID = 1L;
        private string img_Renamed;

        private string link_Renamed;

        private string text_Renamed;

        /// <summary>
        ///     构造方泿
        /// </summary>
        public Detail()
        {
        }

        /// <summary>
        ///     构造方泿     *
        /// </summary>
        /// <param name="link"> </param>
        /// <param name="text"> </param>
        public Detail(string link, string text)
        {
            link_Renamed = link;
            text_Renamed = text;
        }

        /// <summary>
        ///     构造方泿     *
        /// </summary>
        /// <param name="link"> </param>
        /// <param name="text"> </param>
        /// <param name="img"> </param>
        public Detail(string link, string text, string img)
        {
            link_Renamed = link;
            text_Renamed = text;
            img_Renamed = img;
        }

        /// <summary>
        ///     获取link值
        /// </summary>
        public virtual string Link
        {
            get { return link_Renamed; }
            set { link_Renamed = value; }
        }


        /// <summary>
        ///     获取text值
        /// </summary>
        public virtual string Text
        {
            get { return text_Renamed; }
            set { text_Renamed = value; }
        }


        /// <summary>
        ///     获取img值
        /// </summary>
        public virtual string Img
        {
            get { return img_Renamed; }
            set { img_Renamed = value; }
        }

        /// <summary>
        ///     获取link值
        /// </summary>
        public virtual string link()
        {
            return link_Renamed;
        }

        /// <summary>
        ///     设置link值     *
        /// </summary>
        /// <param name="link"> </param>
        public virtual Detail link(string link)
        {
            link_Renamed = link;
            return this;
        }

        /// <summary>
        ///     获取text值
        /// </summary>
        public virtual string text()
        {
            return text_Renamed;
        }

        /// <summary>
        ///     设置text值     *
        /// </summary>
        /// <param name="text"> </param>
        public virtual Detail text(string text)
        {
            text_Renamed = text;
            return this;
        }

        /// <summary>
        ///     获取img值
        /// </summary>
        public virtual string img()
        {
            return img_Renamed;
        }

        /// <summary>
        ///     设置img值     *
        /// </summary>
        /// <param name="img"> </param>
        public virtual Detail img(string img)
        {
            img_Renamed = img;
            return this;
        }
    }
}