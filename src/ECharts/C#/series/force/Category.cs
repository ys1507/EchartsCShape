using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.series.force
{
    /// <summary>
    ///     力导向图中节点的分类
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Category
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     所有该类目的节点是否能被拖拿
        /// </summary>
        private bool? draggable_Renamed;

        /// <summary>
        ///     详见 itemStyle
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.ItemStyle
        /// </seealso>
        private ItemStyle itemStyle_Renamed;

        /// <summary>
        ///     类目名称
        /// </summary>
        private string name_Renamed;

        /// <summary>
        ///     所有该类目的节点的大小
        /// </summary>
        private object symbolSize_Renamed;

        /// <summary>
        ///     所有该类目的节点的形状, 详见 symbolList
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Symbol
        /// </seealso>
        private object symbol_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Category()
        {
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Category(string name)
        {
            name_Renamed = name;
        }

        /// <summary>
        ///     获取itemStyle值
        /// </summary>
        public virtual ItemStyle ItemStyle
        {
            get { return itemStyle_Renamed; }
            set { itemStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string Name
        {
            get { return name_Renamed; }
            set { name_Renamed = value; }
        }


        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object Symbol
        {
            get { return symbol_Renamed; }
            set { symbol_Renamed = value; }
        }


        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object SymbolSize
        {
            get { return symbolSize_Renamed; }
            set { symbolSize_Renamed = value; }
        }


        /// <summary>
        ///     获取draggable值
        /// </summary>
        public virtual bool? Draggable
        {
            get { return draggable_Renamed; }
            set { draggable_Renamed = value; }
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string name()
        {
            return name_Renamed;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual Category name(string name)
        {
            name_Renamed = name;
            return this;
        }

        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object symbol()
        {
            return symbol_Renamed;
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual Category symbol(object symbol)
        {
            symbol_Renamed = symbol;
            return this;
        }

        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object symbolSize()
        {
            return symbolSize_Renamed;
        }

        /// <summary>
        ///     设置symbolSize值     *
        /// </summary>
        /// <param name="symbolSize"> </param>
        public virtual Category symbolSize(object symbolSize)
        {
            symbolSize_Renamed = symbolSize;
            return this;
        }

        /// <summary>
        ///     获取draggable值
        /// </summary>
        public virtual bool? draggable()
        {
            return draggable_Renamed;
        }

        /// <summary>
        ///     设置draggable值     *
        /// </summary>
        /// <param name="draggable"> </param>
        public virtual Category draggable(bool? draggable)
        {
            draggable_Renamed = draggable;
            return this;
        }

        /// <summary>
        ///     详见 itemStyle
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.ItemStyle
        /// </seealso>
        public virtual ItemStyle itemStyle()
        {
            if (itemStyle_Renamed == null)
            {
                itemStyle_Renamed = new ItemStyle();
            }
            return itemStyle_Renamed;
        }
    }
}