using System;
using System.Collections.Generic;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"], to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS"]= WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.series.force
{
    /// <summary>
    ///     力导向图的顶点数捿 *
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Node : Dictionary<string, object>
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Node()
        {
        }

        /// <summary>
        ///     构造函敿参数:category,name,value
        /// </summary>
        /// <param name="category"> </param>
        /// <param name="name"> </param>
        /// <param name="value"> </param>
        public Node(int? category, string name, int? value)
        {
            this["category"] = category;
            this["name"] = name;
            this["value"] = value;
        }

        /// <summary>
        ///     获取itemStyle值
        /// </summary>
        public virtual ItemStyle ItemStyle
        {
            get { return (ItemStyle) this["itemStyle"]; }
            set { this["itemStyle"] = value; }
        }


        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string Name
        {
            get { return (string) this["name"]; }
            set { this["name"] = value; }
        }


        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual int? Value
        {
            get { return (int?) this["value"]; }
            set { this["value"] = value; }
        }


        /// <summary>
        ///     获取initial值
        /// </summary>
        public virtual object Initial
        {
            get { return this["initial"]; }
            set { this["initial"] = value; }
        }


        /// <summary>
        ///     获取fixX值
        /// </summary>
        public virtual bool? FixX
        {
            get { return (bool?) this["fixX"]; }
            set { this["fixX"] = value; }
        }


        /// <summary>
        ///     获取fixY值
        /// </summary>
        public virtual bool? FixY
        {
            get { return (bool?) this["fixY"]; }
            set { this["fixY"] = value; }
        }


        /// <summary>
        ///     获取ignore值
        /// </summary>
        public virtual bool? Ignore
        {
            get { return (bool?) this["ignore"]; }
            set { this["ignore"] = value; }
        }


        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object Symbol
        {
            get { return this["symbol"]; }
            set { this["symbol"] = value; }
        }


        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object SymbolSize
        {
            get { return this["symbolSize"]; }
            set { this["symbolSize"] = value; }
        }


        /// <summary>
        ///     获取draggable值
        /// </summary>
        public virtual bool? Draggable
        {
            get { return (bool?) this["draggable"]; }
            set { this["draggable"] = value; }
        }


        /// <summary>
        ///     获取category值
        /// </summary>
        public virtual int? Category
        {
            get { return (int?) this["category"]; }
            set { this["category"] = value; }
        }


        /// <summary>
        ///     获取label值
        /// </summary>
        public virtual string Label
        {
            get { return (string) this["label"]; }
            set { this["label"] = value; }
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string name()
        {
            return (string) this["name"];
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual Node name(string name)
        {
            this["name"] = name;
            return this;
        }

        /// <summary>
        ///     获取label值     *
        ///     @since 2.1.9
        /// </summary>
        public virtual string label()
        {
            return (string) this["label"];
        }

        /// <summary>
        ///     设置label值     *
        /// </summary>
        /// <param name="label">
        ///     @since 2.1.9
        /// </param>
        public virtual Node label(string label)
        {
            this["label"] = label;
            return this;
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual int? value()
        {
            return (int?) this["value"];
        } //

        /// <summary>
        ///     设置value值     *
        /// </summary>
        /// <param name="value"> </param>
        public virtual Node value(int? value)
        {
            this["value"] = value;
            return this;
        }

        /// <summary>
        ///     获取initial值
        /// </summary>
        public virtual object initial()
        {
            return this["initial"];
        }

        /// <summary>
        ///     设置initial值     *
        /// </summary>
        /// <param name="initial"> </param>
        public virtual Node initial(object initial)
        {
            this["initial"] = initial;

            return this;
        }

        /// <summary>
        ///     获取fixX值
        /// </summary>
        public virtual bool? fixX()
        {
            return (bool?) this["fixX"];
        }

        /// <summary>
        ///     设置fixX值     *
        /// </summary>
        /// <param name="fixX"> </param>
        public virtual Node fixX(bool? fixX)
        {
            this["fixX"] = fixX;

            //this["fixX"]= fixX);
            return this;
        }

        /// <summary>
        ///     获取fixY值
        /// </summary>
        public virtual bool? fixY()
        {
            return (bool?) this["fixY"];
        }

        /// <summary>
        ///     设置fixY值     *
        /// </summary>
        /// <param name="fixY"> </param>
        public virtual Node fixY(bool? fixY)
        {
            this["fixY"] = fixY;
            //this["fixY"]= fixY);
            return this;
        }

        /// <summary>
        ///     获取ignore值
        /// </summary>
        public virtual bool? ignore()
        {
            return (bool?) this["ignore"];
        }

        /// <summary>
        ///     设置ignore值     *
        /// </summary>
        /// <param name="ignore"> </param>
        public virtual Node ignore(bool? ignore)
        {
            this["ignore"] = ignore;
            //this["ignore"]= ignore);
            return this;
        }

        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object symbol()
        {
            return this["symbol"];
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual Node symbol(object symbol)
        {
            this["symbol"] = symbol;
            return this;
        }

        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object symbolSize()
        {
            return this["symbolSize"];
        }

        /// <summary>
        ///     设置symbolSize值     *
        /// </summary>
        /// <param name="symbolSize"> </param>
        public virtual Node symbolSize(object symbolSize)
        {
            this["symbolSize"] = symbolSize;
            return this;
        }

        /// <summary>
        ///     获取draggable值
        /// </summary>
        public virtual bool? draggable()
        {
            return (bool?) this["draggable"];
        }

        /// <summary>
        ///     设置draggable值     *
        /// </summary>
        /// <param name="draggable"> </param>
        public virtual Node draggable(bool? draggable)
        {
            this["draggable"] = draggable;
            return this;
        }

        /// <summary>
        ///     获取category值
        /// </summary>
        public virtual int? category()
        {
            return (int?) this["category"];
        }

        /// <summary>
        ///     设置category值     *
        /// </summary>
        /// <param name="category"> </param>
        public virtual Node category(int? category)
        {
            this["category"] = category;
            return this;
        }

        /// <summary>
        ///     详见 itemStyle
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.ItemStyle
        /// </seealso>
        public virtual ItemStyle itemStyle()
        {
            if (this["itemStyle"] == null)
            {
                this["itemStyle"] = new ItemStyle();
            }
            return (ItemStyle) this["itemStyle"];
        }
    }
}