using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.series.force
{
    /// <summary>
    ///     力导向图的边数据
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class Link
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     详见 itemStyle, 只能设置 lineWidth, strokeColor, lineType 等描边的属怿
        /// </summary>
        private ItemStyle itemStyle_Renamed;

        /// <summary>
        ///     源节点的index或者源节点的name
        /// </summary>
        private object source_Renamed;

        /// <summary>
        ///     目标节点的index或者目标节点的name
        /// </summary>
        private object target_Renamed;

        /// <summary>
        ///     边的权重，权重越大邻接节点越靠拢
        /// </summary>
        private int? weight_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Link()
        {
        }

        /// <summary>
        ///     构造函敿参数:source,target,weight
        /// </summary>
        /// <param name="source"> </param>
        /// <param name="target"> </param>
        /// <param name="weight"> </param>
        public Link(object source, object target, int? weight)
        {
            source_Renamed = source;
            target_Renamed = target;
            weight_Renamed = weight;
        }

        /// <summary>
        ///     获取itemStyle值
        /// </summary>
        public virtual ItemStyle ItemStyle
        {
            get { return itemStyle_Renamed; }
            set { itemStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取source值
        /// </summary>
        public virtual object Source
        {
            get { return source_Renamed; }
            set { source_Renamed = value; }
        }


        /// <summary>
        ///     获取target值
        /// </summary>
        public virtual object Target
        {
            get { return target_Renamed; }
            set { target_Renamed = value; }
        }


        /// <summary>
        ///     获取weight值
        /// </summary>
        public virtual int? Weight
        {
            get { return weight_Renamed; }
            set { weight_Renamed = value; }
        }

        /// <summary>
        ///     获取source值
        /// </summary>
        public virtual object source()
        {
            return source_Renamed;
        }

        /// <summary>
        ///     设置source值     *
        /// </summary>
        /// <param name="source"> </param>
        public virtual Link source(object source)
        {
            source_Renamed = source;
            return this;
        }

        /// <summary>
        ///     获取target值
        /// </summary>
        public virtual object target()
        {
            return target_Renamed;
        }

        /// <summary>
        ///     设置target值     *
        /// </summary>
        /// <param name="target"> </param>
        public virtual Link target(object target)
        {
            target_Renamed = target;
            return this;
        }

        /// <summary>
        ///     获取weight值
        /// </summary>
        public virtual int? weight()
        {
            return weight_Renamed;
        }

        /// <summary>
        ///     设置weight值     *
        /// </summary>
        /// <param name="weight"> </param>
        public virtual Link weight(int? weight)
        {
            weight_Renamed = weight;
            return this;
        }

        /// <summary>
        ///     详见 itemStyle, 只能设置 lineWidth, strokeColor, lineType 等描边的属怿
        /// </summary>
        public virtual ItemStyle itemStyle()
        {
            if (itemStyle_Renamed == null)
            {
                itemStyle_Renamed = new ItemStyle();
            }
            return itemStyle_Renamed;
        }
    }
}