/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     柱形囿 *
    ///     @author Yongjin.C
    /// </summary>
    public class Bar : Series<Bar>
    {
        /// <summary>
        ///     类目间柱形距离，默认为类目间距的20%，可设固定值
        /// </summary>
        private string barCategoryGap_Renamed;

        /// <summary>
        ///     柱间距离，默认为柱形宽度皿0%，可设固定值
        /// </summary>
        private string barGap_Renamed;

        /// <summary>
        ///     柱条最小高度，可用于防止某item的值过小而影响交亿
        /// </summary>
        private int? barMinHeight_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Bar()
        {
            type(SeriesType.bar);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Bar(string name) : base(name)
        {
            type(SeriesType.bar);
        }

        /// <summary>
        ///     获取barMinHeight值
        /// </summary>
        public virtual int? BarMinHeight
        {
            get { return barMinHeight_Renamed; }
            set { barMinHeight_Renamed = value; }
        }


        /// <summary>
        ///     获取barGap值
        /// </summary>
        public virtual string BarGap
        {
            get { return barGap_Renamed; }
            set { barGap_Renamed = value; }
        }


        /// <summary>
        ///     获取barCategoryGap值
        /// </summary>
        public virtual string BarCategoryGap
        {
            get { return barCategoryGap_Renamed; }
            set { barCategoryGap_Renamed = value; }
        }

        /// <summary>
        ///     获取barMinHeight值
        /// </summary>
        public virtual int? barMinHeight()
        {
            return barMinHeight_Renamed;
        }

        /// <summary>
        ///     设置barMinHeight值     *
        /// </summary>
        /// <param name="barMinHeight"> </param>
        public virtual Bar barMinHeight(int? barMinHeight)
        {
            barMinHeight_Renamed = barMinHeight;
            return this;
        }

        /// <summary>
        ///     获取barGap值
        /// </summary>
        public virtual string barGap()
        {
            return barGap_Renamed;
        }

        /// <summary>
        ///     设置barGap值     *
        /// </summary>
        /// <param name="barGap"> </param>
        public virtual Bar barGap(string barGap)
        {
            barGap_Renamed = barGap;
            return this;
        }

        /// <summary>
        ///     获取barCategoryGap值
        /// </summary>
        public virtual string barCategoryGap()
        {
            return barCategoryGap_Renamed;
        }

        /// <summary>
        ///     设置barCategoryGap值     *
        /// </summary>
        /// <param name="barCategoryGap"> </param>
        public virtual Bar barCategoryGap(string barCategoryGap)
        {
            barCategoryGap_Renamed = barCategoryGap;
            return this;
        }
    }
}