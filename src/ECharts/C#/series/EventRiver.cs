using System.Collections.Generic;
using System.Linq;
using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     事件河流囿
    /// </summary>
    public class EventRiver : Series<EventRiver>
    {
        private List<@event.Event> eventList_Renamed;

        /// <summary>
        ///     该事件类别的权重
        /// </summary>
        private int? weight_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public EventRiver()
        {
            type(SeriesType.eventRiver);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public EventRiver(string name) : base(name)
        {
            type(SeriesType.eventRiver);
        }

        public EventRiver(string name, int? weight) : base(name)
        {
            type(SeriesType.eventRiver);
            this.weight(weight);
        }

        /// <summary>
        ///     获取weight值
        /// </summary>
        public virtual int? Weight
        {
            get { return weight_Renamed; }
            set { weight_Renamed = value; }
        }


        /// <summary>
        ///     获取eventList值
        /// </summary>
        public virtual List<@event.Event> EventList
        {
            get { return eventList_Renamed; }
            set { eventList_Renamed = value; }
        }

        /// <summary>
        ///     获取weight值
        /// </summary>
        public virtual int? weight()
        {
            return weight_Renamed;
        }

        /// <summary>
        ///     设置weight值     *
        /// </summary>
        /// <param name="weight"> </param>
        public virtual EventRiver weight(int? weight)
        {
            weight_Renamed = weight;
            return this;
        }

        /// <summary>
        ///     获取eventList值
        /// </summary>
        public virtual List<@event.Event> eventList()
        {
            if (eventList_Renamed == null)
            {
                eventList_Renamed = new List<@event.Event>();
            }
            return eventList_Renamed;
        }

        /// <summary>
        ///     设置eventList值     *
        /// </summary>
        /// <param name="eventList"> </param>
        public virtual EventRiver eventList(List<@event.Event> eventList)
        {
            eventList_Renamed = eventList;
            return this;
        }

        /// <summary>
        ///     添加eventList值     *
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual EventRiver eventList(params @event.Event[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            eventList().AddRange(values.ToList());
            return this;
        }
    }
}