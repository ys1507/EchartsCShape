/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     漏斗囿 *
    ///     @author Yongjin.C
    /// </summary>
    public class Funnel : Series<Funnel>
    {
        /// <summary>
        ///     水平方向对齐布局类型，默认居中对齐，可用选项还有＿left' | 'right' | 'center'
        /// </summary>
        private X funnelAlign_Renamed;

        /// <summary>
        ///     数据图形间距
        /// </summary>
        private int? gap_Renamed;

        /// <summary>
        ///     总宽度，默认为绘图区总高庿- y - y2，数值单位px，指定width后将忽略x2，支持百分比（字符串），妿50%'(显示区域一半的高度)
        /// </summary>
        private object height_Renamed;

        /// <summary>
        ///     最大值max映射到总宽度的比例
        /// </summary>
        private string maxSize_Renamed;

        /// <summary>
        ///     指定的最大值
        /// </summary>
        private int? max_Renamed;

        /// <summary>
        ///     最小值min映射到总宽度的比例，如果需要最小值的图形并不是尖端三角，可设置minSize实现
        /// </summary>
        private string minSize_Renamed;

        /// <summary>
        ///     指定的最小值
        /// </summary>
        private int? min_Renamed;

        /// <summary>
        ///     数据排序＿可以取ascending, descending
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Sort
        /// </seealso>
        private Sort sort_Renamed;

        /// <summary>
        ///     总宽度，默认为绘图区总宽庿- x - x2，数值单位px，指定width后将忽略x2，支持百分比（字符串），妿50%'(显示区域一半的宽度)
        /// </summary>
        private object width_Renamed;

        /// <summary>
        ///     右下角横坐标，数值单位px，支持百分比（字符串），妿50%'(显示区域横向中心)
        /// </summary>
        private object x2_Renamed;

        /// <summary>
        ///     左上角横坐标，数值单位px，支持百分比（字符串），妿50%'(显示区域横向中心)
        /// </summary>
        private object x_Renamed;

        /// <summary>
        ///     右下角纵坐标，数值单位px，支持百分比（字符串），妿50%'(显示区域纵向中心)
        /// </summary>
        private object y2_Renamed;

        /// <summary>
        ///     左上角纵坐标，数值单位px，支持百分比（字符串），妿50%'(显示区域纵向中心)
        /// </summary>
        private object y_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Funnel()
        {
            type(SeriesType.funnel);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Funnel(string name) : base(name)
        {
            type(SeriesType.funnel);
        }

        /// <summary>
        ///     获取x值
        /// </summary>
        public virtual object X
        {
            get { return x_Renamed; }
            set { x_Renamed = value; }
        }


        /// <summary>
        ///     获取y值
        /// </summary>
        public virtual object Y
        {
            get { return y_Renamed; }
            set { y_Renamed = value; }
        }


        /// <summary>
        ///     获取x2值
        /// </summary>
        public virtual object X2
        {
            get { return x2_Renamed; }
            set { x2_Renamed = value; }
        }


        /// <summary>
        ///     获取y2值
        /// </summary>
        public virtual object Y2
        {
            get { return y2_Renamed; }
            set { y2_Renamed = value; }
        }


        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object Width
        {
            get { return width_Renamed; }
            set { width_Renamed = value; }
        }


        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual object Height
        {
            get { return height_Renamed; }
            set { height_Renamed = value; }
        }


        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual int? Min
        {
            get { return min_Renamed; }
            set { min_Renamed = value; }
        }


        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual int? Max
        {
            get { return max_Renamed; }
            set { max_Renamed = value; }
        }


        /// <summary>
        ///     获取minSize值
        /// </summary>
        public virtual string MinSize
        {
            get { return minSize_Renamed; }
            set { minSize_Renamed = value; }
        }


        /// <summary>
        ///     获取maxSize值
        /// </summary>
        public virtual string MaxSize
        {
            get { return maxSize_Renamed; }
            set { maxSize_Renamed = value; }
        }


        /// <summary>
        ///     获取sort值
        /// </summary>
        public virtual Sort Sort
        {
            get { return sort_Renamed; }
            set { sort_Renamed = value; }
        }


        /// <summary>
        ///     获取gap值
        /// </summary>
        public virtual int? Gap
        {
            get { return gap_Renamed; }
            set { gap_Renamed = value; }
        }


        /// <summary>
        ///     设置funnelAlign值
        /// </summary>
        public virtual X FunnelAlign
        {
            get { return funnelAlign_Renamed; }
            set { funnelAlign_Renamed = value; }
        }

        /// <summary>
        ///     获取x值
        /// </summary>
        public virtual object x()
        {
            return x_Renamed;
        }

        /// <summary>
        ///     设置x值     *
        /// </summary>
        /// <param name="x"> </param>
        public virtual Funnel x(object x)
        {
            x_Renamed = x;
            return this;
        }

        /// <summary>
        ///     获取y值
        /// </summary>
        public virtual object y()
        {
            return y_Renamed;
        }

        /// <summary>
        ///     设置y值     *
        /// </summary>
        /// <param name="y"> </param>
        public virtual Funnel y(object y)
        {
            y_Renamed = y;
            return this;
        }

        /// <summary>
        ///     获取x2值
        /// </summary>
        public virtual object x2()
        {
            return x2_Renamed;
        }

        /// <summary>
        ///     设置x2值     *
        /// </summary>
        /// <param name="x2"> </param>
        public virtual Funnel x2(object x2)
        {
            x2_Renamed = x2;
            return this;
        }

        /// <summary>
        ///     获取y2值
        /// </summary>
        public virtual object y2()
        {
            return y2_Renamed;
        }

        /// <summary>
        ///     设置y2值     *
        /// </summary>
        /// <param name="y2"> </param>
        public virtual Funnel y2(object y2)
        {
            y2_Renamed = y2;
            return this;
        }

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object width()
        {
            return width_Renamed;
        }

        /// <summary>
        ///     设置width值     *
        /// </summary>
        /// <param name="width"> </param>
        public virtual Funnel width(object width)
        {
            width_Renamed = width;
            return this;
        }

        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual object height()
        {
            return height_Renamed;
        }

        /// <summary>
        ///     设置height值     *
        /// </summary>
        /// <param name="height"> </param>
        public virtual Funnel height(object height)
        {
            height_Renamed = height;
            return this;
        }

        /// <summary>
        ///     获取funnelAlign值
        /// </summary>
        public virtual X funnelAlign()
        {
            return funnelAlign_Renamed;
        }

        /// <summary>
        ///     设置funnelAlign值     *
        /// </summary>
        /// <param name="funnelAlign"> </param>
        public virtual Funnel funnelAlign(X funnelAlign)
        {
            funnelAlign_Renamed = funnelAlign;
            return this;
        }

        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual int? min()
        {
            return min_Renamed;
        }

        /// <summary>
        ///     设置min值     *
        /// </summary>
        /// <param name="min"> </param>
        public virtual Funnel min(int? min)
        {
            min_Renamed = min;
            return this;
        }

        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual int? max()
        {
            return max_Renamed;
        }

        /// <summary>
        ///     设置max值     *
        /// </summary>
        /// <param name="max"> </param>
        public virtual Funnel max(int? max)
        {
            max_Renamed = max;
            return this;
        }

        /// <summary>
        ///     获取minSize值
        /// </summary>
        public virtual string minSize()
        {
            return minSize_Renamed;
        }

        /// <summary>
        ///     设置minSize值     *
        /// </summary>
        /// <param name="minSize"> </param>
        public virtual Funnel minSize(string minSize)
        {
            minSize_Renamed = minSize;
            return this;
        }

        /// <summary>
        ///     获取maxSize值
        /// </summary>
        public virtual string maxSize()
        {
            return maxSize_Renamed;
        }

        /// <summary>
        ///     设置maxSize值     *
        /// </summary>
        /// <param name="maxSize"> </param>
        public virtual Funnel maxSize(string maxSize)
        {
            maxSize_Renamed = maxSize;
            return this;
        }

        /// <summary>
        ///     获取sort值
        /// </summary>
        public virtual Sort sort()
        {
            return sort_Renamed;
        }

        /// <summary>
        ///     设置sort值     *
        /// </summary>
        /// <param name="sort"> </param>
        public virtual Funnel sort(Sort sort)
        {
            sort_Renamed = sort;
            return this;
        }

        /// <summary>
        ///     获取gap值
        /// </summary>
        public virtual int? gap()
        {
            return gap_Renamed;
        }

        /// <summary>
        ///     设置gap值     *
        /// </summary>
        /// <param name="gap"> </param>
        public virtual Funnel gap(int? gap)
        {
            gap_Renamed = gap;
            return this;
        }
    }
}