/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     雷达囿 *
    ///     @author Yongjin.C
    /// </summary>
    public class Radar : Series<Radar>
    {
        /// <summary>
        ///     极坐标索弿
        /// </summary>
        private int? polarIndex_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Radar()
        {
            type(SeriesType.radar);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Radar(string name) : base(name)
        {
            type(SeriesType.radar);
        }

        /// <summary>
        ///     获取polarIndex值
        /// </summary>
        public virtual int? PolarIndex
        {
            get { return polarIndex_Renamed; }
            set { polarIndex_Renamed = value; }
        }

        /// <summary>
        ///     获取polarIndex值
        /// </summary>
        public virtual int? polarIndex()
        {
            return polarIndex_Renamed;
        }

        /// <summary>
        ///     设置polarIndex值     *
        /// </summary>
        /// <param name="polarIndex"> </param>
        public virtual Radar polarIndex(int? polarIndex)
        {
            polarIndex_Renamed = polarIndex;
            return this;
        }
    }
}