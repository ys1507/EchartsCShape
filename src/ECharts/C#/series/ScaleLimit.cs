using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class ScaleLimit
    {
        private const long serialVersionUID = 1L;

        private double? max_Renamed;
        private double? min_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public ScaleLimit()
        {
        }

        /// <summary>
        ///     构造函敿参数:min,max
        /// </summary>
        /// <param name="min"> </param>
        /// <param name="max"> </param>
        public ScaleLimit(double? min, double? max)
        {
            min_Renamed = min;
            max_Renamed = max;
        }

        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual double? Min
        {
            get { return min_Renamed; }
            set { min_Renamed = value; }
        }


        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual double? Max
        {
            get { return max_Renamed; }
            set { max_Renamed = value; }
        }

        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual double? min()
        {
            return min_Renamed;
        }

        /// <summary>
        ///     设置min值     *
        /// </summary>
        /// <param name="min"> </param>
        public virtual ScaleLimit min(double? min)
        {
            min_Renamed = min;
            return this;
        }

        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual double? max()
        {
            return max_Renamed;
        }

        /// <summary>
        ///     设置max值     *
        /// </summary>
        /// <param name="max"> </param>
        public virtual ScaleLimit max(double? max)
        {
            max_Renamed = max;
            return this;
        }
    }
}