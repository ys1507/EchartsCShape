/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System.Collections.Generic;
using System.Linq;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.series.force;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     Description: Force
    ///     @author Yongjin.C
    /// </summary>
    public class Force : Series<Force>
    {
        /// <summary>
        ///     力导向图中节点的分类
        /// </summary>
        private List<Category> categories_Renamed;

        /// <summary>
        ///     布局中心，可以是绝对值或者相对百分比
        /// </summary>
        private object center_Renamed;

        /// <summary>
        ///     布局冷却因子，值越小结束时间越短，值越大时间越长但是结果也越收敿
        /// </summary>
        private object coolDown_Renamed;

        /// <summary>
        ///     节点是否能被拖拽
        /// </summary>
        private bool? draggable_Renamed;

        /// <summary>
        ///     向心力系数，系数越大则节点越往中心靠拢
        /// </summary>
        private double? gravity_Renamed;

        /// <summary>
        ///     圿500+ 顶点的图上建议设罿large 丿true, 会使甿Barnes-Hut simulation, 同时开吿useWorker 并且抿steps 值调夿
        /// </summary>
        private bool? large_Renamed;

        /// <summary>
        ///     力导向图的边两端图形大小
        /// </summary>
        private int? linkSymbolSize_Renamed;

        /// <summary>
        ///     力导向图的边两端图形样式，可指定丿arrow', 详见symbolList
        /// </summary>
        private object linkSymbol_Renamed;

        /// <summary>
        ///     力导向图的边数据
        /// </summary>
        private List<Link> links_Renamed;

        /// <summary>
        ///     顶点数据映射成圆半径后的最大半徿
        /// </summary>
        private int? maxRadius_Renamed;

        /// <summary>
        ///     顶点数据映射成圆半径后的最小半徿
        /// </summary>
        private int? minRadius_Renamed;

        /// <summary>
        ///     力导向图的顶点数捿
        /// </summary>
        private List<Node> nodes_Renamed;

        /// <summary>
        ///     防止节点和节点，节点和边之间的重叿
        /// </summary>
        private bool? preventOverlap_Renamed;

        /// <summary>
        ///     是否根据屏幕比例拉伸
        /// </summary>
        private bool? ratioScaling_Renamed;

        /// <summary>
        ///     是否开启滚轮缩放和拖拽漫游，默认为false（关闭），其他有效输入为true（开启）＿scale'（仅开启滚轮缩放）＿move'（仅开启拖拽漫游）
        /// </summary>
        private object roam_Renamed;

        /// <summary>
        ///     布局缩放系数，并不完全精硿 效果跟布局大小类似
        /// </summary>
        private double? scaling_Renamed;

        /// <summary>
        ///     布局大小，可以是绝对值或者相对百分比
        /// </summary>
        private object size_Renamed;

        /// <summary>
        ///     每一帧布局计算的迭代次数，因为每一帧绘制的时间经常会比布局时间长很多，所以在使用 web worker 的时候可以把 steps 调大来平衡两者的时间从而达到效率最优化
        /// </summary>
        private int? steps_Renamed;

        /// <summary>
        ///     是否在浏览器支持 web worker 的时候把布局计算放入 web worker 丿
        /// </summary>
        private bool? useWorker_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Force()
        {
            type(SeriesType.force);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Force(string name) : base(name)
        {
            type(SeriesType.force);
        }

        /// <summary>
        ///     获取categories值
        /// </summary>
        public virtual List<Category> Categories
        {
            get { return categories_Renamed; }
            set { categories_Renamed = value; }
        }


        /// <summary>
        ///     获取nodes值
        /// </summary>
        public virtual List<Node> Nodes
        {
            get { return nodes_Renamed; }
            set { nodes_Renamed = value; }
        }


        /// <summary>
        ///     获取links值
        /// </summary>
        public virtual List<Link> Links
        {
            get { return links_Renamed; }
            set { links_Renamed = value; }
        }


        /// <summary>
        ///     获取center值
        /// </summary>
        public virtual object Center
        {
            get { return center_Renamed; }
            set { center_Renamed = value; }
        }


        /// <summary>
        ///     获取size值
        /// </summary>
        public virtual object Size
        {
            get { return size_Renamed; }
            set { size_Renamed = value; }
        }


        /// <summary>
        ///     获取minRadius值
        /// </summary>
        public virtual int? MinRadius
        {
            get { return minRadius_Renamed; }
            set { minRadius_Renamed = value; }
        }


        /// <summary>
        ///     获取maxRadius值
        /// </summary>
        public virtual int? MaxRadius
        {
            get { return maxRadius_Renamed; }
            set { maxRadius_Renamed = value; }
        }


        /// <summary>
        ///     获取linkSymbol值
        /// </summary>
        public virtual object LinkSymbol
        {
            get { return linkSymbol_Renamed; }
            set { linkSymbol_Renamed = value; }
        }


        /// <summary>
        ///     获取linkSymbolSize值
        /// </summary>
        public virtual int? LinkSymbolSize
        {
            get { return linkSymbolSize_Renamed; }
            set { linkSymbolSize_Renamed = value; }
        }


        /// <summary>
        ///     获取scaling值
        /// </summary>
        public virtual double? Scaling
        {
            get { return scaling_Renamed; }
            set { scaling_Renamed = value; }
        }


        /// <summary>
        ///     获取gravity值
        /// </summary>
        public virtual double? Gravity
        {
            get { return gravity_Renamed; }
            set { gravity_Renamed = value; }
        }


        /// <summary>
        ///     获取draggable值
        /// </summary>
        public virtual bool? Draggable
        {
            get { return draggable_Renamed; }
            set { draggable_Renamed = value; }
        }


        /// <summary>
        ///     获取large值
        /// </summary>
        public virtual bool? Large
        {
            get { return large_Renamed; }
            set { large_Renamed = value; }
        }


        /// <summary>
        ///     获取useWorker值
        /// </summary>
        public virtual bool? UseWorker
        {
            get { return useWorker_Renamed; }
            set { useWorker_Renamed = value; }
        }


        /// <summary>
        ///     获取steps值
        /// </summary>
        public virtual int? Steps
        {
            get { return steps_Renamed; }
            set { steps_Renamed = value; }
        }


        /// <summary>
        ///     获取coolDown值
        /// </summary>
        public virtual object CoolDown
        {
            get { return coolDown_Renamed; }
            set { coolDown_Renamed = value; }
        }


        /// <summary>
        ///     获取ratioScaling值
        /// </summary>
        public virtual bool? RatioScaling
        {
            get { return ratioScaling_Renamed; }
            set { ratioScaling_Renamed = value; }
        }


        /// <summary>
        ///     获取preventOverlap值
        /// </summary>
        public virtual bool? PreventOverlap
        {
            get { return preventOverlap_Renamed; }
            set { preventOverlap_Renamed = value; }
        }


        /// <summary>
        ///     获取roam值
        /// </summary>
        public virtual object Roam
        {
            get { return roam_Renamed; }
            set { roam_Renamed = value; }
        }

        /// <summary>
        ///     获取coolDown值
        /// </summary>
        public virtual object coolDown()
        {
            return coolDown_Renamed;
        }

        /// <summary>
        ///     设置coolDown值     *
        /// </summary>
        /// <param name="coolDown"> </param>
        public virtual Force coolDown(object coolDown)
        {
            coolDown_Renamed = coolDown;
            return this;
        }

        /// <summary>
        ///     获取ratioScaling值
        /// </summary>
        public virtual bool? ratioScaling()
        {
            return ratioScaling_Renamed;
        }

        /// <summary>
        ///     设置ratioScaling值     *
        /// </summary>
        /// <param name="ratioScaling"> </param>
        public virtual Force ratioScaling(bool? ratioScaling)
        {
            ratioScaling_Renamed = ratioScaling;
            return this;
        }


        /// <summary>
        ///     获取preventOverlap值
        /// </summary>
        public virtual bool? preventOverlap()
        {
            return preventOverlap_Renamed;
        }

        /// <summary>
        ///     设置preventOverlap值     *
        /// </summary>
        /// <param name="preventOverlap"> </param>
        public virtual Force preventOverlap(bool? preventOverlap)
        {
            preventOverlap_Renamed = preventOverlap;
            return this;
        }

        /// <summary>
        ///     设置categories值     *
        /// </summary>
        /// <param name="categories"> </param>
        public virtual Force categories(List<Category> categories)
        {
            categories_Renamed = categories;
            return this;
        }

        /// <summary>
        ///     设置nodes值     *
        /// </summary>
        /// <param name="nodes"> </param>
        public virtual Force nodes(List<Node> nodes)
        {
            nodes_Renamed = nodes;
            return this;
        }

        /// <summary>
        ///     设置links值     *
        /// </summary>
        /// <param name="links"> </param>
        public virtual Force links(List<Link> links)
        {
            links_Renamed = links;
            return this;
        }

        /// <summary>
        ///     力导向图中节点的分类
        /// </summary>
        public virtual List<Category> categories()
        {
            if (categories_Renamed == null)
            {
                categories_Renamed = new List<Category>();
            }
            return categories_Renamed;
        }

        /// <summary>
        ///     添加节点分类
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Force categories(params Category[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            categories().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     添加节点分类，使用分类名
        /// </summary>
        /// <param name="names">
        ///     @return
        /// </param>
        public virtual Force categories(params string[] names)
        {
            if (names == null || names.Length == 0)
            {
                return this;
            }
            foreach (string name in names)
            {
                categories().Add(new Category(name));
            }
            return this;
        }

        /// <summary>
        ///     添加节点分类，使用分类名
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Force categories(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            foreach (object value in values)
            {
                if (value is string)
                {
                    categories().Add(new Category((string) value));
                }
                else if (value is Category)
                {
                    categories().Add((Category) value);
                }
                //其他忽略
            }
            return this;
        }

        /// <summary>
        ///     力导向图的顶点数捿
        /// </summary>
        public virtual List<Node> nodes()
        {
            if (nodes_Renamed == null)
            {
                nodes_Renamed = new List<Node>();
            }
            return nodes_Renamed;
        }

        /// <summary>
        ///     添加力导向图的顶点数捿     *
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Force nodes(params Node[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            nodes().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     力导向图的边数据
        /// </summary>
        public virtual List<Link> links()
        {
            if (links_Renamed == null)
            {
                links_Renamed = new List<Link>();
            }
            return links_Renamed;
        }

        /// <summary>
        ///     添加力导向图的边数据
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Force links(params Link[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            links().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     获取center值
        /// </summary>
        public virtual object center()
        {
            return center_Renamed;
        }

        /// <summary>
        ///     设置center值     *
        /// </summary>
        /// <param name="center"> </param>
        public virtual Force center(object center)
        {
            center_Renamed = center;
            return this;
        }

        /// <summary>
        ///     获取size值
        /// </summary>
        public virtual object size()
        {
            return size_Renamed;
        }

        /// <summary>
        ///     设置size值     *
        /// </summary>
        /// <param name="size"> </param>
        public virtual Force size(object size)
        {
            size_Renamed = size;
            return this;
        }

        /// <summary>
        ///     获取minRadius值
        /// </summary>
        public virtual int? minRadius()
        {
            return minRadius_Renamed;
        }

        /// <summary>
        ///     设置minRadius值     *
        /// </summary>
        /// <param name="minRadius"> </param>
        public virtual Force minRadius(int? minRadius)
        {
            minRadius_Renamed = minRadius;
            return this;
        }

        /// <summary>
        ///     获取maxRadius值
        /// </summary>
        public virtual int? maxRadius()
        {
            return maxRadius_Renamed;
        }

        /// <summary>
        ///     设置maxRadius值     *
        /// </summary>
        /// <param name="maxRadius"> </param>
        public virtual Force maxRadius(int? maxRadius)
        {
            maxRadius_Renamed = maxRadius;
            return this;
        }

        /// <summary>
        ///     获取linkSymbol值
        /// </summary>
        public virtual object linkSymbol()
        {
            return linkSymbol_Renamed;
        }

        /// <summary>
        ///     设置linkSymbol值     *
        /// </summary>
        /// <param name="linkSymbol"> </param>
        public virtual Force linkSymbol(Symbol linkSymbol)
        {
            linkSymbol_Renamed = linkSymbol;
            return this;
        }

        /// <summary>
        ///     设置linkSymbol值     *
        /// </summary>
        /// <param name="linkSymbol"> </param>
        public virtual Force linkSymbol(string linkSymbol)
        {
            linkSymbol_Renamed = linkSymbol;
            return this;
        }

        /// <summary>
        ///     获取linkSymbolSize值
        /// </summary>
        public virtual int? linkSymbolSize()
        {
            return linkSymbolSize_Renamed;
        }

        /// <summary>
        ///     设置linkSymbolSize值     *
        /// </summary>
        /// <param name="linkSymbolSize"> </param>
        public virtual Force linkSymbolSize(int? linkSymbolSize)
        {
            linkSymbolSize_Renamed = linkSymbolSize;
            return this;
        }

        /// <summary>
        ///     获取scaling值
        /// </summary>
        public virtual double? scaling()
        {
            return scaling_Renamed;
        }

        /// <summary>
        ///     设置scaling值     *
        /// </summary>
        /// <param name="scaling"> </param>
        public virtual Force scaling(double? scaling)
        {
            scaling_Renamed = scaling;
            return this;
        }

        /// <summary>
        ///     获取gravity值
        /// </summary>
        public virtual double? gravity()
        {
            return gravity_Renamed;
        }

        /// <summary>
        ///     设置gravity值     *
        /// </summary>
        /// <param name="gravity"> </param>
        public virtual Force gravity(double? gravity)
        {
            gravity_Renamed = gravity;
            return this;
        }

        /// <summary>
        ///     获取draggable值
        /// </summary>
        public virtual bool? draggable()
        {
            return draggable_Renamed;
        }

        /// <summary>
        ///     设置draggable值     *
        /// </summary>
        /// <param name="draggable"> </param>
        public virtual Force draggable(bool? draggable)
        {
            draggable_Renamed = draggable;
            return this;
        }

        /// <summary>
        ///     获取large值
        /// </summary>
        public virtual bool? large()
        {
            return large_Renamed;
        }

        /// <summary>
        ///     设置large值     *
        /// </summary>
        /// <param name="large"> </param>
        public virtual Force large(bool? large)
        {
            large_Renamed = large;
            return this;
        }

        /// <summary>
        ///     获取useWorker值
        /// </summary>
        public virtual bool? useWorker()
        {
            return useWorker_Renamed;
        }

        /// <summary>
        ///     设置useWorker值     *
        /// </summary>
        /// <param name="useWorker"> </param>
        public virtual Force useWorker(bool? useWorker)
        {
            useWorker_Renamed = useWorker;
            return this;
        }

        /// <summary>
        ///     获取steps值
        /// </summary>
        public virtual int? steps()
        {
            return steps_Renamed;
        }

        /// <summary>
        ///     设置steps值     *
        /// </summary>
        /// <param name="steps"> </param>
        public virtual Force steps(int? steps)
        {
            steps_Renamed = steps;
            return this;
        }

        /// <summary>
        ///     获取roam值
        /// </summary>
        public virtual object roam()
        {
            return roam_Renamed;
        }

        /// <summary>
        ///     设置roam值     *
        /// </summary>
        /// <param name="roam"> </param>
        public virtual Force roam(bool? roam)
        {
            roam_Renamed = roam;
            return this;
        }

        /// <summary>
        ///     设置roam值     *
        /// </summary>
        /// <param name="roam"> </param>
        public virtual Force roam(Roam roam)
        {
            roam_Renamed = roam;
            return this;
        }
    }
}