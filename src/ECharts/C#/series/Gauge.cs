/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.axis;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.series.gauge;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     仪表盿 *
    ///     @author Yongjin.C
    /// </summary>
    public class Gauge : Series<Gauge>
    {
        /// <summary>
        ///     坐标轴文本标筿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.Label
        /// </seealso>
        private Label axisLabel_Renamed;

        public Label Label
        {
            set { axisLabel_Renamed = value; }
            get { return axisLabel_Renamed; }
        }

        /// <summary>
        ///     坐标轴线，默认显礿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Line
        /// </seealso>
        private Line axisLine_Renamed;
        public Line Line
        {
            set { axisLine_Renamed = value; }
            get { return axisLine_Renamed; }
        }

        /// <summary>
        ///     坐标轴小标记，默认显礿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisTick
        /// </seealso>
        private AxisTick axisTick_Renamed;
       
        /// <summary>
        ///     圆心坐标，支持绝对值（px）和百分比，百分比计算min(width, height) * 50%
        /// </summary>
        private object[] center_Renamed;

        /// <summary>
        ///     仪表盘详惿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.gauge.Detail
        /// </seealso>
        private Detail detail_Renamed;
        
        /// <summary>
        ///     结束角度,有效输入范围：[-360,360]，保证startAngle - endAngle为正值
        /// </summary>
        private int? endAngle_Renamed;

        /// <summary>
        ///     指定的最大值
        /// </summary>
        private int? max_Renamed;

        /// <summary>
        ///     指定的最小值
        /// </summary>
        private int? min_Renamed;

        /// <summary>
        ///     指针样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.gauge.Pointer
        /// </seealso>
        private Pointer pointer_Renamed;
        
        /// <summary>
        ///     小数精度，默认为0，无小数炿
        /// </summary>
        private int? precision_Renamed;

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        private object radius_Renamed;

        /// <summary>
        ///     主分隔线，默认显礿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.SplitLine
        /// </seealso>
        private SplitLine splitLine_Renamed;
       
        /// <summary>
        ///     分割段数，默认为5，为0时为线性渐变，calculable为true是默认均刿00仿
        /// </summary>
        private int? splitNumber_Renamed;

        /// <summary>
        ///     开始角庿 饼图＿0）、仪表盘＿25），有效输入范围：[-360,360]
        /// </summary>
        private int? startAngle_Renamed;

        /// <summary>
        ///     仪表盘标颿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.Title
        /// </seealso>
        private Title title_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Gauge()
        {
            type(SeriesType.gauge);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Gauge(string name) : base(name)
        {
            type(SeriesType.gauge);
        }

        /// <summary>
        ///     获取center值
        /// </summary>
        public virtual object[] Center
        {
            get { return center_Renamed; }
            set { center_Renamed = value; }
        }


        /// <summary>
        ///     获取radius值
        /// </summary>
        public virtual object Radius
        {
            get { return radius_Renamed; }
            set { radius_Renamed = value; }
        }

        /// <summary>
        ///     获取axisTick值
        /// </summary>
        public virtual AxisTick AxisTick
        {
            get { return axisTick_Renamed; }
            set { axisTick_Renamed = value; }
        }


        /// <summary>
        ///     获取axisLabel值
        /// </summary>
        public virtual Label AxisLabel
        {
            get { return axisLabel_Renamed; }
            set { axisLabel_Renamed = value; }
        }


        /// <summary>
        ///     获取splitLine值
        /// </summary>
        public virtual SplitLine SplitLine
        {
            get { return splitLine_Renamed; }
            set { splitLine_Renamed = value; }
        }


        /// <summary>
        ///     获取pointer值
        /// </summary>
        public virtual Pointer Pointer
        {
            get { return pointer_Renamed; }
            set { pointer_Renamed = value; }
        }


        /// <summary>
        ///     获取title值
        /// </summary>
        public virtual Title Title
        {
            get { return title_Renamed; }
            set { title_Renamed = value; }
        }


        /// <summary>
        ///     获取detail值
        /// </summary>
        public virtual Detail Detail
        {
            get { return detail_Renamed; }
            set { detail_Renamed = value; }
        }


        /// <summary>
        ///     获取startAngle值
        /// </summary>
        public virtual int? StartAngle
        {
            get { return startAngle_Renamed; }
            set { startAngle_Renamed = value; }
        }


        /// <summary>
        ///     获取endAngle值
        /// </summary>
        public virtual int? EndAngle
        {
            get { return endAngle_Renamed; }
            set { endAngle_Renamed = value; }
        }


        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual int? Min
        {
            get { return min_Renamed; }
            set { min_Renamed = value; }
        }


        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual int? Max
        {
            get { return max_Renamed; }
            set { max_Renamed = value; }
        }


        /// <summary>
        ///     获取precision值
        /// </summary>
        public virtual int? Precision
        {
            get { return precision_Renamed; }
            set { precision_Renamed = value; }
        }


        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? SplitNumber
        {
            get { return splitNumber_Renamed; }
            set { splitNumber_Renamed = value; }
        }

        /// <summary>
        ///     获取center值
        /// </summary>
        public virtual object[] center()
        {
            return center_Renamed;
        }

        /// <summary>
        ///     设置center值     *
        /// </summary>
        /// <param name="center"> </param>
        public virtual Gauge center(object[] center)
        {
            center_Renamed = center;
            return this;
        }

        /// <summary>
        ///     获取radius值
        /// </summary>
        public virtual object radius()
        {
            return radius_Renamed;
        }

        /// <summary>
        ///     设置axisLine值     *
        /// </summary>
        /// <param name="axisLine"> </param>
        public virtual Gauge axisLine(Line axisLine)
        {
            axisLine_Renamed = axisLine;
            return this;
        }

        /// <summary>
        ///     设置axisTick值     *
        /// </summary>
        /// <param name="axisTick"> </param>
        public virtual Gauge axisTick(AxisTick axisTick)
        {
            axisTick_Renamed = axisTick;
            return this;
        }

        /// <summary>
        ///     设置axisLabel值     *
        /// </summary>
        /// <param name="axisLabel"> </param>
        public virtual Gauge axisLabel(Label axisLabel)
        {
            axisLabel_Renamed = axisLabel;
            return this;
        }

        /// <summary>
        ///     设置splitLine值     *
        /// </summary>
        /// <param name="splitLine"> </param>
        public virtual Gauge splitLine(SplitLine splitLine)
        {
            splitLine_Renamed = splitLine;
            return this;
        }

        /// <summary>
        ///     设置pointer值     *
        /// </summary>
        /// <param name="pointer"> </param>
        public virtual Gauge pointer(Pointer pointer)
        {
            pointer_Renamed = pointer;
            return this;
        }

        /// <summary>
        ///     设置title值     *
        /// </summary>
        /// <param name="title"> </param>
        public virtual Gauge title(Title title)
        {
            title_Renamed = title;
            return this;
        }

        /// <summary>
        ///     设置detail值     *
        /// </summary>
        /// <param name="detail"> </param>
        public virtual Gauge detail(Detail detail)
        {
            detail_Renamed = detail;
            return this;
        }

        /// <summary>
        ///     圆心坐标，支持绝对值（px）和百分比，百分比计算min(width, height) * 50%
        /// </summary>
        public virtual Gauge center(object width, object height)
        {
            center_Renamed = new[] {width, height};
            return this;
        }

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        /// <param name="radius">
        ///     @return
        /// </param>
        public virtual Gauge radius(object radius)
        {
            radius_Renamed = radius;
            return this;
        }

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        /// <param name="width"> </param>
        /// <param name="height">
        ///     @return
        /// </param>
        public virtual Gauge radius(object width, object height)
        {
            radius_Renamed = new[] {width, height};
            return this;
        }

        /// <summary>
        ///     获取startAngle值
        /// </summary>
        public virtual int? startAngle()
        {
            return startAngle_Renamed;
        }

        /// <summary>
        ///     设置startAngle值     *
        /// </summary>
        /// <param name="startAngle"> </param>
        public virtual Gauge startAngle(int? startAngle)
        {
            startAngle_Renamed = startAngle;
            return this;
        }

        /// <summary>
        ///     获取endAngle值
        /// </summary>
        public virtual int? endAngle()
        {
            return endAngle_Renamed;
        }

        /// <summary>
        ///     设置endAngle值     *
        /// </summary>
        /// <param name="endAngle"> </param>
        public virtual Gauge endAngle(int? endAngle)
        {
            endAngle_Renamed = endAngle;
            return this;
        }

        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual int? min()
        {
            return min_Renamed;
        }

        /// <summary>
        ///     设置min值     *
        /// </summary>
        /// <param name="min"> </param>
        public virtual Gauge min(int? min)
        {
            min_Renamed = min;
            return this;
        }

        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual int? max()
        {
            return max_Renamed;
        }

        /// <summary>
        ///     设置max值     *
        /// </summary>
        /// <param name="max"> </param>
        public virtual Gauge max(int? max)
        {
            max_Renamed = max;
            return this;
        }

        /// <summary>
        ///     获取precision值
        /// </summary>
        public virtual int? precision()
        {
            return precision_Renamed;
        }

        /// <summary>
        ///     设置precision值     *
        /// </summary>
        /// <param name="precision"> </param>
        public virtual Gauge precision(int? precision)
        {
            precision_Renamed = precision;
            return this;
        }

        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? splitNumber()
        {
            return splitNumber_Renamed;
        }

        /// <summary>
        ///     设置splitNumber值     *
        /// </summary>
        /// <param name="splitNumber"> </param>
        public virtual Gauge splitNumber(int? splitNumber)
        {
            splitNumber_Renamed = splitNumber;
            return this;
        }

        /// <summary>
        ///     坐标轴线，默认显礿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Line
        /// </seealso>
        public virtual Line axisLine()
        {
            if (axisLine_Renamed == null)
            {
                axisLine_Renamed = new Line();
            }
            return axisLine_Renamed;
        }

        /// <summary>
        ///     坐标轴小标记，默认显礿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisTick
        /// </seealso>
        public virtual AxisTick axisTick()
        {
            if (axisTick_Renamed == null)
            {
                axisTick_Renamed = new AxisTick();
            }
            return axisTick_Renamed;
        }

        /// <summary>
        ///     坐标轴文本标筿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.Label
        /// </seealso>
        public virtual Label axisLabel()
        {
            if (axisLabel_Renamed == null)
            {
                axisLabel_Renamed = new Label();
            }
            return axisLabel_Renamed;
        }

        /// <summary>
        ///     主分隔线，默认显礿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.SplitLine
        /// </seealso>
        public virtual SplitLine splitLine()
        {
            if (splitLine_Renamed == null)
            {
                splitLine_Renamed = new SplitLine();
            }
            return splitLine_Renamed;
        }

        /// <summary>
        ///     指针样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.gauge.Pointer
        /// </seealso>
        public virtual Pointer pointer()
        {
            if (pointer_Renamed == null)
            {
                pointer_Renamed = new Pointer();
            }
            return pointer_Renamed;
        }

        /// <summary>
        ///     仪表盘标颿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.Title
        /// </seealso>
        public virtual Title title()
        {
            if (title_Renamed == null)
            {
                title_Renamed = new Title();
            }
            return title_Renamed;
        }

        /// <summary>
        ///     仪表盘详惿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.gauge.Detail
        /// </seealso>
        public virtual Detail detail()
        {
            if (detail_Renamed == null)
            {
                detail_Renamed = new Detail();
            }
            return detail_Renamed;
        }


        /// <summary>
        ///     获取axisLine值
        /// </summary>
        public virtual Line getAxisLine()
        {
            return axisLine_Renamed;
        }

        /// <summary>
        ///     设置axisLine值     *
        /// </summary>
        /// <param name="axisLine"> </param>
        public virtual void setAxisLine(Line axisLine)
        {
            axisLine_Renamed = axisLine;
        }
    }
}