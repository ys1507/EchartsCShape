/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     饼图
    ///     @author Yongjin.C
    /// </summary>
    public class Pie : Series<Pie>
    {
        /// <summary>
        ///     圆心坐标，支持绝对值（px）和百分比，百分比计算min(width, height) * 50%
        /// </summary>
        private object[] center_Renamed;

        /// <summary>
        ///     显示是否顺时钿
        /// </summary>
        private bool? clockWise_Renamed;

        /// <summary>
        ///     最小角度，可用于防止某item的值过小而影响交亿
        /// </summary>
        private int? minAngle_Renamed;

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        private object radius_Renamed;

        /// <summary>
        ///     南丁格尔玫瑰图模式，'radius'（半径） | 'area'（面积）
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.RoseType
        /// </seealso>
        private RoseType roseType_Renamed;

        /// <summary>
        ///     选中模式，默认关闭，可选single，multiple
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.SelectedMode
        /// </seealso>
        private SelectedMode selectedMode_Renamed;

        /// <summary>
        ///     选中是扇区偏移量
        /// </summary>
        private int? selectedOffset_Renamed;

        /// <summary>
        ///     开始角庿 饼图＿0）、仪表盘＿25），有效输入范围：[-360,360]
        /// </summary>
        private int? startAngle_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Pie()
        {
            type(SeriesType.pie);
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Pie(string name) : base(name)
        {
            type(SeriesType.pie);
        }

        /// <summary>
        ///     获取center值
        /// </summary>
        public virtual object[] Center
        {
            get { return center_Renamed; }
            set { center_Renamed = value; }
        }


        /// <summary>
        ///     获取radius值
        /// </summary>
        public virtual object Radius
        {
            get { return radius_Renamed; }
            set { radius_Renamed = value; }
        }


        public virtual int? StartAngle
        {
            get { return startAngle_Renamed; }
            set { startAngle_Renamed = value; }
        }


        public virtual int? MinAngle
        {
            get { return minAngle_Renamed; }
            set { minAngle_Renamed = value; }
        }


        public virtual bool? ClockWise
        {
            get { return clockWise_Renamed; }
            set { clockWise_Renamed = value; }
        }


        public virtual RoseType RoseType
        {
            get { return roseType_Renamed; }
            set { roseType_Renamed = value; }
        }


        public virtual int? SelectedOffset
        {
            get { return selectedOffset_Renamed; }
            set { selectedOffset_Renamed = value; }
        }


        public virtual SelectedMode SelectedMode
        {
            get { return selectedMode_Renamed; }
            set { selectedMode_Renamed = value; }
        }

        /// <summary>
        ///     获取center值
        /// </summary>
        public virtual object[] center()
        {
            return center_Renamed;
        }

        /// <summary>
        ///     设置center值     *
        /// </summary>
        /// <param name="center"> </param>
        public virtual Pie center(object[] center)
        {
            center_Renamed = center;
            return this;
        }

        /// <summary>
        ///     获取radius值
        /// </summary>
        public virtual object radius()
        {
            return radius_Renamed;
        }

        /// <summary>
        ///     圆心坐标，支持绝对值（px）和百分比，百分比计算min(width, height) * 50%
        /// </summary>
        /// <param name="width"> </param>
        /// <param name="height">
        ///     @return
        /// </param>
        public virtual Pie center(object width, object height)
        {
            center_Renamed = new[] {width, height};
            return this;
        }

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        /// <param name="radius">
        ///     @return
        /// </param>
        public virtual Pie radius(object radius)
        {
            radius_Renamed = radius;
            return this;
        }

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        /// </param>
        /// <param name="width">  内半徿     *
        ///     <param name="height"> 外半徿     * @return </param>
        public virtual Pie radius(object width, object height)
        {
            radius_Renamed = new[] {width, height};
            return this;
        }

        /// <summary>
        ///     获取startAngle值
        /// </summary>
        public virtual int? startAngle()
        {
            return startAngle_Renamed;
        }

        /// <summary>
        ///     设置startAngle值     *
        /// </summary>
        /// <param name="startAngle"> </param>
        public virtual Pie startAngle(int? startAngle)
        {
            startAngle_Renamed = startAngle;
            return this;
        }

        /// <summary>
        ///     获取minAngle值
        /// </summary>
        public virtual int? minAngle()
        {
            return minAngle_Renamed;
        }

        /// <summary>
        ///     设置minAngle值     *
        /// </summary>
        /// <param name="minAngle"> </param>
        public virtual Pie minAngle(int? minAngle)
        {
            minAngle_Renamed = minAngle;
            return this;
        }

        /// <summary>
        ///     获取clockWise值
        /// </summary>
        public virtual bool? clockWise()
        {
            return clockWise_Renamed;
        }

        /// <summary>
        ///     设置clockWise值     *
        /// </summary>
        /// <param name="clockWise"> </param>
        public virtual Pie clockWise(bool? clockWise)
        {
            clockWise_Renamed = clockWise;
            return this;
        }

        /// <summary>
        ///     获取roseType值
        /// </summary>
        public virtual RoseType roseType()
        {
            return roseType_Renamed;
        }

        /// <summary>
        ///     设置roseType值     *
        /// </summary>
        /// <param name="roseType"> </param>
        public virtual Pie roseType(RoseType roseType)
        {
            roseType_Renamed = roseType;
            return this;
        }

        /// <summary>
        ///     获取selectedOffset值
        /// </summary>
        public virtual int? selectedOffset()
        {
            return selectedOffset_Renamed;
        }

        /// <summary>
        ///     设置selectedOffset值     *
        /// </summary>
        /// <param name="selectedOffset"> </param>
        public virtual Pie selectedOffset(int? selectedOffset)
        {
            selectedOffset_Renamed = selectedOffset;
            return this;
        }

        /// <summary>
        ///     获取selectedMode值
        /// </summary>
        public virtual SelectedMode selectedMode()
        {
            return selectedMode_Renamed;
        }

        /// <summary>
        ///     设置selectedMode值     *
        /// </summary>
        /// <param name="selectedMode"> </param>
        public virtual Pie selectedMode(SelectedMode selectedMode)
        {
            selectedMode_Renamed = selectedMode;
            return this;
        }
    }
}