/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts.series
{
    /// <summary>
    ///     Description: Series
    ///     @author Yongjin.C
    /// </summary>
    public abstract class Series<T> : AbstractData<T>, Chart where T : class
    {
        /// <summary>
        ///     图形样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.ItemStyle
        /// </seealso>
        private ItemStyle itemStyle_Renamed;

        /// <summary>
        ///     是否启用图例（legend）hover时的联动响应（高亮显示）
        /// </summary>
        private bool? legendHoverLink_Renamed;

        /// <summary>
        ///     标线
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.MarkLine
        /// </seealso>
        private MarkLine markLine_Renamed;

        /// <summary>
        ///     标注
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.MarkPoint
        /// </seealso>
        private MarkPoint markPoint_Renamed;

        /// <summary>
        ///     系列名称，如启用legend，该值将被legend.data索引相关
        /// </summary>
        private string name_Renamed;

        /// <summary>
        ///     标志图形默认只有主轴显示（随主轴标签间隔隐藏策略），如需全部显示可把showAllSymbol设为true
        /// </summary>
        private bool? showAllSymbol_Renamed;

        /// <summary>
        ///     组合名称，多组数据的堆积图时使用，eg：stack:'group1'，则series数组中stack值等亿group1'的数据做堆积计算
        /// </summary>
        private string stack_Renamed;

        /// <summary>
        ///     标志图形旋转角度[-180,180]
        /// </summary>
        private object symbolRoate_Renamed;

        /// <summary>
        ///     标志图形大小，可计算特性启用情况建议增大以提高交互体验。实现气泡图时symbolSize需为Function，气泡大小取决于该方法返回值，传入参数为当前数据项（value数组＿
        /// </summary>
        private object symbolSize_Renamed;

        /// <summary>
        ///     标志图形类型，默认自动选择＿种类型循环使用，不显示标志图形可设为'none'＿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Symbol
        /// </seealso>
        private object symbol_Renamed;

        /// <summary>
        ///     提示框样式，仅对本系列有效，如不设则用option.tooltip（详见tooltip＿鼠标悬浮交互时的信息提示
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.Tooltip
        /// </seealso>
        private Tooltip tooltip_Renamed;

        /// <summary>
        ///     图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.SeriesType
        /// </seealso>
        private SeriesType type_Renamed;

        /// <summary>
        ///     xAxis坐标轴数组的索引，指定该系列数据所用的横坐标轴
        /// </summary>
        private int? xAxisIndex_Renamed;

        /// <summary>
        ///     yAxis坐标轴数组的索引，指定该系列数据所用的纵坐标轴
        /// </summary>
        private int? yAxisIndex_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        protected internal Series()
        {
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        protected internal Series(string name)
        {
            name_Renamed = name;
        }

        /// <summary>
        ///     获取legendHoverLink值     *
        ///     @return
        /// </summary>
        public virtual bool? LegendHoverLink
        {
            get { return legendHoverLink_Renamed; }
            set { legendHoverLink_Renamed = value; }
        }


        /// <summary>
        ///     获取tooltip值
        /// </summary>
        public virtual Tooltip Tooltip
        {
            get { return tooltip_Renamed; }
            set { tooltip_Renamed = value; }
        }


        /// <summary>
        ///     获取itemStyle值
        /// </summary>
        public virtual ItemStyle ItemStyle
        {
            get { return itemStyle_Renamed; }
            set { itemStyle_Renamed = value; }
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string Name
        {
            get { return name_Renamed; }
            set { name_Renamed = value; }
        }


        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual SeriesType Type
        {
            get { return type_Renamed; }
            set { type_Renamed = value; }
        }


        /// <summary>
        ///     获取stack值
        /// </summary>
        public virtual string Stack
        {
            get { return stack_Renamed; }
            set { stack_Renamed = value; }
        }


        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object Symbol
        {
            get { return symbol_Renamed; }
            set { symbol_Renamed = value; }
        }


        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object SymbolSize
        {
            get { return symbolSize_Renamed; }
            set { symbolSize_Renamed = value; }
        }


        /// <summary>
        ///     获取symbolRoate值
        /// </summary>
        public virtual object SymbolRoate
        {
            get { return symbolRoate_Renamed; }
            set { symbolRoate_Renamed = value; }
        }


        /// <summary>
        ///     获取showAllSymbol值
        /// </summary>
        public virtual bool? ShowAllSymbol
        {
            get { return showAllSymbol_Renamed; }
            set { showAllSymbol_Renamed = value; }
        }

        /// <summary>
        ///     设置tooltip值     *
        /// </summary>
        /// <param name="tooltip"> </param>
        public virtual Series<T> tooltip(Tooltip tooltip)
        {
            tooltip_Renamed = tooltip;
            return this;
        }

        /// <summary>
        ///     设置itemStyle值     *
        /// </summary>
        /// <param name="itemStyle"> </param>
        public virtual Series<T> itemStyle(ItemStyle itemStyle)
        {
            itemStyle_Renamed = itemStyle;
            return this;
        }

        /// <summary>
        ///     设置markPoint值     *
        /// </summary>
        /// <param name="markPoint"> </param>
        public virtual Series<T> markPoint(MarkPoint markPoint)
        {
            markPoint_Renamed = markPoint;
            return this;
        }

        /// <summary>
        ///     设置markLine值     *
        /// </summary>
        /// <param name="markLine"> </param>
        public virtual Series<T> markLine(MarkLine markLine)
        {
            markLine_Renamed = markLine;
            return this;
        }

        /// <summary>
        ///     获取legendHoverLink值     *
        ///     @return
        /// </summary>
        public virtual bool? legendHoverLink()
        {
            return legendHoverLink_Renamed;
        }

        /// <summary>
        ///     设置legendHoverLink值     *
        /// </summary>
        /// <param name="legendHoverLink"> </param>
        public virtual T legendHoverLink(bool? legendHoverLink)
        {
            legendHoverLink_Renamed = legendHoverLink;
            return this as T;
        }

        /// <summary>
        ///     获取xAxisIndex值
        /// </summary>
        public virtual int? xAxisIndex()
        {
            return xAxisIndex_Renamed;
        }

        /// <summary>
        ///     设置xAxisIndex值     *
        /// </summary>
        /// <param name="xAxisIndex"> </param>
        public virtual T xAxisIndex(int? xAxisIndex)
        {
            xAxisIndex_Renamed = xAxisIndex;
            return this as T;
        }

        /// <summary>
        ///     获取yAxisIndex值
        /// </summary>
        public virtual int? yAxisIndex()
        {
            return yAxisIndex_Renamed;
        }

        /// <summary>
        ///     设置yAxisIndex值     *
        /// </summary>
        /// <param name="yAxisIndex"> </param>
        public virtual T yAxisIndex(int? yAxisIndex)
        {
            yAxisIndex_Renamed = yAxisIndex;
            return this as T;
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string name()
        {
            return name_Renamed;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual T name(string name)
        {
            name_Renamed = name;
            return this as T;
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual SeriesType type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual T type(SeriesType type)
        {
            type_Renamed = type;
            return this as T;
        }

        /// <summary>
        ///     获取stack值
        /// </summary>
        public virtual string stack()
        {
            return stack_Renamed;
        }

        /// <summary>
        ///     设置stack值     *
        /// </summary>
        /// <param name="stack"> </param>
        public virtual T stack(string stack)
        {
            stack_Renamed = stack;
            return this as T;
        }

        /// <summary>
        ///     提示框样式，仅对本系列有效，如不设则用option.tooltip（详见tooltip＿鼠标悬浮交互时的信息提示
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.Tooltip
        /// </seealso>
        public virtual Tooltip tooltip()
        {
            if (tooltip_Renamed == null)
            {
                tooltip_Renamed = new Tooltip();
            }
            return tooltip_Renamed;
        }

        /// <summary>
        ///     图形样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.ItemStyle
        /// </seealso>
        public virtual ItemStyle itemStyle()
        {
            if (itemStyle_Renamed == null)
            {
                itemStyle_Renamed = new ItemStyle();
            }
            return itemStyle_Renamed;
        }

        /// <summary>
        ///     标注
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.MarkPoint
        /// </seealso>
        public virtual MarkPoint markPoint()
        {
            if (markPoint_Renamed == null)
            {
                markPoint_Renamed = new MarkPoint();
            }
            return markPoint_Renamed;
        }

        /// <summary>
        ///     标线
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.series.MarkLine
        /// </seealso>
        public virtual MarkLine markLine()
        {
            if (markLine_Renamed == null)
            {
                markLine_Renamed = new MarkLine();
            }
            return markLine_Renamed;
        }

        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object symbol()
        {
            return symbol_Renamed;
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual T symbol(object symbol)
        {
            symbol_Renamed = symbol;
            return this as T;
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual T symbol(Symbol symbol)
        {
            symbol_Renamed = symbol;
            return this as T;
        }

        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object symbolSize()
        {
            return symbolSize_Renamed;
        }

        /// <summary>
        ///     设置symbolSize值     *
        /// </summary>
        /// <param name="symbolSize"> </param>
        public virtual T symbolSize(object symbolSize)
        {
            symbolSize_Renamed = symbolSize;
            return this as T;
        }

        /// <summary>
        ///     获取symbolRoate值
        /// </summary>
        public virtual object symbolRoate()
        {
            return symbolRoate_Renamed;
        }

        /// <summary>
        ///     设置symbolRoate值     *
        /// </summary>
        /// <param name="symbolRoate"> </param>
        public virtual T symbolRoate(object symbolRoate)
        {
            symbolRoate_Renamed = symbolRoate;
            return this as T;
        }

        /// <summary>
        ///     获取showAllSymbol值
        /// </summary>
        public virtual bool? showAllSymbol()
        {
            return showAllSymbol_Renamed;
        }

        /// <summary>
        ///     设置showAllSymbol值     *
        /// </summary>
        /// <param name="showAllSymbol"> </param>
        public virtual T showAllSymbol(bool? showAllSymbol)
        {
            showAllSymbol_Renamed = showAllSymbol;
            return this as T;
        }


        /// <summary>
        ///     获取markPoint值
        /// </summary>
        public virtual MarkPoint getMarkPoint()
        {
            return markPoint_Renamed;
        }

        /// <summary>
        ///     设置markPoint值     *
        /// </summary>
        /// <param name="markPoint"> </param>
        public virtual void setMarkPoint(MarkPoint markPoint)
        {
            markPoint_Renamed = markPoint;
        }

        /// <summary>
        ///     获取markLine值
        /// </summary>
        public virtual MarkLine getMarkLine()
        {
            return markLine_Renamed;
        }

        /// <summary>
        ///     设置markLine值     *
        /// </summary>
        /// <param name="markLine"> </param>
        public virtual void setMarkLine(MarkLine markLine)
        {
            markLine_Renamed = markLine;
        }

        /// <summary>
        ///     获取xAxisIndex值
        /// </summary>
        public virtual int? getxAxisIndex()
        {
            return xAxisIndex_Renamed;
        }

        /// <summary>
        ///     设置xAxisIndex值     *
        /// </summary>
        /// <param name="xAxisIndex"> </param>
        public virtual void setxAxisIndex(int? xAxisIndex)
        {
            xAxisIndex_Renamed = xAxisIndex;
        }

        /// <summary>
        ///     获取yAxisIndex值
        /// </summary>
        public virtual int? getyAxisIndex()
        {
            return yAxisIndex_Renamed;
        }

        /// <summary>
        ///     设置yAxisIndex值     *
        /// </summary>
        /// <param name="yAxisIndex"> </param>
        public virtual void setyAxisIndex(int? yAxisIndex)
        {
            yAxisIndex_Renamed = yAxisIndex;
        }
    }
}