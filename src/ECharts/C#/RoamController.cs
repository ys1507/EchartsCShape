using System.Collections.Generic;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     缩放漫游组件，仅对地图有敿 *
    ///     @author Yongjin.C
    /// </summary>
    public class RoamController : Basic<RoamController>, Component
    {
        private string fillerColor_Renamed;
        private string handleColor_Renamed;
        private int? height_Renamed;
        private IDictionary<string, bool?> mapTypeControl_Renamed;
        private int? step_Renamed;
        private int? width_Renamed;

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual int? Width
        {
            get { return width_Renamed; }
            set { width_Renamed = value; }
        }


        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual int? Height
        {
            get { return height_Renamed; }
            set { height_Renamed = value; }
        }


        /// <summary>
        ///     获取fillerColor值
        /// </summary>
        public virtual string FillerColor
        {
            get { return fillerColor_Renamed; }
            set { fillerColor_Renamed = value; }
        }


        /// <summary>
        ///     获取handleColor值
        /// </summary>
        public virtual string HandleColor
        {
            get { return handleColor_Renamed; }
            set { handleColor_Renamed = value; }
        }


        /// <summary>
        ///     获取step值
        /// </summary>
        public virtual int? Step
        {
            get { return step_Renamed; }
            set { step_Renamed = value; }
        }


        /// <summary>
        ///     获取mapTypeControl值
        /// </summary>
        public virtual IDictionary<string, bool?> MapTypeControl
        {
            get { return mapTypeControl_Renamed; }
            set { mapTypeControl_Renamed = value; }
        }

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual int? width()
        {
            return width_Renamed;
        }

        /// <summary>
        ///     设置width值
        /// </summary>
        /// <param name="width"> </param>
        public virtual RoamController width(int? width)
        {
            width_Renamed = width;
            return this;
        }

        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual int? height()
        {
            return height_Renamed;
        }

        /// <summary>
        ///     设置height值
        /// </summary>
        /// <param name="height"> </param>
        public virtual RoamController height(int? height)
        {
            height_Renamed = height;
            return this;
        }

        /// <summary>
        ///     获取fillerColor值
        /// </summary>
        public virtual string fillerColor()
        {
            return fillerColor_Renamed;
        }

        /// <summary>
        ///     设置fillerColor值
        /// </summary>
        /// <param name="fillerColor"> </param>
        public virtual RoamController fillerColor(string fillerColor)
        {
            fillerColor_Renamed = fillerColor;
            return this;
        }

        /// <summary>
        ///     获取handleColor值
        /// </summary>
        public virtual string handleColor()
        {
            return handleColor_Renamed;
        }

        /// <summary>
        ///     设置handleColor值
        /// </summary>
        /// <param name="handleColor"> </param>
        public virtual RoamController handleColor(string handleColor)
        {
            handleColor_Renamed = handleColor;
            return this;
        }

        /// <summary>
        ///     获取step值
        /// </summary>
        public virtual int? step()
        {
            return step_Renamed;
        }

        /// <summary>
        ///     设置step值
        /// </summary>
        /// <param name="step"> </param>
        public virtual RoamController step(int? step)
        {
            step_Renamed = step;
            return this;
        }

        /// <summary>
        ///     获取mapTypeControl值
        /// </summary>
        public virtual IDictionary<string, bool?> mapTypeControl()
        {
            return mapTypeControl_Renamed;
        }

        /// <summary>
        ///     设置mapTypeControl值     *
        /// </summary>
        /// <param name="key"> 地名 </param>
        /// <param name="value"> true|false </param>
        public virtual RoamController mapTypeControl(string key, bool? value)
        {
            if (mapTypeControl_Renamed == null)
            {
                mapTypeControl_Renamed = new Dictionary<string, bool?>();
            }
            mapTypeControl_Renamed[key] = value;
            return this;
        }
    }
}