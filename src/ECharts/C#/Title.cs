/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    public class Title : Basic<Title>, Component
    {
        /// <summary>
        ///     主标题文本超链接
        /// </summary>
        private string link_Renamed;

        /// <summary>
        ///     属性offsetCenter用于详情定位，数组为横纵相对仪表盘圆心坐标偏移，支持百分比（相对外半径）
        /// </summary>
        private object offsetCenter_Renamed;

        /// <summary>
        ///     副标题文本超链接
        /// </summary>
        private string sublink_Renamed;

        /// <summary>
        ///     指定窗口打开副标题超链接，支挿self' | 'blank'，不指定等同丿blank'（新窗口＿
        /// </summary>
        private string subtarget_Renamed;

        /// <summary>
        ///     默认值{color: '#aaa'}，副标题文本样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        private TextStyle subtextStyle_Renamed;

        /// <summary>
        ///     副标题文本，'\n'指定换行
        /// </summary>
        private string subtext_Renamed;

        /// <summary>
        ///     指定窗口打开主标题超链接，支挿self' | 'blank'，不指定等同丿blank'（新窗口＿
        /// </summary>
        private string target_Renamed;

        /// <summary>
        ///     水平对齐方式，默认根据x设置自动调整，可选为＿left' | 'right' | 'center
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.X
        /// </seealso>
        private X textAlign_Renamed;

        /// <summary>
        ///     主标题文本样式（详见textStyle＿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        private TextStyle textStyle_Renamed;

        /// <summary>
        ///     主标题文本，'\n'指定换行
        /// </summary>
        private string text_Renamed;

        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取subtextStyle值
        /// </summary>
        public virtual TextStyle SubtextStyle
        {
            get { return subtextStyle_Renamed; }
            set { subtextStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取text值
        /// </summary>
        public virtual string Text
        {
            get { return text_Renamed; }
            set { text_Renamed = value; }
        }


        /// <summary>
        ///     获取link值
        /// </summary>
        public virtual string Link
        {
            get { return link_Renamed; }
            set { link_Renamed = value; }
        }


        /// <summary>
        ///     获取target值
        /// </summary>
        public virtual string Target
        {
            get { return target_Renamed; }
            set { target_Renamed = value; }
        }


        /// <summary>
        ///     获取subtext值
        /// </summary>
        public virtual string Subtext
        {
            get { return subtext_Renamed; }
            set { subtext_Renamed = value; }
        }


        /// <summary>
        ///     获取sublink值
        /// </summary>
        public virtual string Sublink
        {
            get { return sublink_Renamed; }
            set { sublink_Renamed = value; }
        }


        /// <summary>
        ///     获取subtarget值
        /// </summary>
        public virtual string Subtarget
        {
            get { return subtarget_Renamed; }
            set { subtarget_Renamed = value; }
        }


        /// <summary>
        ///     获取textAlign值
        /// </summary>
        public virtual X TextAlign
        {
            get { return textAlign_Renamed; }
            set { textAlign_Renamed = value; }
        }


        /// <summary>
        ///     获取offsetCenter值
        /// </summary>
        public virtual object OffsetCenter
        {
            get { return offsetCenter_Renamed; }
            set { offsetCenter_Renamed = value; }
        }

        /// <summary>
        ///     设置textStyle值     *
        /// </summary>
        /// <param name="textStyle"> </param>
        public virtual Title textStyle(TextStyle textStyle)
        {
            textStyle_Renamed = textStyle;
            return this;
        }

        /// <summary>
        ///     设置subtextStyle值     *
        /// </summary>
        /// <param name="subtextStyle"> </param>
        public virtual Title subtextStyle(TextStyle subtextStyle)
        {
            subtextStyle_Renamed = subtextStyle;
            return this;
        }

        /// <summary>
        ///     获取text值
        /// </summary>
        public virtual string text()
        {
            return text_Renamed;
        }

        /// <summary>
        ///     设置text值     *
        /// </summary>
        /// <param name="text"> </param>
        public virtual Title text(string text)
        {
            text_Renamed = text;
            return this;
        }

        /// <summary>
        ///     获取link值
        /// </summary>
        public virtual string link()
        {
            return link_Renamed;
        }

        /// <summary>
        ///     设置link值     *
        /// </summary>
        /// <param name="link"> </param>
        public virtual Title link(string link)
        {
            link_Renamed = link;
            return this;
        }

        /// <summary>
        ///     获取target值
        /// </summary>
        public virtual string target()
        {
            return target_Renamed;
        }

        /// <summary>
        ///     设置target值     *
        /// </summary>
        /// <param name="target"> </param>
        public virtual Title target(string target)
        {
            target_Renamed = target;
            return this;
        }

        /// <summary>
        ///     获取subtext值
        /// </summary>
        public virtual string subtext()
        {
            return subtext_Renamed;
        }

        /// <summary>
        ///     设置subtext值     *
        /// </summary>
        /// <param name="subtext"> </param>
        public virtual Title subtext(string subtext)
        {
            subtext_Renamed = subtext;
            return this;
        }

        /// <summary>
        ///     获取sublink值
        /// </summary>
        public virtual string sublink()
        {
            return sublink_Renamed;
        }

        /// <summary>
        ///     设置sublink值     *
        /// </summary>
        /// <param name="sublink"> </param>
        public virtual Title sublink(string sublink)
        {
            sublink_Renamed = sublink;
            return this;
        }

        /// <summary>
        ///     获取subtarget值
        /// </summary>
        public virtual string subtarget()
        {
            return subtarget_Renamed;
        }

        /// <summary>
        ///     设置subtarget值     *
        /// </summary>
        /// <param name="subtarget"> </param>
        public virtual Title subtarget(string subtarget)
        {
            subtarget_Renamed = subtarget;
            return this;
        }

        /// <summary>
        ///     获取textAlign值
        /// </summary>
        public virtual X textAlign()
        {
            return textAlign_Renamed;
        }

        /// <summary>
        ///     设置textAlign值     *
        /// </summary>
        /// <param name="textAlign"> </param>
        public virtual Title textAlign(X textAlign)
        {
            textAlign_Renamed = textAlign;
            return this;
        }

        /// <summary>
        ///     主标题文本样式（详见textStyle＿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        public virtual TextStyle textStyle()
        {
            if (textStyle_Renamed == null)
            {
                textStyle_Renamed = new TextStyle();
            }
            return textStyle_Renamed;
        }

        /// <summary>
        ///     默认值{color: '#aaa'}，副标题文本样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        public virtual TextStyle subtextStyle()
        {
            if (subtextStyle_Renamed == null)
            {
                subtextStyle_Renamed = new TextStyle();
            }
            return subtextStyle_Renamed;
        }

        /// <summary>
        ///     获取offsetCenter值
        /// </summary>
        public virtual object offsetCenter()
        {
            return offsetCenter_Renamed;
        }

        /// <summary>
        ///     设置offsetCenter值     *
        /// </summary>
        /// <param name="offsetCenter"> </param>
        public virtual Title offsetCenter(object offsetCenter)
        {
            offsetCenter_Renamed = offsetCenter;
            return this;
        }
    }
}