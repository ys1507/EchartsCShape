using System;
using System.Collections.Generic;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.feature;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    public class Toolbox : Basic<Toolbox>, Component
    {
        /// <summary>
        ///     工具箱背景颜色，默认透明
        /// </summary>
        private IList<object> color_Renamed;

        /// <summary>
        ///     无效颜色
        /// </summary>
        private string disableColor_Renamed;

        /// <summary>
        ///     激活颜艿
        /// </summary>
        private string effectiveColor_Renamed;

        /// <summary>
        ///     启用功能，目前支持feature见下，工具箱自定义功能回调处理
        /// </summary>
        private IDictionary<string, Feature> feature_Renamed;

        /// <summary>
        ///     工具箱icon大小，单位（px＿
        /// </summary>
        private int? itemSize_Renamed;

        /// <summary>
        ///     布局方式，默认为水平布局，可选为＿horizontal' | 'vertical'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Orient
        /// </seealso>
        private Orient orient_Renamed;

        /// <summary>
        ///     是否显示工具箱文字提示，默认启用
        /// </summary>
        private bool? showTitle_Renamed;

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual IList<object> Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }


        /// <summary>
        ///     获取feature值
        /// </summary>
        public virtual IDictionary<string, Feature> Feature
        {
            get { return feature_Renamed; }
            set { feature_Renamed = value; }
        }


        /// <summary>
        ///     获取orient值
        /// </summary>
        public virtual Orient Orient
        {
            get { return orient_Renamed; }
            set { orient_Renamed = value; }
        }


        /// <summary>
        ///     获取disableColor值
        /// </summary>
        public virtual string DisableColor
        {
            get { return disableColor_Renamed; }
            set { disableColor_Renamed = value; }
        }


        /// <summary>
        ///     获取effectiveColor值
        /// </summary>
        public virtual string EffectiveColor
        {
            get { return effectiveColor_Renamed; }
            set { effectiveColor_Renamed = value; }
        }


        /// <summary>
        ///     获取itemSize值
        /// </summary>
        public virtual int? ItemSize
        {
            get { return itemSize_Renamed; }
            set { itemSize_Renamed = value; }
        }


        /// <summary>
        ///     获取showTitle值
        /// </summary>
        public virtual bool? ShowTitle
        {
            get { return showTitle_Renamed; }
            set { showTitle_Renamed = value; }
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual Toolbox color(IList<object> color)
        {
            color_Renamed = color;
            return this;
        }

        /// <summary>
        ///     获取orient值
        /// </summary>
        public virtual Orient orient()
        {
            return orient_Renamed;
        }

        /// <summary>
        ///     设置orient值     *
        /// </summary>
        /// <param name="orient"> </param>
        public virtual Toolbox orient(Orient orient)
        {
            orient_Renamed = orient;
            return this;
        }

        /// <summary>
        ///     工具箱背景颜色，默认透明
        /// </summary>
        public virtual IList<object> color()
        {
            if (color_Renamed == null)
            {
                color_Renamed = new List<object>();
            }
            return color_Renamed;
        }

        /// <summary>
        ///     获取disableColor值
        /// </summary>
        public virtual string disableColor()
        {
            return disableColor_Renamed;
        }

        /// <summary>
        ///     设置disableColor值     *
        /// </summary>
        /// <param name="disableColor"> </param>
        public virtual Toolbox disableColor(string disableColor)
        {
            disableColor_Renamed = disableColor;
            return this;
        }

        /// <summary>
        ///     获取effectiveColor值
        /// </summary>
        public virtual string effectiveColor()
        {
            return effectiveColor_Renamed;
        }

        /// <summary>
        ///     设置effectiveColor值     *
        /// </summary>
        /// <param name="effectiveColor"> </param>
        public virtual Toolbox effectiveColor(string effectiveColor)
        {
            effectiveColor_Renamed = effectiveColor;
            return this;
        }

        /// <summary>
        ///     获取itemSize值
        /// </summary>
        public virtual int? itemSize()
        {
            return itemSize_Renamed;
        }

        /// <summary>
        ///     设置itemSize值     *
        /// </summary>
        /// <param name="itemSize"> </param>
        public virtual Toolbox itemSize(int? itemSize)
        {
            itemSize_Renamed = itemSize;
            return this;
        }

        /// <summary>
        ///     获取showTitle值
        /// </summary>
        public virtual bool? showTitle()
        {
            return showTitle_Renamed;
        }

        /// <summary>
        ///     设置showTitle值     *
        /// </summary>
        /// <param name="showTitle"> </param>
        public virtual Toolbox showTitle(bool? showTitle)
        {
            showTitle_Renamed = showTitle;
            return this;
        }

        /// <summary>
        ///     启用功能，目前支持feature见下，工具箱自定义功能回调处理
        /// </summary>
        public virtual IDictionary<string, Feature> feature()
        {
            if (feature_Renamed == null)
            {
                feature_Renamed = new Dictionary<string, Feature>();
            }
            return feature_Renamed;
        }

        /// <summary>
        ///     添加组件
        /// </summary>
        /// <param name="value">
        ///     @return
        /// </param>
        private Toolbox _addFeature(Feature value)
        {
            if (value == null)
            {
                return this;
            }
            //第一个字母转小写
            string name = value.GetType().Name;
            name = name.Substring(0, 1).ToLower() + name.Substring(1);
            _addFeatureOnce(name, value);
            return this;
        }

        /// <summary>
        ///     添加组件
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public virtual Toolbox feature(params object[] values)
        {
            if (values == null && values.Length == 0)
            {
                return this;
            }
            if (feature_Renamed == null)
            {
                feature_Renamed = new Dictionary<string, Feature>();
            }
            foreach (object t in values)
            {
                if (t is Feature)
                {
                    _addFeature((Feature) t);
                }
                else if (t is Tool)
                {
                    switch ((Tool) t)
                    {
                        case Tool.dataView:
                            _addFeatureOnce(t, echarts.feature.Feature.dataView);
                            break;
                        case Tool.dataZoom:
                            _addFeatureOnce(t, echarts.feature.Feature.dataZoom);
                            break;
                        case Tool.magicType:
                            _addFeatureOnce(t, echarts.feature.Feature.magicType);
                            break;
                        case Tool.mark:
                            _addFeatureOnce(t, echarts.feature.Feature.mark);
                            break;
                        case Tool.restore:
                            _addFeatureOnce(t, echarts.feature.Feature.restore);
                            break;
                        case Tool.saveAsImage:
                            _addFeatureOnce(t, echarts.feature.Feature.saveAsImage);
                            break;
                        default:
                            //ignore
                            break;
                    }
                }
            }
            return this;
        }

        /// <summary>
        ///     同一种组件只添加一处     *
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="feature"> </param>
        private void _addFeatureOnce(object name, Feature feature)
        {
            string _name = Convert.ToString(name);
            if (!this.feature().ContainsKey(_name))
            {
                this.feature()[_name] = feature;
            }
        }
    }
}