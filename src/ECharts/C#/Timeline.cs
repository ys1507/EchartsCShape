/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     Description: Timeline
    ///     @author Yongjin.C
    /// </summary>
    public class Timeline : AbstractData<Timeline>, Component
    {
        /// <summary>
        ///     默认值false,是否自动播放
        /// </summary>
        private bool? autoPlay_Renamed;

        /// <summary>
        ///     背景颜色，默认透明(rgba(0,0,0,0))?
        /// </summary>
        private string backgroundColor_Renamed;

        /// <summary>
        ///     默认值ccc，边框颜艿
        /// </summary>
        private string borderColor_Renamed;

        /// <summary>
        ///     默认值，边框线宿
        /// </summary>
        private int? borderWidth_Renamed;

        /// <summary>
        ///     时间轴当前点
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.CheckpointStyle
        /// </seealso>
        private CheckpointStyle checkpointStyle_Renamed;

        /// <summary>
        ///     默认值left,播放控制器位置，可选为＿left' | 'right' | 'none'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.ControlPosition
        /// </seealso>
        private ControlPosition controlPosition_Renamed;

        /// <summary>
        ///     时间轴控制器样式，可指定正常和高亮颜艿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.ControlPosition
        /// </seealso>
        private ControlStyle controlStyle_Renamed;

        /// <summary>
        ///     默认值，当前索引位置，对应options数组，用于指定显示特定系刿
        /// </summary>
        private int? currentIndex_Renamed;

        /// <summary>
        ///     默认值0，时间轴高度，数值单位px，支持百分比（字符串），妿50%'(显示区域一半的高度)
        /// </summary>
        private object height_Renamed;

        /// <seealso cref= Yongjin.Cshape.echarts.Label
        /// </seealso>
        private Label label_Renamed;

        /// <summary>
        ///     默认值{color: '#666', width: 1, type: 'dashed'}，时间轴轴线样式lineStyle控制线条样式＿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        private LineStyle lineStyle_Renamed;

        /// <summary>
        ///     默认值true,是否循环播放
        /// </summary>
        private bool? loop_Renamed;

        /// <summary>
        ///     默认值false,时间轴上多个option切换时是否进行merge操作，同setOption第二个参敿
        /// </summary>
        private bool? notMerge_Renamed;

        /// <summary>
        ///     默认值，内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距，同css，见下图
        /// </summary>
        private int? padding_Renamed;

        /// <summary>
        ///     默认值000，播放时间间隔，单位ms
        /// </summary>
        private int? playInterval_Renamed;

        /// <summary>
        ///     默认值true,拖拽或点击改变时间轴是否实时显示
        /// </summary>
        private bool? realtime_Renamed;

        /// <summary>
        ///     默认值true,显示策略，可选为：true（显示） | false（隐藏）
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     默认值，轴点symbol，同serie.symbolSize
        /// </summary>
        private object symbolSize_Renamed;

        /// <summary>
        ///     默认值emptyDiamond，轴点symbol，同serie.symbol
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Symbol
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.series.Series# symbol
        /// </seealso>
        private object symbol_Renamed;

        /// <summary>
        ///     默认为time,模式是时间类型，时间轴间隔根据时间跨度计算，可选为＿number'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.TimeLineType
        /// </seealso>
        private TimeLineType type_Renamed;

        /// <summary>
        ///     默认值auto
        ///     时间轴宽度，默认为总宽庿- x - x2，数值单位px，指定width后将忽略x2。见下图?     * 支持百分比（字符串），如'50%'(显示区域一半的宽度)
        /// </summary>
        private object width_Renamed;

        /// <summary>
        ///     默认值0，时间轴右下角横坐标，数值单位px，支持百分比（字符串），妿50%'(显示区域横向中心)
        /// </summary>
        private object x2_Renamed;

        /// <summary>
        ///     默认值0，时间轴左上角横坐标，数值单位px，支持百分比（字符串），妿50%'(显示区域横向中心)
        /// </summary>
        private object x_Renamed;

        /// <summary>
        ///     默认值null,时间轴右下角纵坐标，数值单位px，支持百分比（字符串），妿50%'(显示区域纵向中心)
        /// </summary>
        private object y2_Renamed;

        /// <summary>
        ///     默认值null,时间轴左上角纵坐标，数值单位px，支持百分比（字符串），
        ///     默认无，随y2定位，如'50%'(显示区域纵向中心)
        /// </summary>
        private object y_Renamed;

        /// <summary>
        ///     获取lineStyle值
        /// </summary>
        public virtual LineStyle LineStyle
        {
            get { return lineStyle_Renamed; }
            set { lineStyle_Renamed = value; }
        }

        /// <summary>
        ///     获取checkpointStyle值
        /// </summary>
        public virtual CheckpointStyle CheckpointStyle
        {
            get { return checkpointStyle_Renamed; }
            set { checkpointStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取controlStyle值
        /// </summary>
        public virtual ControlStyle ControlStyle
        {
            get { return controlStyle_Renamed; }
            set { controlStyle_Renamed = value; }
        }


        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }


        public virtual TimeLineType Type
        {
            get { return type_Renamed; }
            set { type_Renamed = value; }
        }


        public virtual bool? NotMerge
        {
            get { return notMerge_Renamed; }
            set { notMerge_Renamed = value; }
        }


        public virtual bool? Realtime
        {
            get { return realtime_Renamed; }
            set { realtime_Renamed = value; }
        }


        public virtual object X
        {
            get { return x_Renamed; }
            set { x_Renamed = value; }
        }


        public virtual object Y
        {
            get { return y_Renamed; }
            set { y_Renamed = value; }
        }


        public virtual object X2
        {
            get { return x2_Renamed; }
            set { x2_Renamed = value; }
        }


        public virtual object Y2
        {
            get { return y2_Renamed; }
            set { y2_Renamed = value; }
        }


        public virtual object Width
        {
            get { return width_Renamed; }
            set { width_Renamed = value; }
        }


        public virtual object Height
        {
            get { return height_Renamed; }
            set { height_Renamed = value; }
        }


        public virtual string BackgroundColor
        {
            get { return backgroundColor_Renamed; }
            set { backgroundColor_Renamed = value; }
        }


        public virtual string BorderColor
        {
            get { return borderColor_Renamed; }
            set { borderColor_Renamed = value; }
        }


        public virtual int? BorderWidth
        {
            get { return borderWidth_Renamed; }
            set { borderWidth_Renamed = value; }
        }


        public virtual int? Padding
        {
            get { return padding_Renamed; }
            set { padding_Renamed = value; }
        }


        public virtual ControlPosition ControlPosition
        {
            get { return controlPosition_Renamed; }
            set { controlPosition_Renamed = value; }
        }


        public virtual bool? AutoPlay
        {
            get { return autoPlay_Renamed; }
            set { autoPlay_Renamed = value; }
        }


        public virtual bool? Loop
        {
            get { return loop_Renamed; }
            set { loop_Renamed = value; }
        }


        public virtual int? PlayInterval
        {
            get { return playInterval_Renamed; }
            set { playInterval_Renamed = value; }
        }


        public virtual object Symbol
        {
            get { return symbol_Renamed; }
            set { symbol_Renamed = value; }
        }


        public virtual object SymbolSize
        {
            get { return symbolSize_Renamed; }
            set { symbolSize_Renamed = value; }
        }


        public virtual int? CurrentIndex
        {
            get { return currentIndex_Renamed; }
            set { currentIndex_Renamed = value; }
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual Timeline show(bool? show)
        {
            show_Renamed = show;
            return this;
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual TimeLineType type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual Timeline type(TimeLineType type)
        {
            type_Renamed = type;
            return this;
        }

        /// <summary>
        ///     获取notMerge值
        /// </summary>
        public virtual bool? notMerge()
        {
            return notMerge_Renamed;
        }

        /// <summary>
        ///     设置notMerge值     *
        /// </summary>
        /// <param name="notMerge"> </param>
        public virtual Timeline notMerge(bool? notMerge)
        {
            notMerge_Renamed = notMerge;
            return this;
        }

        /// <summary>
        ///     获取realtime值
        /// </summary>
        public virtual bool? realtime()
        {
            return realtime_Renamed;
        }

        /// <summary>
        ///     设置realtime值     *
        /// </summary>
        /// <param name="realtime"> </param>
        public virtual Timeline realtime(bool? realtime)
        {
            realtime_Renamed = realtime;
            return this;
        }

        /// <summary>
        ///     获取x值
        /// </summary>
        public virtual object x()
        {
            return x_Renamed;
        }

        /// <summary>
        ///     设置x值     *
        /// </summary>
        /// <param name="x"> </param>
        public virtual Timeline x(object x)
        {
            x_Renamed = x;
            return this;
        }

        /// <summary>
        ///     获取y值
        /// </summary>
        public virtual object y()
        {
            return y_Renamed;
        }

        /// <summary>
        ///     设置y值     *
        /// </summary>
        /// <param name="y"> </param>
        public virtual Timeline y(object y)
        {
            y_Renamed = y;
            return this;
        }

        /// <summary>
        ///     获取x2值
        /// </summary>
        public virtual object x2()
        {
            return x2_Renamed;
        }

        /// <summary>
        ///     设置x2值     *
        /// </summary>
        /// <param name="x2"> </param>
        public virtual Timeline x2(object x2)
        {
            x2_Renamed = x2;
            return this;
        }

        /// <summary>
        ///     获取y2值
        /// </summary>
        public virtual object y2()
        {
            return y2_Renamed;
        }

        /// <summary>
        ///     设置y2值     *
        /// </summary>
        /// <param name="y2"> </param>
        public virtual Timeline y2(object y2)
        {
            y2_Renamed = y2;
            return this;
        }

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object width()
        {
            return width_Renamed;
        }

        /// <summary>
        ///     设置width值     *
        /// </summary>
        /// <param name="width"> </param>
        public virtual Timeline width(object width)
        {
            width_Renamed = width;
            return this;
        }

        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual object height()
        {
            return height_Renamed;
        }

        /// <summary>
        ///     设置height值     *
        /// </summary>
        /// <param name="height"> </param>
        public virtual Timeline height(object height)
        {
            height_Renamed = height;
            return this;
        }

        /// <summary>
        ///     获取backgroundColor值
        /// </summary>
        public virtual string backgroundColor()
        {
            return backgroundColor_Renamed;
        }

        /// <summary>
        ///     设置backgroundColor值     *
        /// </summary>
        /// <param name="backgroundColor"> </param>
        public virtual Timeline backgroundColor(string backgroundColor)
        {
            backgroundColor_Renamed = backgroundColor;
            return this;
        }

        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string borderColor()
        {
            return borderColor_Renamed;
        }

        /// <summary>
        ///     设置borderColor值     *
        /// </summary>
        /// <param name="borderColor"> </param>
        public virtual Timeline borderColor(string borderColor)
        {
            borderColor_Renamed = borderColor;
            return this;
        }

        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? borderWidth()
        {
            return borderWidth_Renamed;
        }

        /// <summary>
        ///     设置borderWidth值     *
        /// </summary>
        /// <param name="borderWidth"> </param>
        public virtual Timeline borderWidth(int? borderWidth)
        {
            borderWidth_Renamed = borderWidth;
            return this;
        }

        /// <summary>
        ///     获取padding值
        /// </summary>
        public virtual int? padding()
        {
            return padding_Renamed;
        }

        /// <summary>
        ///     设置padding值     *
        /// </summary>
        /// <param name="padding"> </param>
        public virtual Timeline padding(int? padding)
        {
            padding_Renamed = padding;
            return this;
        }

        /// <summary>
        ///     获取controlPosition值
        /// </summary>
        public virtual ControlPosition controlPosition()
        {
            return controlPosition_Renamed;
        }

        /// <summary>
        ///     设置controlPosition值     *
        /// </summary>
        /// <param name="controlPosition"> </param>
        public virtual Timeline controlPosition(ControlPosition controlPosition)
        {
            controlPosition_Renamed = controlPosition;
            return this;
        }

        /// <summary>
        ///     获取autoPlay值
        /// </summary>
        public virtual bool? autoPlay()
        {
            return autoPlay_Renamed;
        }

        /// <summary>
        ///     设置autoPlay值     *
        /// </summary>
        /// <param name="autoPlay"> </param>
        public virtual Timeline autoPlay(bool? autoPlay)
        {
            autoPlay_Renamed = autoPlay;
            return this;
        }

        /// <summary>
        ///     获取loop值
        /// </summary>
        public virtual bool? loop()
        {
            return loop_Renamed;
        }

        /// <summary>
        ///     设置loop值     *
        /// </summary>
        /// <param name="loop"> </param>
        public virtual Timeline loop(bool? loop)
        {
            loop_Renamed = loop;
            return this;
        }

        /// <summary>
        ///     获取playInterval值
        /// </summary>
        public virtual int? playInterval()
        {
            return playInterval_Renamed;
        }

        /// <summary>
        ///     设置playInterval值     *
        /// </summary>
        /// <param name="playInterval"> </param>
        public virtual Timeline playInterval(int? playInterval)
        {
            playInterval_Renamed = playInterval;
            return this;
        }

        /// <summary>
        ///     默认值{color: '#666', width: 1, type: 'dashed'}，时间轴轴线样式lineStyle控制线条样式＿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        public virtual LineStyle lineStyle()
        {
            if (lineStyle_Renamed == null)
            {
                lineStyle_Renamed = new LineStyle();
            }
            return lineStyle_Renamed;
        }

        /// <seealso cref= Yongjin.Cshape.echarts.Label
        /// </seealso>
        public virtual Label label()
        {
            if (label_Renamed == null)
            {
                label_Renamed = new Label();
            }
            return label_Renamed;
        }

        /// <summary>
        ///     时间轴当前点
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.CheckpointStyle
        /// </seealso>
        public virtual CheckpointStyle checkpointStyle()
        {
            if (checkpointStyle_Renamed == null)
            {
                checkpointStyle_Renamed = new CheckpointStyle();
            }
            return checkpointStyle_Renamed;
        }

        /// <summary>
        ///     时间轴控制器样式，可指定正常和高亮颜艿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.ControlPosition
        /// </seealso>
        public virtual ControlStyle controlStyle()
        {
            if (controlStyle_Renamed == null)
            {
                controlStyle_Renamed = new ControlStyle();
            }
            return controlStyle_Renamed;
        }

        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object symbol()
        {
            return symbol_Renamed;
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual Timeline symbol(object symbol)
        {
            symbol_Renamed = symbol;
            return this;
        }

        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object symbolSize()
        {
            return symbolSize_Renamed;
        }

        /// <summary>
        ///     设置symbolSize值     *
        /// </summary>
        /// <param name="symbolSize"> </param>
        public virtual Timeline symbolSize(object symbolSize)
        {
            symbolSize_Renamed = symbolSize;
            return this;
        }

        /// <summary>
        ///     获取currentIndex值
        /// </summary>
        public virtual int? currentIndex()
        {
            return currentIndex_Renamed;
        }

        /// <summary>
        ///     设置currentIndex值     *
        /// </summary>
        /// <param name="currentIndex"> </param>
        public virtual Timeline currentIndex(int? currentIndex)
        {
            currentIndex_Renamed = currentIndex;
            return this;
        }


        /// <summary>
        ///     获取label值
        /// </summary>
        public virtual Label getLabel()
        {
            return label_Renamed;
        }

        /// <summary>
        ///     设置label值     *
        /// </summary>
        /// <param name="label"> </param>
        public virtual void setLabel(Label label)
        {
            label_Renamed = label;
        }
    }
}