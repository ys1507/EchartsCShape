/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts.data
{
    /// <summary>
    ///     Description: BasicData
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public abstract class BasicData<T> where T : class
    {
        private const long serialVersionUID = 1L;
        private ItemStyle itemStyle_Renamed;

        private string name_Renamed;
        private object symbolSize_Renamed;
        private object symbol_Renamed;

        /// <summary>
        ///     特殊样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        private TextStyle textStyle_Renamed;

        private string text_Renamed;
        private MarkType type_Renamed;
        private object value_Renamed;

        /// <summary>
        ///     在存在直角坐标系的图表如折线、柱形、K线、散点图丿     * 除了通过直接指定位置外，如果希望标注基于直角系的定位，可以通过xAxis，yAxis
        /// </summary>
        private int? xAxis_Renamed;

        /// <summary>
        ///     饼图、雷达图、力导、和弦图使用x,y
        /// </summary>
        private object x_Renamed;

        private int? yAxis_Renamed;
        private object y_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public BasicData()
        {
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        protected internal BasicData(string name)
        {
            name_Renamed = name;
        }

        /// <summary>
        ///     构造函敿参数:name,value
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="value"> </param>
        public BasicData(string name, object value)
        {
            name_Renamed = name;
            value_Renamed = value;
        }

        /// <summary>
        ///     构造函敿参数:name,symbol,symbolSize
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="symbol"> </param>
        /// <param name="symbolSize"> </param>
        public BasicData(string name, object symbol, object symbolSize)
        {
            name_Renamed = name;
            symbol_Renamed = symbol;
            symbolSize_Renamed = symbolSize;
        }

        /// <summary>
        ///     构造函敿参数:value,symbol
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="symbol"> </param>
        public BasicData(object value, object symbol)
        {
            value_Renamed = value;
            symbol_Renamed = symbol;
        }

        /// <summary>
        ///     构造函敿参数:value,symbol,symbolSize
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="symbol"> </param>
        /// <param name="symbolSize"> </param>
        public BasicData(object value, object symbol, object symbolSize)
        {
            value_Renamed = value;
            symbol_Renamed = symbol;
            symbolSize_Renamed = symbolSize;
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string Name
        {
            get { return name_Renamed; }
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual object Value
        {
            get { return value_Renamed; }
        }

        /// <summary>
        ///     获取x值
        /// </summary>
        public virtual object X
        {
            get { return x_Renamed; }
        }

        /// <summary>
        ///     获取y值
        /// </summary>
        public virtual object Y
        {
            get { return y_Renamed; }
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual MarkType Type
        {
            get { return type_Renamed; }
        }

        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object Symbol
        {
            get { return symbol_Renamed; }
        }

        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object SymbolSize
        {
            get { return symbolSize_Renamed; }
        }

        /// <summary>
        ///     获取itemStyle值
        /// </summary>
        public virtual ItemStyle ItemStyle
        {
            get { return itemStyle_Renamed; }
        }

        /// <summary>
        ///     获取text值
        /// </summary>
        public virtual string Text
        {
            get { return text_Renamed; }
            set { text_Renamed = value; }
        }


        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }

        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle textStyle()
        {
            if (textStyle_Renamed == null)
            {
                textStyle_Renamed = new TextStyle();
            }
            return textStyle_Renamed;
        }

        /// <summary>
        ///     设置textStyle值     *
        /// </summary>
        /// <param name="textStyle"> </param>
        public virtual T textStyle(TextStyle textStyle)
        {
            textStyle_Renamed = textStyle;
            return this as T;
        }

        /// <summary>
        ///     获取text值
        /// </summary>
        public virtual string text()
        {
            return text_Renamed;
        }

        /// <summary>
        ///     设置text值     *
        /// </summary>
        /// <param name="text"> </param>
        public virtual T text(string text)
        {
            text_Renamed = text;
            return this as T;
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string name()
        {
            return name_Renamed;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual T name(string name)
        {
            name_Renamed = name;
            return this as T;
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual object value()
        {
            return value_Renamed;
        }

        /// <summary>
        ///     设置value值     *
        /// </summary>
        /// <param name="value"> </param>
        public virtual T value(object value)
        {
            value_Renamed = value;
            return this as T;
        }

        /// <summary>
        ///     设置value值     *
        /// </summary>
        /// <param name="values"> </param>
        public virtual T value(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this as T;
            }
            if (value_Renamed == null)
            {
                value_Renamed = new List<object>(values.Length);
            }
            if (value_Renamed is List<T>)
            {
                ((List<T>) value_Renamed).AddRange((IEnumerable<T>) values.ToList());
            }
            return this as T;
        }

        /// <summary>
        ///     获取x值
        /// </summary>
        public virtual object x()
        {
            return x_Renamed;
        }

        /// <summary>
        ///     设置x值     *
        /// </summary>
        /// <param name="x"> </param>
        public virtual T x(object x)
        {
            x_Renamed = x;
            return this as T;
        }

        /// <summary>
        ///     获取y值
        /// </summary>
        public virtual object y()
        {
            return y_Renamed;
        }

        /// <summary>
        ///     设置y值     *
        /// </summary>
        /// <param name="y"> </param>
        public virtual T y(object y)
        {
            y_Renamed = y;
            return this as T;
        }

        /// <summary>
        ///     获取xAxis值
        /// </summary>
        public virtual int? xAxis()
        {
            return xAxis_Renamed;
        }

        /// <summary>
        ///     设置xAxis值     *
        /// </summary>
        /// <param name="xAxis"> </param>
        public virtual T xAxis(int? xAxis)
        {
            xAxis_Renamed = xAxis;
            return this as T;
        }

        /// <summary>
        ///     获取yAxis值
        /// </summary>
        public virtual int? yAxis()
        {
            return yAxis_Renamed;
        }

        /// <summary>
        ///     设置yAxis值     *
        /// </summary>
        /// <param name="yAxis"> </param>
        public virtual T yAxis(int? yAxis)
        {
            yAxis_Renamed = yAxis;
            return this as T;
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual MarkType type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual T type(MarkType type)
        {
            type_Renamed = type;
            return this as T;
        }

        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object symbol()
        {
            return symbol_Renamed;
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual T symbol(object symbol)
        {
            symbol_Renamed = symbol;
            return this as T;
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual T symbol(Symbol symbol)
        {
            symbol_Renamed = symbol;
            return this as T;
        }

        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object symbolSize()
        {
            return symbolSize_Renamed;
        }

        /// <summary>
        ///     设置symbolSize值     *
        /// </summary>
        /// <param name="symbolSize"> </param>
        public virtual T symbolSize(object symbolSize)
        {
            symbolSize_Renamed = symbolSize;
            return this as T;
        }

        /// <summary>
        ///     获取itemStyle值
        /// </summary>
        public virtual ItemStyle itemStyle()
        {
            if (itemStyle_Renamed == null)
            {
                itemStyle_Renamed = new ItemStyle();
            }
            return itemStyle_Renamed;
        }

        /// <summary>
        ///     设置itemStyle值     *
        /// </summary>
        /// <param name="itemStyle"> </param>
        public virtual T itemStyle(ItemStyle itemStyle)
        {
            itemStyle_Renamed = itemStyle;
            return this as T;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual T setName(string name)
        {
            name_Renamed = name;
            return this as T;
        }

        /// <summary>
        ///     设置value值     *
        /// </summary>
        /// <param name="value"> </param>
        public virtual T setValue(object value)
        {
            value_Renamed = value;
            return this as T;
        }

        /// <summary>
        ///     设置x值     *
        /// </summary>
        /// <param name="x"> </param>
        public virtual T setX(object x)
        {
            x_Renamed = x;
            return this as T;
        }

        /// <summary>
        ///     设置y值     *
        /// </summary>
        /// <param name="y"> </param>
        public virtual T setY(object y)
        {
            y_Renamed = y;
            return this as T;
        }

        /// <summary>
        ///     获取xAxis值
        /// </summary>
        public virtual int? getxAxis()
        {
            return xAxis_Renamed;
        }

        /// <summary>
        ///     设置xAxis值     *
        /// </summary>
        /// <param name="xAxis"> </param>
        public virtual T setxAxis(int? xAxis)
        {
            xAxis_Renamed = xAxis;
            return this as T;
        }

        /// <summary>
        ///     获取yAxis值
        /// </summary>
        public virtual int? getyAxis()
        {
            return yAxis_Renamed;
        }

        /// <summary>
        ///     设置yAxis值     *
        /// </summary>
        /// <param name="yAxis"> </param>
        public virtual T setyAxis(int? yAxis)
        {
            yAxis_Renamed = yAxis;
            return this as T;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual T setType(MarkType type)
        {
            type_Renamed = type;
            return this as T;
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual T setSymbol(object symbol)
        {
            symbol_Renamed = symbol;
            return this as T;
        }

        /// <summary>
        ///     设置symbolSize值     *
        /// </summary>
        /// <param name="symbolSize"> </param>
        public virtual T setSymbolSize(object symbolSize)
        {
            symbolSize_Renamed = symbolSize;
            return this as T;
        }

        /// <summary>
        ///     设置itemStyle值     *
        /// </summary>
        /// <param name="itemStyle"> </param>
        public virtual T setItemStyle(ItemStyle itemStyle)
        {
            itemStyle_Renamed = itemStyle;
            return this as T;
        }
    }
}