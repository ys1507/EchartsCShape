using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.data
{
    /// <summary>
    ///     KData
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class KData
    {
        private const long serialVersionUID = 1L;

        private double?[] value_Renamed;

        /// <summary>
        ///     开盘，收盘，最低，最髿     *
        /// </summary>
        /// <param name="open"> </param>
        /// <param name="close"> </param>
        /// <param name="min"> </param>
        /// <param name="max"> </param>
        public KData(double? open, double? close, double? min, double? max)
        {
            value_Renamed = new double?[4];
            value_Renamed[0] = open;
            value_Renamed[1] = close;
            value_Renamed[2] = min;
            value_Renamed[3] = max;
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual double?[] Value
        {
            get { return value_Renamed; }
            set { value_Renamed = value; }
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual double?[] value()
        {
            return value_Renamed;
        }

        /// <summary>
        ///     设置value值     *
        /// </summary>
        /// <param name="value"> </param>
        public virtual KData value(double?[] value)
        {
            value_Renamed = value;
            return this;
        }
    }
}