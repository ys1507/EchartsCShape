using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.data
{
    /// <summary>
    ///     自定义样式的数据 - 适用于axis.data
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class AxisData
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     特殊样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        private TextStyle textStyle_Renamed;

        /// <summary>
        ///     值
        /// </summary>
        private object value_Renamed;

        /// <summary>
        ///     构造函敿参数:value
        /// </summary>
        /// <param name="value"> </param>
        public AxisData(object value)
        {
            value_Renamed = value;
        }

        /// <summary>
        ///     构造函敿参数:value,textStyle
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="textStyle"> </param>
        public AxisData(object value, TextStyle textStyle)
        {
            value_Renamed = value;
            textStyle_Renamed = textStyle;
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual object Value
        {
            get { return value_Renamed; }
            set { value_Renamed = value; }
        }


        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual object value()
        {
            return value_Renamed;
        }

        /// <summary>
        ///     设置value值     *
        /// </summary>
        /// <param name="value"> </param>
        public virtual AxisData value(object value)
        {
            value_Renamed = value;
            return this;
        }

        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle textStyle()
        {
            if (textStyle_Renamed == null)
            {
                textStyle_Renamed = new TextStyle();
            }
            return textStyle_Renamed;
        }

        /// <summary>
        ///     设置textStyle值     *
        /// </summary>
        /// <param name="textStyle"> </param>
        public virtual AxisData textStyle(TextStyle textStyle)
        {
            textStyle_Renamed = textStyle;
            return this;
        }
    }
}