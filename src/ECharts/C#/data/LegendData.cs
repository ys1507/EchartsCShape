using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.data
{
    /// <summary>
    ///     Description : LegendData
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class LegendData
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     图标
        /// </summary>
        private string icon_Renamed;

        /// <summary>
        ///     名称
        /// </summary>
        private string name_Renamed;

        /// <summary>
        ///     文字样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.TextStyle
        /// </seealso>
        private TextStyle textStyle_Renamed;

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public LegendData(string name)
        {
            name_Renamed = name;
        }

        /// <summary>
        ///     构造函敿参数:name,textStyle
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="textStyle"> </param>
        public LegendData(string name, TextStyle textStyle)
        {
            name_Renamed = name;
            textStyle_Renamed = textStyle;
        }

        /// <summary>
        ///     构造函敿参数:name,textStyle,icon
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="textStyle"> </param>
        /// <param name="icon"> </param>
        public LegendData(string name, TextStyle textStyle, string icon)
        {
            name_Renamed = name;
            textStyle_Renamed = textStyle;
            icon_Renamed = icon;
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string Name
        {
            get { return name_Renamed; }
            set { name_Renamed = value; }
        }


        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle TextStyle
        {
            get { return textStyle_Renamed; }
            set { textStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取icon值
        /// </summary>
        public virtual string Icon
        {
            get { return icon_Renamed; }
            set { icon_Renamed = value; }
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string name()
        {
            return name_Renamed;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual LegendData name(string name)
        {
            name_Renamed = name;
            return this;
        }

        /// <summary>
        ///     获取textStyle值
        /// </summary>
        public virtual TextStyle textStyle()
        {
            if (textStyle_Renamed == null)
            {
                textStyle_Renamed = new TextStyle();
            }
            return textStyle_Renamed;
        }

        /// <summary>
        ///     设置textStyle值     *
        /// </summary>
        /// <param name="textStyle"> </param>
        public virtual LegendData textStyle(TextStyle textStyle)
        {
            textStyle_Renamed = textStyle;
            return this;
        }

        /// <summary>
        ///     获取icon值
        /// </summary>
        public virtual string icon()
        {
            return icon_Renamed;
        }

        /// <summary>
        ///     设置icon值     *
        /// </summary>
        /// <param name="icon"> </param>
        public virtual LegendData icon(string icon)
        {
            icon_Renamed = icon;
            return this;
        }
    }
}