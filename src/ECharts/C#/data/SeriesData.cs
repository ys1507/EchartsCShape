using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.data
{
    /// <summary>
    ///     Description: Series.Data
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class SeriesData
    {
        private const long serialVersionUID = 1L;

        private ItemStyle itemStyle_Renamed;
        private Tooltip tooltip_Renamed;
        private object value_Renamed;

        /// <summary>
        ///     构造函敿参数:value
        /// </summary>
        /// <param name="value"> </param>
        public SeriesData(object value)
        {
            value_Renamed = value;
        }

        /// <summary>
        ///     构造函敿参数:value,tooltip
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="tooltip"> </param>
        public SeriesData(object value, Tooltip tooltip)
        {
            value_Renamed = value;
            tooltip_Renamed = tooltip;
        }

        /// <summary>
        ///     构造函敿参数:value,itemStyle
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="itemStyle"> </param>
        public SeriesData(object value, ItemStyle itemStyle)
        {
            value_Renamed = value;
            itemStyle_Renamed = itemStyle;
        }

        /// <summary>
        ///     构造函敿参数:value,tooltip,itemStyle
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="tooltip"> </param>
        /// <param name="itemStyle"> </param>
        public SeriesData(object value, Tooltip tooltip, ItemStyle itemStyle)
        {
            value_Renamed = value;
            tooltip_Renamed = tooltip;
            itemStyle_Renamed = itemStyle;
        }

        public virtual object Value
        {
            get { return value_Renamed; }
            set { value_Renamed = value; }
        }


        public virtual Tooltip Tooltip
        {
            get { return tooltip_Renamed; }
            set { tooltip_Renamed = value; }
        }


        public virtual ItemStyle ItemStyle
        {
            get { return itemStyle_Renamed; }
            set { itemStyle_Renamed = value; }
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual object value()
        {
            return value_Renamed;
        }

        /// <summary>
        ///     设置value值     *
        /// </summary>
        /// <param name="value"> </param>
        public virtual SeriesData value(object value)
        {
            value_Renamed = value;
            return this;
        }

        /// <summary>
        ///     获取tooltip值
        /// </summary>
        public virtual Tooltip tooltip()
        {
            if (tooltip_Renamed == null)
            {
                tooltip_Renamed = new Tooltip();
            }
            return tooltip_Renamed;
        }

        /// <summary>
        ///     设置tooltip值     *
        /// </summary>
        /// <param name="tooltip"> </param>
        public virtual SeriesData tooltip(Tooltip tooltip)
        {
            tooltip_Renamed = tooltip;
            return this;
        }

        /// <summary>
        ///     获取itemStyle值
        /// </summary>
        public virtual ItemStyle itemStyle()
        {
            if (itemStyle_Renamed == null)
            {
                itemStyle_Renamed = new ItemStyle();
            }
            return itemStyle_Renamed;
        }

        /// <summary>
        ///     设置itemStyle值     *
        /// </summary>
        /// <param name="itemStyle"> </param>
        public virtual SeriesData itemStyle(ItemStyle itemStyle)
        {
            itemStyle_Renamed = itemStyle;
            return this;
        }
    }
}