/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.data
{
    /// <summary>
    ///     Description: PointData
    ///     @author Yongjin.C
    /// </summary>
    public class PointData : BasicData<PointData>
    {
        /// <summary>
        ///     构造函敿
        /// </summary>
        public PointData()
        {
        }

        /// <summary>
        ///     构造函敿参数:name,value
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="value"> </param>
        public PointData(string name, object value) : base(name, value)
        {
        }

        /// <summary>
        ///     构造函敿参数:name,symbol,symbolSize
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="symbol"> </param>
        /// <param name="symbolSize"> </param>
        public PointData(string name, object symbol, object symbolSize) : base(name, symbol, symbolSize)
        {
        }

        /// <summary>
        ///     构造函敿参数:value,symbol
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="symbol"> </param>
        public PointData(object value, object symbol) : base(value, symbol)
        {
        }

        /// <summary>
        ///     构造函敿参数:value,symbol,symbolSize
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="symbol"> </param>
        /// <param name="symbolSize"> </param>
        public PointData(object value, object symbol, object symbolSize) : base(value, symbol, symbolSize)
        {
        }
    }
}