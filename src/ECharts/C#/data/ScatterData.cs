/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace Yongjin.Cshape.echarts.data
{
    /// <summary>
    ///     Description: ScatterData
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class ScatterData
    {
        private const long serialVersionUID = 1L;

        private List<object> value_Renamed;

        /// <summary>
        ///     横值，纵值     *
        /// </summary>
        /// <param name="width"> </param>
        /// <param name="height"> </param>
        public ScatterData(object width, object height)
        {
            value(width, height);
        }

        /// <summary>
        ///     横值，纵值，大小
        /// </summary>
        /// <param name="width"> </param>
        /// <param name="height"> </param>
        /// <param name="size"> </param>
        public ScatterData(object width, object height, object size)
        {
            value(width, height, size);
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual List<object> Value
        {
            get { return value_Renamed; }
            set { value_Renamed = value; }
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual List<object> value()
        {
            if (value_Renamed == null)
            {
                value_Renamed = new List<object>();
            }
            return value_Renamed;
        }

        /// <summary>
        ///     设置values值     *
        /// </summary>
        /// <param name="values"> </param>
        private ScatterData value(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            value().AddRange(values.ToList());
            return this;
        }
    }
}