using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.data
{
    /// <summary>
    ///     MapData
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class MapData
    {
        private const long serialVersionUID = 1L;

        private string name_Renamed;
        private bool? selected_Renamed;
        private object value_Renamed;

        /// <summary>
        ///     构造函敿参数:name,value
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="value"> </param>
        public MapData(string name, object value)
        {
            name_Renamed = name;
            value_Renamed = value;
        }

        /// <summary>
        ///     构造函敿参数:name,value,selected
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="value"> </param>
        /// <param name="selected"> </param>
        public MapData(string name, object value, bool? selected)
        {
            name_Renamed = name;
            value_Renamed = value;
            selected_Renamed = selected;
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string Name
        {
            get { return name_Renamed; }
            set { name_Renamed = value; }
        }


        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual object Value
        {
            get { return value_Renamed; }
            set { value_Renamed = value; }
        }


        /// <summary>
        ///     获取selected值
        /// </summary>
        public virtual bool? Selected
        {
            get { return selected_Renamed; }
            set { selected_Renamed = value; }
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string name()
        {
            return name_Renamed;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual MapData name(string name)
        {
            name_Renamed = name;
            return this;
        }

        /// <summary>
        ///     获取value值
        /// </summary>
        public virtual object value()
        {
            return value_Renamed;
        }

        /// <summary>
        ///     设置value值     *
        /// </summary>
        /// <param name="value"> </param>
        public virtual MapData value(object value)
        {
            value_Renamed = value;
            return this;
        }

        /// <summary>
        ///     获取selected值
        /// </summary>
        public virtual bool? selected()
        {
            return selected_Renamed;
        }

        /// <summary>
        ///     设置selected值     *
        /// </summary>
        /// <param name="selected"> </param>
        public virtual MapData selected(bool? selected)
        {
            selected_Renamed = selected;
            return this;
        }
    }
}