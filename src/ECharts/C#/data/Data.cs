/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.data
{
    /// <summary>
    ///     通用的Data对象...和Data接口同名，Data接口中的data使用的就是这里的Data..
    ///     @author Yongjin.C
    /// </summary>
    public class Data : BasicData<Data>
    {
        /// <summary>
        ///     图标
        /// </summary>
        private string icon_Renamed;

        private object max_Renamed;
        private object min_Renamed;

        private bool? selected_Renamed;

        /// <summary>
        ///     平滑曲线弧度，smooth为true时有效，指定平滑曲线弧度
        /// </summary>
        private double? smoothRadian_Renamed;

        private Tooltip tooltip_Renamed;

        /// <summary>
        ///     可以通过valueIndex:0指定为横轴特殊点
        /// </summary>
        private int? valueIndex_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public Data()
        {
        }

        /// <summary>
        ///     构造函敿参数:name
        /// </summary>
        /// <param name="name"> </param>
        public Data(string name) : base(name)
        {
        }

        /// <summary>
        ///     构造函敿参数:name,value
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="value"> </param>
        public Data(string name, object value) : base(name, value)
        {
        }

        /// <summary>
        ///     构造函敿参数:name,symbol,symbolSize
        /// </summary>
        /// <param name="name"> </param>
        /// <param name="symbol"> </param>
        /// <param name="symbolSize"> </param>
        public Data(string name, object symbol, object symbolSize) : base(name, symbol, symbolSize)
        {
        }

        /// <summary>
        ///     构造函敿参数:value,symbol
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="symbol"> </param>
        public Data(object value, object symbol) : base(value, symbol)
        {
        }

        /// <summary>
        ///     构造函敿参数:value,symbol,symbolSize
        /// </summary>
        /// <param name="value"> </param>
        /// <param name="symbol"> </param>
        /// <param name="symbolSize"> </param>
        public Data(object value, object symbol, object symbolSize) : base(value, symbol, symbolSize)
        {
        }

        /// <summary>
        ///     获取valueIndex值
        /// </summary>
        public virtual int? ValueIndex
        {
            get { return valueIndex_Renamed; }
        }

        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual object Min
        {
            get { return min_Renamed; }
            set { min_Renamed = value; }
        }


        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual object Max
        {
            get { return max_Renamed; }
            set { max_Renamed = value; }
        }


        /// <summary>
        ///     获取icon值
        /// </summary>
        public virtual string Icon
        {
            get { return icon_Renamed; }
            set { icon_Renamed = value; }
        }


        /// <summary>
        ///     获取selected值
        /// </summary>
        public virtual bool? Selected
        {
            get { return selected_Renamed; }
            set { selected_Renamed = value; }
        }


        /// <summary>
        ///     获取tooltip值
        /// </summary>
        public virtual Tooltip Tooltip
        {
            get { return tooltip_Renamed; }
            set { tooltip_Renamed = value; }
        }


        public virtual double? SmoothRadian
        {
            get { return smoothRadian_Renamed; }
            set { smoothRadian_Renamed = value; }
        }

        /// <summary>
        ///     获取平滑曲线弧度
        /// </summary>
        public virtual double? smoothRadian()
        {
            return smoothRadian_Renamed;
        }

        /// <summary>
        ///     设置平滑曲线弧度
        /// </summary>
        /// <param name="smoothRadian"> </param>
        public virtual Data smoothRadian(double? smoothRadian)
        {
            smoothRadian_Renamed = smoothRadian;
            return this;
        }

        /// <summary>
        ///     获取tooltip值
        /// </summary>
        public virtual Tooltip tooltip()
        {
            if (tooltip_Renamed == null)
            {
                tooltip_Renamed = new Tooltip();
            }
            return tooltip_Renamed;
        }

        /// <summary>
        ///     设置tooltip值     *
        /// </summary>
        /// <param name="tooltip"> </param>
        public virtual Data tooltip(Tooltip tooltip)
        {
            tooltip_Renamed = tooltip;
            return this;
        }

        /// <summary>
        ///     获取selected值
        /// </summary>
        public virtual bool? selected()
        {
            return selected_Renamed;
        }

        /// <summary>
        ///     设置selected值     *
        /// </summary>
        /// <param name="selected"> </param>
        public virtual Data selected(bool? selected)
        {
            selected_Renamed = selected;
            return this;
        }

        /// <summary>
        ///     获取icon值
        /// </summary>
        public virtual string icon()
        {
            return icon_Renamed;
        }

        /// <summary>
        ///     设置icon值     *
        /// </summary>
        /// <param name="icon"> </param>
        public virtual Data icon(string icon)
        {
            icon_Renamed = icon;
            return this;
        }

        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual object min()
        {
            return min_Renamed;
        }

        /// <summary>
        ///     设置min值     *
        /// </summary>
        /// <param name="min"> </param>
        public virtual Data min(object min)
        {
            min_Renamed = min;
            return this;
        }

        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual object max()
        {
            return max_Renamed;
        }

        /// <summary>
        ///     设置max值     *
        /// </summary>
        /// <param name="max"> </param>
        public virtual Data max(object max)
        {
            max_Renamed = max;
            return this;
        }

        /// <summary>
        ///     获取valueIndex值
        /// </summary>
        public virtual int? valueIndex()
        {
            return valueIndex_Renamed;
        }

        /// <summary>
        ///     设置valueIndex值     *
        /// </summary>
        /// <param name="valueIndex"> </param>
        public virtual Data valueIndex(int? valueIndex)
        {
            valueIndex_Renamed = valueIndex;
            return this;
        }

        /// <summary>
        ///     设置valueIndex值     *
        /// </summary>
        /// <param name="valueIndex"> </param>
        public virtual Data setValueIndex(int? valueIndex)
        {
            valueIndex_Renamed = valueIndex;
            return this;
        }
    }
}