﻿namespace Yongjin.Cshape.echarts.code
{
    /// <summary>
    ///     线条类型，可选为�?curve'（曲线） | 'line'（直线）
    /// </summary>
    public enum LinkType
    {
        curve,
        line
    }
}