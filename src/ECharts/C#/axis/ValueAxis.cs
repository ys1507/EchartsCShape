/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.axis
{
    /// <summary>
    ///     值轴
    ///     @author Yongjin.C.
    /// </summary>
    public class ValueAxis : Axis<ValueAxis>
    {
        /// <summary>
        ///     [数值型]数值轴两端空白策略，数组内数值代表百分比＿     * [原始数据最小值与最终最小值之间的差额，原始数据最大值与最终最大值之间的差额]
        /// </summary>
        private double?[] boundaryGap_Renamed;

        /// <summary>
        ///     指定的最大值，eg: 100，默认无，会自动根据具体数值调整，指定后将忽略boundaryGap[1]
        /// </summary>
        private int? max_Renamed;

        /// <summary>
        ///     指定的最小值，eg: 0，默认无，会自动根据具体数值调整，指定后将忽略boundaryGap[0]
        /// </summary>
        private int? min_Renamed;

        /// <summary>
        ///     整数精度，默认为100，个位和百位丿
        /// </summary>
        private int? power_Renamed;

        /// <summary>
        ///     小数精度，默认为0，无小数炿
        /// </summary>
        private int? precision_Renamed;

        /// <summary>
        ///     默认值false，脱禿值比例，放大聚焦到最终_min，_max区间
        /// </summary>
        private bool? scale_Renamed;

        /// <summary>
        ///     分割段数，默认为5
        /// </summary>
        private int? splitNumber_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public ValueAxis()
        {
            type(AxisType.value);
        }

        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual int? Min
        {
            get { return min_Renamed; }
            set { min_Renamed = value; }
        }


        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual int? Max
        {
            get { return max_Renamed; }
            set { max_Renamed = value; }
        }


        /// <summary>
        ///     获取scale值
        /// </summary>
        public virtual bool? Scale
        {
            get { return scale_Renamed; }
            set { scale_Renamed = value; }
        }


        /// <summary>
        ///     获取precision值
        /// </summary>
        public virtual int? Precision
        {
            get { return precision_Renamed; }
            set { precision_Renamed = value; }
        }


        /// <summary>
        ///     获取power值
        /// </summary>
        public virtual int? Power
        {
            get { return power_Renamed; }
            set { power_Renamed = value; }
        }


        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? SplitNumber
        {
            get { return splitNumber_Renamed; }
            set { splitNumber_Renamed = value; }
        }


        /// <summary>
        ///     获取boundaryGap值
        /// </summary>
        public virtual double?[] BoundaryGap
        {
            get { return boundaryGap_Renamed; }
            set { boundaryGap_Renamed = value; }
        }

        /// <summary>
        ///     获取min值
        /// </summary>
        public virtual int? min()
        {
            return min_Renamed;
        }

        /// <summary>
        ///     设置min值     *
        /// </summary>
        /// <param name="min"> </param>
        public virtual ValueAxis min(int? min)
        {
            min_Renamed = min;
            return this;
        }

        /// <summary>
        ///     获取max值
        /// </summary>
        public virtual int? max()
        {
            return max_Renamed;
        }

        /// <summary>
        ///     设置max值     *
        /// </summary>
        /// <param name="max"> </param>
        public virtual ValueAxis max(int? max)
        {
            max_Renamed = max;
            return this;
        }

        /// <summary>
        ///     获取scale值
        /// </summary>
        public virtual bool? scale()
        {
            return scale_Renamed;
        }

        /// <summary>
        ///     设置scale值     *
        /// </summary>
        /// <param name="scale"> </param>
        public virtual ValueAxis scale(bool? scale)
        {
            scale_Renamed = scale;
            return this;
        }

        /// <summary>
        ///     获取precision值
        /// </summary>
        public virtual int? precision()
        {
            return precision_Renamed;
        }

        /// <summary>
        ///     设置precision值     *
        /// </summary>
        /// <param name="precision"> </param>
        public virtual ValueAxis precision(int? precision)
        {
            precision_Renamed = precision;
            return this;
        }

        /// <summary>
        ///     获取power值
        /// </summary>
        public virtual int? power()
        {
            return power_Renamed;
        }

        /// <summary>
        ///     设置power值     *
        /// </summary>
        /// <param name="power"> </param>
        public virtual ValueAxis power(int? power)
        {
            power_Renamed = power;
            return this;
        }

        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? splitNumber()
        {
            return splitNumber_Renamed;
        }

        /// <summary>
        ///     设置splitNumber值     *
        /// </summary>
        /// <param name="splitNumber"> </param>
        public virtual ValueAxis splitNumber(int? splitNumber)
        {
            splitNumber_Renamed = splitNumber;
            return this;
        }


        /// <summary>
        ///     获取boundaryGap值
        /// </summary>
        public virtual double?[] boundaryGap()
        {
            if (boundaryGap_Renamed == null)
            {
                boundaryGap_Renamed = new double?[2];
            }
            return boundaryGap_Renamed;
        }

        /// <summary>
        ///     设置boundaryGap值     *
        /// </summary>
        /// <param name="boundaryGap"> </param>
        public virtual ValueAxis boundaryGap(double?[] boundaryGap)
        {
            boundaryGap_Renamed = boundaryGap;
            return this;
        }

        /// <summary>
        ///     设置boundaryGap值     *
        /// </summary>
        /// <param name="min"> </param>
        /// <param name="max"> </param>
        public virtual ValueAxis boundaryGap(double? min, double? max)
        {
            boundaryGap()[0] = min;
            boundaryGap()[1] = max;
            return this;
        }
    }
}