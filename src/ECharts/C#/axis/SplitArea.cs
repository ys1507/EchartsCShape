using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.axis
{
    /// <summary>
    ///     分隔区域
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class SplitArea
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     属性areaStyle（详见areaStyle）控制区域样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.AreaStyle
        /// </seealso>
        private AreaStyle areaStyle_Renamed;

        /// <summary>
        ///     默认不显示，属性show控制显示与否
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     获取areaStyle值
        /// </summary>
        public virtual AreaStyle AreaStyle
        {
            get { return areaStyle_Renamed; }
            set { areaStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual SplitArea show(bool? show)
        {
            show_Renamed = show;
            return this;
        }

        /// <summary>
        ///     设置areaStyle值     *
        /// </summary>
        /// <param name="areaStyle"> </param>
        public virtual SplitArea areaStyle(AreaStyle areaStyle)
        {
            areaStyle_Renamed = areaStyle;
            return this;
        }

        /// <summary>
        ///     属性areaStyle（详见areaStyle）控制区域样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.AreaStyle
        /// </seealso>
        public virtual AreaStyle areaStyle()
        {
            if (areaStyle_Renamed == null)
            {
                areaStyle_Renamed = new AreaStyle();
            }
            return areaStyle_Renamed;
        }
    }
}