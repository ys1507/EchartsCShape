using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.axis
{
    /// <summary>
    ///     时间型坐标轴用法同数值型，只是目标处理和格式化显示时会自动转变为时间，并且随着时间跨度的不同自动切换需要显示的时间粒度
    ///     @author Yongjin.C
    /// </summary>
    public class TimeAxis : ValueAxis
    {
        /// <summary>
        ///     构造函敿
        /// </summary>
        public TimeAxis()
        {
            type(AxisType.time);
        }
    }
}