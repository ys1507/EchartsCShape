/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts.axis
{
    /// <summary>
    ///     坐标轿 *
    ///     @author Yongjin.C
    /// </summary>
    public abstract class Axis<T> : AbstractData<T>, Component where T : class
    {
        /// <summary>
        ///     坐标轴文本标签，详见axis.axisLabel
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisLabel
        /// </seealso>
        private AxisLabel axisLabel_Renamed;
        public AxisLabel AxisLabel
        {
            get { return axisLabel_Renamed; }
            set { axisLabel_Renamed = value; }
        }

        /// <summary>
        ///     坐标轴线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisLine
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        private AxisLine axisLine_Renamed;

        public virtual AxisLine AxisLine
        {
            get { return axisLine_Renamed; }
            set { axisLine_Renamed = value; }
        }

        /// <summary>
        ///     坐标轴小标记，默认不显示，属性show控制显示与否，属性length控制线长，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisTick
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        private AxisTick axisTick_Renamed;

        public virtual AxisTick AxisTick
        {
            get { return axisTick_Renamed; }
            set { axisTick_Renamed = value; }
        }
        /// <summary>
        ///     坐标轴名称位置，默认丿end'，可选为＿start' | 'end'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.NameLocation
        /// </seealso>
        private NameLocation nameLocation_Renamed;

        /// <summary>
        ///     获取nameLocation值
        /// </summary>
        public virtual NameLocation NameLocation
        {
            get { return nameLocation_Renamed; }
            set { nameLocation_Renamed = value; }
        }
        /// <summary>
        ///     坐标轴名称文字样式，默认取全局配置，颜色跟随axisLine主色，可访
        /// </summary>
        private LineStyle nameTextStyle_Renamed;
        public virtual LineStyle LineStyle
        {
            get { return nameTextStyle_Renamed; }
            set { nameTextStyle_Renamed = value; }
        }
        /// <summary>
        ///     坐标轴名称，默认为空
        /// </summary>
        private string name_Renamed;

        /// <summary>
        ///     坐标轴类型，横轴默认为类目型'bottom'，纵轴默认为数值型'left'，可选为＿bottom' | 'top' | 'left' | 'right'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.X
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Y
        /// </seealso>
        private object position_Renamed;


        /// <summary>
        ///     是否显示
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     分隔区域，默认不显示，属性show控制显示与否，属性areaStyle（详见areaStyle）控制区域样弿
        /// </summary>
        private SplitArea splitArea_Renamed;
        public virtual SplitArea SplitArea
        {
            get { return splitArea_Renamed; }
            set { splitArea_Renamed = value; }
        }
        /// <summary>
        ///     分隔线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.SplitLine
        /// </seealso>
        private SplitLine splitLine_Renamed;
        public virtual SplitLine SplitLine
        {
            get { return splitLine_Renamed; }
            set { splitLine_Renamed = value; }
        }
        /// <summary>
        ///     坐标轴类型，横轴默认为类目型'category'，纵轴默认为数值型'value'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.AxisType
        /// </seealso>
        private AxisType type_Renamed;

        

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual AxisType Type
        {
            get { return type_Renamed; }
            set { type_Renamed = value; }
        }


        /// <summary>
        ///     获取position值
        /// </summary>
        public virtual object Position
        {
            get { return position_Renamed; }
            set { position_Renamed = value; }
        }


        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string Name
        {
            get { return name_Renamed; }
            set { name_Renamed = value; }
        }



        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }


        /// <summary>
        ///     获取nameTextStyle值
        /// </summary>
        public virtual LineStyle NameTextStyle
        {
            get { return nameTextStyle_Renamed; }
            set { nameTextStyle_Renamed = value; }
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual T show(bool? show)
        {
            show_Renamed = show;
            return this as T;
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual AxisType type()
        {
            return type_Renamed;
        }


        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual T type(AxisType type)
        {
            type_Renamed = type;
            return this as T;
        }

        /// <summary>
        ///     获取position值
        /// </summary>
        public virtual object position()
        {
            return position_Renamed;
        }

        /// <summary>
        ///     设置position值     *
        /// </summary>
        /// <param name="position"> </param>
        public virtual T position(object position)
        {
            position_Renamed = position;
            return this as T;
        }

        /// <summary>
        ///     设置position值     *
        /// </summary>
        /// <param name="position"> </param>
        public virtual T position(X position)
        {
            position_Renamed = position;
            return this as T;
        }

        /// <summary>
        ///     设置position值     *
        /// </summary>
        /// <param name="position"> </param>
        public virtual T position(Y position)
        {
            position_Renamed = position;
            return this as T;
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual string name()
        {
            return name_Renamed;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual T name(string name)
        {
            name_Renamed = name;
            return this as T;
        }

        /// <summary>
        ///     获取nameLocation值
        /// </summary>
        public virtual NameLocation nameLocation()
        {
            return nameLocation_Renamed;
        }

        /// <summary>
        ///     设置nameLocation值     *
        /// </summary>
        /// <param name="nameLocation"> </param>
        public virtual T nameLocation(NameLocation nameLocation)
        {
            nameLocation_Renamed = nameLocation;
            return this as T;
        }

        /// <summary>
        ///     坐标轴名称文字样式，默认取全局配置，颜色跟随axisLine主色，可访
        /// </summary>
        public virtual LineStyle nameTextStyle()
        {
            if (nameTextStyle_Renamed == null)
            {
                nameTextStyle_Renamed = new LineStyle();
            }
            return nameTextStyle_Renamed;
        }

        /// <summary>
        ///     设置style值     *
        /// </summary>
        /// <param name="style"> </param>
        public virtual T nameTextStyle(LineStyle style)
        {
            nameTextStyle_Renamed = style;
            return this as T;
        }

        /// <summary>
        ///     坐标轴线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisLine
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        public virtual AxisLine axisLine()
        {
            if (axisLine_Renamed == null)
            {
                axisLine_Renamed = new AxisLine();
            }
            return axisLine_Renamed;
        }
       
        /// <summary>
        ///     设置axisLine值     *
        /// </summary>
        /// <param name="axisLine"> </param>
        public virtual T axisLine(AxisLine axisLine)
        {
            axisLine_Renamed = axisLine;
            return this as T;
        }


        /// <summary>
        ///     坐标轴小标记，默认不显示，属性show控制显示与否，属性length控制线长，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisTick
        /// </seealso>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        public virtual AxisTick axisTick()
        {
            if (axisTick_Renamed == null)
            {
                axisTick_Renamed = new AxisTick();
            }
            return axisTick_Renamed;
        }

        /// <summary>
        ///     设置axisTick值     *
        /// </summary>
        /// <param name="axisTick"> </param>
        public virtual T axisTick(AxisTick axisTick)
        {
            axisTick_Renamed = axisTick;
            return this as T;
        }

        /// <summary>
        ///     坐标轴文本标签，详见axis.axisLabel
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisLabel
        /// </seealso>
        public virtual AxisLabel axisLabel()
        {
            if (axisLabel_Renamed == null)
            {
                axisLabel_Renamed = new AxisLabel();
            }
            return axisLabel_Renamed;
        }

        /// <param name="label">
        ///     @return
        /// </param>
        public virtual T axisLabel(AxisLabel label)
        {
            axisLabel_Renamed = label;
            return this as T;
        }


        /// <summary>
        ///     分隔线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.SplitLine
        /// </seealso>
        public virtual SplitLine splitLine()
        {
            if (splitLine_Renamed == null)
            {
                splitLine_Renamed = new SplitLine();
            }
            return splitLine_Renamed;
        }

        /// <summary>
        ///     设置splitLine值     *
        /// </summary>
        /// <param name="splitLine"> </param>
        public virtual T splitLine(SplitLine splitLine)
        {
            if (splitLine_Renamed == null)
            {
                splitLine_Renamed = splitLine;
            }
            return this as T;
        }

        /// <summary>
        ///     分隔区域，默认不显示，属性show控制显示与否，属性areaStyle（详见areaStyle）控制区域样弿
        /// </summary>
        public virtual SplitArea splitArea()
        {
            if (splitArea_Renamed == null)
            {
                splitArea_Renamed = new SplitArea();
            }
            return splitArea_Renamed;
        }

        /// <summary>
        ///     设置splitArea值     *
        /// </summary>
        /// <param name="splitArea"> </param>
        public virtual T splitArea(SplitArea splitArea)
        {
            splitArea_Renamed = splitArea;
            return this as T;
        }

        /// <summary>
        ///     添加坐标轴的类目属怿     *
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        public  override T data(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this as T;
            }
            if (data_Renamed == null)
            {
                if (type_Renamed == AxisType.category)
                {
                    data_Renamed = new List<object>();
                }
                else
                {
                    throw new Exception("数据轴不能添加类目信恿");
                }
            }
            data_Renamed.AddRange(values.ToList());
            return this as T;
        }


        /// <summary>
        ///     获取axisLine值
        /// </summary>
        public virtual AxisLine getAxisLine()
        {
            return axisLine_Renamed;
        }

        /// <summary>
        ///     设置axisLine值     *
        /// </summary>
        /// <param name="axisLine"> </param>
        public virtual void setAxisLine(AxisLine axisLine)
        {
            axisLine_Renamed = axisLine;
        }

        /// <summary>
        ///     获取axisTick值
        /// </summary>
        public virtual AxisTick getAxisTick()
        {
            return axisTick_Renamed;
        }

        /// <summary>
        ///     设置axisTick值     *
        /// </summary>
        /// <param name="axisTick"> </param>
        public virtual void setAxisTick(AxisTick axisTick)
        {
            axisTick_Renamed = axisTick;
        }

        /// <summary>
        ///     获取axisLabel值
        /// </summary>
        public virtual AxisLabel getAxisLabel()
        {
            return axisLabel_Renamed;
        }

        /// <summary>
        ///     设置axisLabel值     *
        /// </summary>
        /// <param name="axisLabel"> </param>
        public virtual void setAxisLabel(AxisLabel axisLabel)
        {
            axisLabel_Renamed = axisLabel;
        }

        /// <summary>
        ///     获取splitLine值
        /// </summary>
        public virtual SplitLine getSplitLine()
        {
            return splitLine_Renamed;
        }

        /// <summary>
        ///     设置splitLine值     *
        /// </summary>
        /// <param name="splitLine"> </param>
        public virtual void setSplitLine(SplitLine splitLine)
        {
            splitLine_Renamed = splitLine;
        }

        /// <summary>
        ///     获取splitArea值
        /// </summary>
        public virtual SplitArea getSplitArea()
        {
            return splitArea_Renamed;
        }

        /// <summary>
        ///     设置splitArea值     *
        /// </summary>
        /// <param name="splitArea"> </param>
        public virtual void setSplitArea(SplitArea splitArea)
        {
            splitArea_Renamed = splitArea;
        }
    }
}