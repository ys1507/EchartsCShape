using System;
using Yongjin.Cshape.echarts.style;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.axis
{
    /// <summary>
    ///     坐标轴小标记
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class AxisTick
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     小标记是否显示为在grid内部，默认在外部
        /// </summary>
        private bool? inside_Renamed;

        /// <summary>
        ///     小标记显示挑选间隔，默认丿auto'，可选为＿     * 'auto'（自动隐藏显示不下的＿| 0（全部显示） | {number}（用户指定选择间隔＿
        /// </summary>
        private object interval_Renamed;

        /// <summary>
        ///     默认值，属性length控制线长
        /// </summary>
        private int? length_Renamed;

        /// <summary>
        ///     属性lineStyle控制线条样式，（详见lineStyle＿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        private LineStyle lineStyle_Renamed;

        /// <summary>
        ///     小标记是否显示为间隔，默认等于boundaryGap
        /// </summary>
        private bool? onGap_Renamed;

        /// <summary>
        ///     是否显示，默认为false，设为true后下面为默认样式
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     分割段数，默认为5，为0时为线性渐变，calculable为true是默认均刿00仿
        /// </summary>
        private int? splitNumber_Renamed;

        /// <summary>
        ///     获取lineStyle值
        /// </summary>
        public virtual LineStyle LineStyle
        {
            get { return lineStyle_Renamed; }
            set { lineStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }


        /// <summary>
        ///     获取interval值
        /// </summary>
        public virtual object Interval
        {
            get { return interval_Renamed; }
            set { interval_Renamed = value; }
        }


        /// <summary>
        ///     获取onGap值
        /// </summary>
        public virtual bool? OnGap
        {
            get { return onGap_Renamed; }
            set { onGap_Renamed = value; }
        }


        /// <summary>
        ///     获取inside值
        /// </summary>
        public virtual bool? Inside
        {
            get { return inside_Renamed; }
            set { inside_Renamed = value; }
        }


        /// <summary>
        ///     获取length值
        /// </summary>
        public virtual int? Length
        {
            get { return length_Renamed; }
            set { length_Renamed = value; }
        }


        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? SplitNumber
        {
            get { return splitNumber_Renamed; }
            set { splitNumber_Renamed = value; }
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual AxisTick show(bool? show)
        {
            show_Renamed = show;
            return this;
        }

        /// <summary>
        ///     获取interval值
        /// </summary>
        public virtual object interval()
        {
            return interval_Renamed;
        }

        /// <summary>
        ///     设置interval值     *
        /// </summary>
        /// <param name="interval"> </param>
        public virtual AxisTick interval(object interval)
        {
            interval_Renamed = interval;
            return this;
        }

        /// <summary>
        ///     获取onGap值
        /// </summary>
        public virtual bool? onGap()
        {
            return onGap_Renamed;
        }

        /// <summary>
        ///     设置onGap值     *
        /// </summary>
        /// <param name="onGap"> </param>
        public virtual AxisTick onGap(bool? onGap)
        {
            onGap_Renamed = onGap;
            return this;
        }

        /// <summary>
        ///     获取inside值
        /// </summary>
        public virtual bool? inside()
        {
            return inside_Renamed;
        }

        /// <summary>
        ///     设置inside值     *
        /// </summary>
        /// <param name="inside"> </param>
        public virtual AxisTick inside(bool? inside)
        {
            inside_Renamed = inside;
            return this;
        }

        /// <summary>
        ///     获取length值
        /// </summary>
        public virtual int? length()
        {
            return length_Renamed;
        }

        /// <summary>
        ///     设置length值     *
        /// </summary>
        /// <param name="length"> </param>
        public virtual AxisTick length(int? length)
        {
            length_Renamed = length;
            return this;
        }

        /// <summary>
        ///     属性lineStyle控制线条样式，（详见lineStyle＿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        public virtual LineStyle lineStyle()
        {
            if (lineStyle_Renamed == null)
            {
                lineStyle_Renamed = new LineStyle();
            }
            return lineStyle_Renamed;
        }

        /// <summary>
        ///     设置style值     *
        /// </summary>
        /// <param name="style"> </param>
        public virtual AxisTick lineStyle(LineStyle style)
        {
            lineStyle_Renamed = style;
            return this;
        }

        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? splitNumber()
        {
            return splitNumber_Renamed;
        }

        /// <summary>
        ///     设置splitNumber值     *
        /// </summary>
        /// <param name="splitNumber"> </param>
        public virtual AxisTick splitNumber(int? splitNumber)
        {
            splitNumber_Renamed = splitNumber;
            return this;
        }
    }
}