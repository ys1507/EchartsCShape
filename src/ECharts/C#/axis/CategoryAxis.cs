/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.axis
{
    /// <summary>
    ///     类目轿 *
    ///     @author Yongjin.C
    /// </summary>
    public class CategoryAxis : Axis<CategoryAxis>
    {
        /// <summary>
        ///     [类目型]类目起始和结束两端空白策略，见下图，默认为true留空，false则顶夿
        /// </summary>
        private bool? boundaryGap_Renamed;

        /// <summary>
        ///     构造函敿
        /// </summary>
        public CategoryAxis()
        {
            type(AxisType.category);
        }

        /// <summary>
        ///     获取boundaryGap值     *
        /// </summary>
        public virtual bool? BoundaryGap
        {
            get { return boundaryGap_Renamed; }
            set { boundaryGap_Renamed = value; }
        }

        /// <summary>
        ///     获取boundaryGap值
        /// </summary>
        public virtual bool? boundaryGap()
        {
            return boundaryGap_Renamed;
        }

        /// <summary>
        ///     设置boundaryGap值     *
        /// </summary>
        /// <param name="boundaryGap"> </param>
        public virtual CategoryAxis boundaryGap(bool? boundaryGap)
        {
            boundaryGap_Renamed = boundaryGap;
            return this;
        }
    }
}