using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.style
{
    /// <summary>
    ///     弦样弿 *
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class ChordStyle
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     ribbon的描边颜艿
        /// </summary>
        private string borderColor_Renamed;

        /// <summary>
        ///     ribbon的描边线宿
        /// </summary>
        private int? borderWidth_Renamed;

        /// <summary>
        ///     贝塞尔曲线的颜色
        /// </summary>
        private string color_Renamed;

        /// <summary>
        ///     透明庿
        /// </summary>
        private double? opacity_Renamed;

        /// <summary>
        ///     贝塞尔曲线的宽度
        /// </summary>
        private int? width_Renamed;

        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string BorderColor
        {
            get { return borderColor_Renamed; }
            set { borderColor_Renamed = value; }
        }


        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? BorderWidth
        {
            get { return borderWidth_Renamed; }
            set { borderWidth_Renamed = value; }
        }


        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual int? Width
        {
            get { return width_Renamed; }
            set { width_Renamed = value; }
        }


        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }


        /// <summary>
        ///     获取opacity值
        /// </summary>
        public virtual double? Opacity
        {
            get { return opacity_Renamed; }
            set { opacity_Renamed = value; }
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual ChordStyle color(string color)
        {
            color_Renamed = color;
            return this;
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual double? opacity()
        {
            return opacity_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="opacity"> </param>
        public virtual ChordStyle opacity(double? opacity)
        {
            opacity_Renamed = opacity;
            return this;
        }

        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string borderColor()
        {
            return borderColor_Renamed;
        }

        /// <summary>
        ///     设置borderColor值     *
        /// </summary>
        /// <param name="borderColor"> </param>
        public virtual ChordStyle borderColor(string borderColor)
        {
            borderColor_Renamed = borderColor;
            return this;
        }

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual int? width()
        {
            return width_Renamed;
        }

        /// <summary>
        ///     设置width值     *
        /// </summary>
        /// <param name="width"> </param>
        public virtual ChordStyle width(int? width)
        {
            width_Renamed = width;
            return this;
        }

        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? borderWidth()
        {
            return borderWidth_Renamed;
        }

        /// <summary>
        ///     设置borderWidth值     *
        /// </summary>
        /// <param name="borderWidth"> </param>
        public virtual ChordStyle borderWidth(int? borderWidth)
        {
            borderWidth_Renamed = borderWidth;
            return this;
        }
    }
}