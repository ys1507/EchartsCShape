using System;
using Yongjin.Cshape.echarts.code;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.style
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class LineStyle
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     阴线颜色
        /// </summary>
        private object color0_Renamed;

        /// <summary>
        ///     阳线颜色
        /// </summary>
        private object color_Renamed;

        /// <summary>
        ///     默认值，折线主线IE8+)有效，阴影模糊度，大亿有效
        /// </summary>
        private int? shadowBlur_Renamed;

        /// <summary>
        ///     折线主线(IE8+)有效，阴影色彩，支持rgba
        /// </summary>
        private string shadowColor_Renamed;

        /// <summary>
        ///     默认值，折线主线IE8+)有效，阴影横向偏移，正值往右，负值往巿
        /// </summary>
        private int? shadowOffsetX_Renamed;

        /// <summary>
        ///     默认值，折线主线IE8+)有效，阴影纵向偏移，正值往下，负值往丿
        /// </summary>
        private int? shadowOffsetY_Renamed;

        /// <summary>
        ///     线条样式，可选为＿solid' | 'dotted' | 'dashed'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.LineType
        /// </seealso>
        private LineType type_Renamed;

        /// <summary>
        ///     线宽
        /// </summary>
        private int? width_Renamed;

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual object Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }


        /// <summary>
        ///     获取color0值
        /// </summary>
        public virtual object Color0
        {
            get { return color0_Renamed; }
            set { color0_Renamed = value; }
        }


        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual LineType Type
        {
            get { return type_Renamed; }
            set { type_Renamed = value; }
        }


        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual int? Width
        {
            get { return width_Renamed; }
            set { width_Renamed = value; }
        }


        /// <summary>
        ///     获取shadowColor值
        /// </summary>
        public virtual string ShadowColor
        {
            get { return shadowColor_Renamed; }
            set { shadowColor_Renamed = value; }
        }


        /// <summary>
        ///     获取shadowBlur值
        /// </summary>
        public virtual int? ShadowBlur
        {
            get { return shadowBlur_Renamed; }
            set { shadowBlur_Renamed = value; }
        }


        /// <summary>
        ///     获取shadowOffsetX值
        /// </summary>
        public virtual int? ShadowOffsetX
        {
            get { return shadowOffsetX_Renamed; }
            set { shadowOffsetX_Renamed = value; }
        }


        /// <summary>
        ///     获取shadowOffsetY值
        /// </summary>
        public virtual int? ShadowOffsetY
        {
            get { return shadowOffsetY_Renamed; }
            set { shadowOffsetY_Renamed = value; }
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual object color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual LineStyle color(object color)
        {
            color_Renamed = color;
            return this;
        }

        /// <summary>
        ///     获取color0值
        /// </summary>
        public virtual object color0()
        {
            return color0_Renamed;
        }

        /// <summary>
        ///     设置color0值     *
        /// </summary>
        /// <param name="color0"> </param>
        public virtual LineStyle color0(object color0)
        {
            color0_Renamed = color0;
            return this;
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual LineType type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual LineStyle type(LineType type)
        {
            type_Renamed = type;
            return this;
        }

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual int? width()
        {
            return width_Renamed;
        }

        /// <summary>
        ///     设置width值     *
        /// </summary>
        /// <param name="width"> </param>
        public virtual LineStyle width(int? width)
        {
            width_Renamed = width;
            return this;
        }

        /// <summary>
        ///     获取shadowColor值
        /// </summary>
        public virtual string shadowColor()
        {
            return shadowColor_Renamed;
        }

        /// <summary>
        ///     设置shadowColor值     *
        /// </summary>
        /// <param name="shadowColor"> </param>
        public virtual LineStyle shadowColor(string shadowColor)
        {
            shadowColor_Renamed = shadowColor;
            return this;
        }

        /// <summary>
        ///     获取shadowBlur值
        /// </summary>
        public virtual int? shadowBlur()
        {
            return shadowBlur_Renamed;
        }

        /// <summary>
        ///     设置shadowBlur值     *
        /// </summary>
        /// <param name="shadowBlur"> </param>
        public virtual LineStyle shadowBlur(int? shadowBlur)
        {
            shadowBlur_Renamed = shadowBlur;
            return this;
        }

        /// <summary>
        ///     获取shadowOffsetX值
        /// </summary>
        public virtual int? shadowOffsetX()
        {
            return shadowOffsetX_Renamed;
        }

        /// <summary>
        ///     设置shadowOffsetX值     *
        /// </summary>
        /// <param name="shadowOffsetX"> </param>
        public virtual LineStyle shadowOffsetX(int? shadowOffsetX)
        {
            shadowOffsetX_Renamed = shadowOffsetX;
            return this;
        }

        /// <summary>
        ///     获取shadowOffsetY值
        /// </summary>
        public virtual int? shadowOffsetY()
        {
            return shadowOffsetY_Renamed;
        }

        /// <summary>
        ///     设置shadowOffsetY值     *
        /// </summary>
        /// <param name="shadowOffsetY"> </param>
        public virtual LineStyle shadowOffsetY(int? shadowOffsetY)
        {
            shadowOffsetY_Renamed = shadowOffsetY;
            return this;
        }
    }
}