using System;
using Yongjin.Cshape.echarts.code;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.style
{
    /// <summary>
    ///     Description: LinkStyle
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class LinkStyle
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     线条颜色
        /// </summary>
        private string borderColor_Renamed;

        /// <summary>
        ///     线宽
        /// </summary>
        private int? borderWidth_Renamed;

        /// <summary>
        ///     线条类型，可选为＿curve'（曲线） | 'line'（直线）
        /// </summary>
        private LinkType type_Renamed;

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual LinkType Type
        {
            get { return type_Renamed; }
            set { type_Renamed = value; }
        }


        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string BorderColor
        {
            get { return borderColor_Renamed; }
            set { borderColor_Renamed = value; }
        }


        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? BorderWidth
        {
            get { return borderWidth_Renamed; }
            set { borderWidth_Renamed = value; }
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual LinkType type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual LinkStyle type(LinkType type)
        {
            type_Renamed = type;
            return this;
        }

        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string borderColor()
        {
            return borderColor_Renamed;
        }

        /// <summary>
        ///     设置borderColor值     *
        /// </summary>
        /// <param name="borderColor"> </param>
        public virtual LinkStyle borderColor(string borderColor)
        {
            borderColor_Renamed = borderColor;
            return this;
        }

        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? borderWidth()
        {
            return borderWidth_Renamed;
        }

        /// <summary>
        ///     设置borderWidth值     *
        /// </summary>
        /// <param name="borderWidth"> </param>
        public virtual LinkStyle borderWidth(int? borderWidth)
        {
            borderWidth_Renamed = borderWidth;
            return this;
        }
    }
}