using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.style
{
    /// <summary>
    ///     时间轴当前点，该类只在Timeline中使甿 *
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class CheckpointStyle
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     当前点symbol边线颜色
        /// </summary>
        private string borderColor_Renamed;

        /// <summary>
        ///     当前点symbol边线宽度
        /// </summary>
        private object borderWidth_Renamed;

        /// <summary>
        ///     当前点symbol颜色，默认为随当前点颜色，可指定具体颜色，如无则丿#1e90ff'
        /// </summary>
        private string color_Renamed;

        /// <seealso cref= Yongjin.Cshape.echarts.Label
        /// </seealso>
        private Label label_Renamed;

        /// <summary>
        ///     当前点symbol大小，默认随轴上symbol大小
        /// </summary>
        private object symbolSize_Renamed;

        /// <summary>
        ///     当前点symbol，默认随轴上的symbol
        /// </summary>
        private object symbol_Renamed;

        /// <summary>
        ///     获取label值
        /// </summary>
        public virtual Label Label
        {
            get { return label_Renamed; }
            set { label_Renamed = value; }
        }


        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object Symbol
        {
            get { return symbol_Renamed; }
            set { symbol_Renamed = value; }
        }


        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object SymbolSize
        {
            get { return symbolSize_Renamed; }
            set { symbolSize_Renamed = value; }
        }


        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }


        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string BorderColor
        {
            get { return borderColor_Renamed; }
            set { borderColor_Renamed = value; }
        }


        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual object BorderWidth
        {
            get { return borderWidth_Renamed; }
            set { borderWidth_Renamed = value; }
        }

        /// <summary>
        ///     设置label值     *
        /// </summary>
        /// <param name="label"> </param>
        public virtual CheckpointStyle label(Label label)
        {
            label_Renamed = label;
            return this;
        }

        /// <summary>
        ///     获取symbol值
        /// </summary>
        public virtual object symbol()
        {
            return symbol_Renamed;
        }

        /// <summary>
        ///     设置symbol值     *
        /// </summary>
        /// <param name="symbol"> </param>
        public virtual CheckpointStyle symbol(object symbol)
        {
            symbol_Renamed = symbol;
            return this;
        }

        /// <summary>
        ///     获取symbolSize值
        /// </summary>
        public virtual object symbolSize()
        {
            return symbolSize_Renamed;
        }

        /// <summary>
        ///     设置symbolSize值     *
        /// </summary>
        /// <param name="symbolSize"> </param>
        public virtual CheckpointStyle symbolSize(object symbolSize)
        {
            symbolSize_Renamed = symbolSize;
            return this;
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual CheckpointStyle color(string color)
        {
            color_Renamed = color;
            return this;
        }

        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string borderColor()
        {
            return borderColor_Renamed;
        }

        /// <summary>
        ///     设置borderColor值     *
        /// </summary>
        /// <param name="borderColor"> </param>
        public virtual CheckpointStyle borderColor(string borderColor)
        {
            borderColor_Renamed = borderColor;
            return this;
        }

        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual object borderWidth()
        {
            return borderWidth_Renamed;
        }

        /// <summary>
        ///     设置borderWidth值     *
        /// </summary>
        /// <param name="borderWidth"> </param>
        public virtual CheckpointStyle borderWidth(object borderWidth)
        {
            borderWidth_Renamed = borderWidth;
            return this;
        }

        /// <seealso cref= Yongjin.Cshape.echarts.Label
        /// </seealso>
        public virtual Label label()
        {
            if (label_Renamed == null)
            {
                label_Renamed = new Label();
            }
            return label_Renamed;
        }
    }
}