/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using Yongjin.Cshape.echarts.code;

namespace Yongjin.Cshape.echarts.style
{
    /// <summary>
    ///     Description: NodeStyle
    ///     @author Yongjin.C
    /// </summary>
    public class NodeStyle : LinkStyle
    {
        /// <summary>
        ///     可逿'both', 'stroke', 'fill'
        /// </summary>
        private BrushType brushType_Renamed;

        /// <summary>
        ///     填充颜色
        /// </summary>
        private string color_Renamed;

        /// <summary>
        ///     获取brushType值
        /// </summary>
        public virtual BrushType BrushType
        {
            get { return brushType_Renamed; }
            set { brushType_Renamed = value; }
        }


        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }

        /// <summary>
        ///     获取brushType值
        /// </summary>
        public virtual BrushType brushType()
        {
            return brushType_Renamed;
        }

        /// <summary>
        ///     设置brushType值     *
        /// </summary>
        /// <param name="brushType"> </param>
        public virtual NodeStyle brushType(BrushType brushType)
        {
            brushType_Renamed = brushType;
            return this;
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual NodeStyle color(string color)
        {
            color_Renamed = color;
            return this;
        }
    }
}