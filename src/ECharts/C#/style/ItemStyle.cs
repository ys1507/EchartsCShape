using System;
using Yongjin.Cshape.echarts.style.itemstyle;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.style
{
    /// <summary>
    ///     Description: ItemStyle
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class ItemStyle
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     强调样式（悬浮时样式＿
        /// </summary>
        private Emphasis emphasis_Renamed;

        /// <summary>
        ///     默认样式
        /// </summary>
        private Normal normal_Renamed;

        /// <summary>
        ///     获取normal值
        /// </summary>
        public virtual Normal Normal
        {
            get { return normal_Renamed; }
            set { normal_Renamed = value; }
        }


        /// <summary>
        ///     获取emphasis值
        /// </summary>
        public virtual Emphasis Emphasis
        {
            get { return emphasis_Renamed; }
            set { emphasis_Renamed = value; }
        }

        /// <summary>
        ///     获取normal值
        /// </summary>
        public virtual Normal normal()
        {
            if (normal_Renamed == null)
            {
                normal_Renamed = new Normal();
            }
            return normal_Renamed;
        }

        /// <summary>
        ///     设置normal值     *
        /// </summary>
        /// <param name="normal"> </param>
        public virtual ItemStyle normal(Normal normal)
        {
            normal_Renamed = normal;
            return this;
        }

        /// <summary>
        ///     获取emphasis值
        /// </summary>
        public virtual Emphasis emphasis()
        {
            if (emphasis_Renamed == null)
            {
                emphasis_Renamed = new Emphasis();
            }
            return emphasis_Renamed;
        }

        /// <summary>
        ///     设置emphasis值     *
        /// </summary>
        /// <param name="emphasis"> </param>
        public virtual ItemStyle emphasis(Emphasis emphasis)
        {
            emphasis_Renamed = emphasis;
            return this;
        }
    }
}