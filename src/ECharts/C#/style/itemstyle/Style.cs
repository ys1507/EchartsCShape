using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.style.itemstyle
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public abstract class Style<T> where T : class
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     区域样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.AreaStyle
        /// </seealso>
        private AreaStyle areaStyle_Renamed;

        public AreaStyle AreaStyle
        {
            set { areaStyle_Renamed = value; }
            get { return areaStyle_Renamed; }
        }

        /// <summary>
        ///     柱形边框颜色
        /// </summary>
        private string barBorderColor_Renamed;

        /// <summary>
        ///     柱形边框圆角，单位px，默认为0
        /// </summary>
        private int? barBorderRadius_Renamed;

        /// <summary>
        ///     柱形边框线宽，单位px，默认为1
        /// </summary>
        private int? barBorderWidth_Renamed;

        /// <summary>
        ///     边框颜色
        /// </summary>
        private string borderColor_Renamed;

        /// <summary>
        ///     边框圆角，单位px，默认为0
        /// </summary>
        private int? borderRadius_Renamed;

        /// <summary>
        ///     边框线宽，单位px，默认为1
        /// </summary>
        private int? borderWidth_Renamed;

        /// <summary>
        ///     和弦囿- 弦样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.ChordStyle
        /// </seealso>
        private ChordStyle chordStyle_Renamed;

        public ChordStyle ChordStyle
        {
            set { chordStyle_Renamed = value; }
            get { return chordStyle_Renamed; }
        }

        /// <summary>
        ///     阴线颜色
        /// </summary>
        private string color0_Renamed;

        /// <summary>
        ///     阳线颜色
        /// </summary>
        private string color_Renamed;

        /// <summary>
        ///     饼图标签视觉引导线，默认显示
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.LabelLine
        /// </seealso>
        private LabelLine labelLine_Renamed;

        /// <summary>
        ///     标签，饼图默认显示在外部，离饼图距离由labelLine.length决定，地图标签不可指定位罿     * 折线图，柱形图，K线图，散点图可指定position见下
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.Label
        /// </seealso>
        private Label label_Renamed;

        /// <summary>
        ///     线条样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        private LineStyle lineStyle_Renamed;

        public LineStyle LineStyle
        {
            set { lineStyle_Renamed = value; }
            get { return lineStyle_Renamed; }
        }

        /// <summary>
        ///     力导向图 - 弦样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LinkStyle
        /// </seealso>
        private LinkStyle linkStyle_Renamed;

        public LinkStyle LinkStyle
        {
            set { linkStyle_Renamed = value; }
            get { return linkStyle_Renamed; }
        }

        /// <summary>
        ///     力导向图 - 弦样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.NodeStyle
        /// </seealso>
        private NodeStyle nodeStyle_Renamed;

        public NodeStyle NodeStyle
        {
            set { nodeStyle_Renamed = value; }
            get { return nodeStyle_Renamed; }
        }

        /// <summary>
        ///     获取label值
        /// </summary>
        public virtual Label Label
        {
            get { return label_Renamed; }
            set { label_Renamed = value; }
        }


        /// <summary>
        ///     获取labelLine值
        /// </summary>
        public virtual LabelLine LabelLine
        {
            get { return labelLine_Renamed; }
            set { labelLine_Renamed = value; }
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }


        /// <summary>
        ///     获取color0值
        /// </summary>
        public virtual string Color0
        {
            get { return color0_Renamed; }
            set { color0_Renamed = value; }
        }

        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string BorderColor
        {
            get { return borderColor_Renamed; }
            set { borderColor_Renamed = value; }
        }


        /// <summary>
        ///     获取borderRadius值
        /// </summary>
        public virtual int? BorderRadius
        {
            get { return borderRadius_Renamed; }
            set { borderRadius_Renamed = value; }
        }


        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? BorderWidth
        {
            get { return borderWidth_Renamed; }
            set { borderWidth_Renamed = value; }
        }


        /// <summary>
        ///     获取barBorderColor值
        /// </summary>
        public virtual string BarBorderColor
        {
            get { return barBorderColor_Renamed; }
            set { barBorderColor_Renamed = value; }
        }


        /// <summary>
        ///     获取barBorderRadius值
        /// </summary>
        public virtual int? BarBorderRadius
        {
            get { return barBorderRadius_Renamed; }
            set { barBorderRadius_Renamed = value; }
        }


        /// <summary>
        ///     获取barBorderWidth值
        /// </summary>
        public virtual int? BarBorderWidth
        {
            get { return barBorderWidth_Renamed; }
            set { barBorderWidth_Renamed = value; }
        }

        /// <summary>
        ///     设置label值     *
        /// </summary>
        /// <param name="label"> </param>
        public virtual T label(Label label)
        {
            label_Renamed = label;
            return this as T;
        }

        /// <summary>
        ///     设置labelLine值     *
        /// </summary>
        /// <param name="labelLine"> </param>
        public virtual T labelLine(LabelLine labelLine)
        {
            labelLine_Renamed = labelLine;
            return this as T;
        }

        /// <summary>
        ///     设置areaStyle值     *
        /// </summary>
        /// <param name="areaStyle"> </param>
        public virtual T areaStyle(AreaStyle areaStyle)
        {
            areaStyle_Renamed = areaStyle;
            return this as T;
        }

        /// <summary>
        ///     设置chordStyle值     *
        /// </summary>
        /// <param name="chordStyle"> </param>
        public virtual T chordStyle(ChordStyle chordStyle)
        {
            chordStyle_Renamed = chordStyle;
            return this as T;
        }

        /// <summary>
        ///     设置nodeStyle值     *
        /// </summary>
        /// <param name="nodeStyle"> </param>
        public virtual T nodeStyle(NodeStyle nodeStyle)
        {
            nodeStyle_Renamed = nodeStyle;
            return this as T;
        }

        /// <summary>
        ///     设置linkStyle值     *
        /// </summary>
        /// <param name="linkStyle"> </param>
        public virtual T linkStyle(LinkStyle linkStyle)
        {
            linkStyle_Renamed = linkStyle;
            return this as T;
        }

        /// <summary>
        ///     标签，饼图默认显示在外部，离饼图距离由labelLine.length决定，地图标签不可指定位罿     * 折线图，柱形图，K线图，散点图可指定position见下
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.Label
        /// </seealso>
        public virtual Label label()
        {
            if (label_Renamed == null)
            {
                label_Renamed = new Label();
            }
            return label_Renamed;
        }

        /// <summary>
        ///     饼图标签视觉引导线，默认显示
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.LabelLine
        /// </seealso>
        public virtual LabelLine labelLine()
        {
            if (labelLine_Renamed == null)
            {
                labelLine_Renamed = new LabelLine();
            }
            return labelLine_Renamed;
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual T color(string color)
        {
            color_Renamed = color;
            return this as T;
        }

        /// <summary>
        ///     获取color0值
        /// </summary>
        public virtual string color0()
        {
            return color0_Renamed;
        }

        /// <summary>
        ///     设置color0值     *
        /// </summary>
        /// <param name="color0"> </param>
        public virtual T color0(string color0)
        {
            color0_Renamed = color0;
            return this as T;
        }

        /// <summary>
        ///     设置lineStyle值     *
        /// </summary>
        /// <param name="lineStyle"> </param>
        public virtual T lineStyle(LineStyle lineStyle)
        {
            lineStyle_Renamed = lineStyle;
            return this as T;
        }

        /// <summary>
        ///     线条样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LineStyle
        /// </seealso>
        public virtual LineStyle lineStyle()
        {
            if (lineStyle_Renamed == null)
            {
                lineStyle_Renamed = new LineStyle();
            }
            return lineStyle_Renamed;
        }

        /// <summary>
        ///     区域样式
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.AreaStyle
        /// </seealso>
        public virtual AreaStyle areaStyle()
        {
            if (areaStyle_Renamed == null)
            {
                areaStyle_Renamed = new AreaStyle();
            }
            return areaStyle_Renamed;
        }

        /// <summary>
        ///     和弦囿- 弦样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.ChordStyle
        /// </seealso>
        public virtual ChordStyle chordStyle()
        {
            if (chordStyle_Renamed == null)
            {
                chordStyle_Renamed = new ChordStyle();
            }
            return chordStyle_Renamed;
        }

        /// <summary>
        ///     力导向图 - 弦样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.NodeStyle
        /// </seealso>
        public virtual NodeStyle nodeStyle()
        {
            if (nodeStyle_Renamed == null)
            {
                nodeStyle_Renamed = new NodeStyle();
            }
            return nodeStyle_Renamed;
        }

        /// <summary>
        ///     力导向图 - 弦样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.style.LinkStyle
        /// </seealso>
        public virtual LinkStyle linkStyle()
        {
            if (linkStyle_Renamed == null)
            {
                linkStyle_Renamed = new LinkStyle();
            }
            return linkStyle_Renamed;
        }

        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string borderColor()
        {
            return borderColor_Renamed;
        }

        /// <summary>
        ///     设置borderColor值     *
        /// </summary>
        /// <param name="borderColor"> </param>
        public virtual T borderColor(string borderColor)
        {
            borderColor_Renamed = borderColor;
            return this as T;
        }

        /// <summary>
        ///     获取borderRadius值
        /// </summary>
        public virtual int? borderRadius()
        {
            return borderRadius_Renamed;
        }

        /// <summary>
        ///     设置borderRadius值     *
        /// </summary>
        /// <param name="borderRadius"> </param>
        public virtual T borderRadius(int? borderRadius)
        {
            borderRadius_Renamed = borderRadius;
            return this as T;
        }

        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? borderWidth()
        {
            return borderWidth_Renamed;
        }

        /// <summary>
        ///     设置borderWidth值     *
        /// </summary>
        /// <param name="borderWidth"> </param>
        public virtual T borderWidth(int? borderWidth)
        {
            borderWidth_Renamed = borderWidth;
            return this as T;
        }

        /// <summary>
        ///     获取barBorderColor值
        /// </summary>
        public virtual string barBorderColor()
        {
            return barBorderColor_Renamed;
        }

        /// <summary>
        ///     设置barBorderColor值     *
        /// </summary>
        /// <param name="barBorderColor"> </param>
        public virtual T barBorderColor(string barBorderColor)
        {
            barBorderColor_Renamed = barBorderColor;
            return this as T;
        }

        /// <summary>
        ///     获取barBorderRadius值
        /// </summary>
        public virtual int? barBorderRadius()
        {
            return barBorderRadius_Renamed;
        }

        /// <summary>
        ///     设置barBorderRadius值     *
        /// </summary>
        /// <param name="barBorderRadius"> </param>
        public virtual T barBorderRadius(int? barBorderRadius)
        {
            barBorderRadius_Renamed = barBorderRadius;
            return this as T;
        }

        /// <summary>
        ///     获取barBorderWidth值
        /// </summary>
        public virtual int? barBorderWidth()
        {
            return barBorderWidth_Renamed;
        }

        /// <summary>
        ///     设置barBorderWidth值     *
        /// </summary>
        /// <param name="barBorderWidth"> </param>
        public virtual T barBorderWidth(int? barBorderWidth)
        {
            barBorderWidth_Renamed = barBorderWidth;
            return this as T;
        }


        /// <summary>
        ///     获取areaStyle值
        /// </summary>
        public virtual AreaStyle getAreaStyle()
        {
            return areaStyle_Renamed;
        }

        /// <summary>
        ///     设置areaStyle值     *
        /// </summary>
        /// <param name="areaStyle"> </param>
        public virtual void setAreaStyle(AreaStyle areaStyle)
        {
            areaStyle_Renamed = areaStyle;
        }

        /// <summary>
        ///     获取chordStyle值
        /// </summary>
        public virtual ChordStyle getChordStyle()
        {
            return chordStyle_Renamed;
        }

        /// <summary>
        ///     设置chordStyle值     *
        /// </summary>
        /// <param name="chordStyle"> </param>
        public virtual void setChordStyle(ChordStyle chordStyle)
        {
            chordStyle_Renamed = chordStyle;
        }

        /// <summary>
        ///     获取nodeStyle值
        /// </summary>
        public virtual NodeStyle getNodeStyle()
        {
            return nodeStyle_Renamed;
        }

        /// <summary>
        ///     设置nodeStyle值     *
        /// </summary>
        /// <param name="nodeStyle"> </param>
        public virtual void setNodeStyle(NodeStyle nodeStyle)
        {
            nodeStyle_Renamed = nodeStyle;
        }

        /// <summary>
        ///     获取linkStyle值
        /// </summary>
        public virtual LinkStyle getLinkStyle()
        {
            return linkStyle_Renamed;
        }

        /// <summary>
        ///     设置linkStyle值     *
        /// </summary>
        /// <param name="linkStyle"> </param>
        public virtual void setLinkStyle(LinkStyle linkStyle)
        {
            linkStyle_Renamed = linkStyle;
        }


        /// <summary>
        ///     获取lineStyle值
        /// </summary>
        public virtual LineStyle getLineStyle()
        {
            return lineStyle_Renamed;
        }

        /// <summary>
        ///     设置lineStyle值     *
        /// </summary>
        /// <param name="lineStyle"> </param>
        public virtual void setLineStyle(LineStyle lineStyle)
        {
            lineStyle_Renamed = lineStyle;
        }
    }
}