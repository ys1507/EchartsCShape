using System;
using Yongjin.Cshape.echarts.code;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.style
{
    /// <summary>
    ///     文字样式
    ///     @author Yongjin.C
    ///     Created by Yongjin.C on 14-8-25.
    /// </summary>
    [Serializable]
    public class TextStyle
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     水平对齐方式，可选为＿left' | 'right' | 'center'
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.X
        /// </seealso>
        private X align_Renamed;

        /// <summary>
        ///     颜色
        /// </summary>
        private string color_Renamed;

        /// <summary>
        ///     修饰，仅对tooltip.textStyle生效
        /// </summary>
        private string decoration_Renamed;

        /// <summary>
        ///     字体系列
        ///     IE8- 字体模糊并且，不支持不同字体混排，额外指定一仿
        /// </summary>
        private string fontFamily2_Renamed;

        /// <summary>
        ///     字体系列
        /// </summary>
        private string fontFamily_Renamed;

        /// <summary>
        ///     字号 ，单位px
        /// </summary>
        private int? fontSize_Renamed;

        /// <summary>
        ///     样式，可选为＿normal' | 'italic' | 'oblique'
        /// </summary>
        private FontStyle fontStyle_Renamed;

        /// <summary>
        ///     粗细，可选为＿normal' | 'bold' | 'bolder' | 'lighter' | 100 | 200 |... | 900
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.FontWeight
        /// </seealso>
        private object fontWeight_Renamed;

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }


        /// <summary>
        ///     获取decoration值
        /// </summary>
        public virtual string Decoration
        {
            get { return decoration_Renamed; }
            set { decoration_Renamed = value; }
        }


        /// <summary>
        ///     获取align值
        /// </summary>
        public virtual X Align
        {
            get { return align_Renamed; }
            set { align_Renamed = value; }
        }


        /// <summary>
        ///     获取fontSize值
        /// </summary>
        public virtual int? FontSize
        {
            get { return fontSize_Renamed; }
            set { fontSize_Renamed = value; }
        }


        /// <summary>
        ///     获取fontFamily值
        /// </summary>
        public virtual string FontFamily
        {
            get { return fontFamily_Renamed; }
            set { fontFamily_Renamed = value; }
        }


        /// <summary>
        ///     获取fontFamily2值
        /// </summary>
        public virtual string FontFamily2
        {
            get { return fontFamily2_Renamed; }
            set { fontFamily2_Renamed = value; }
        }


        /// <summary>
        ///     获取fontStyle值
        /// </summary>
        public virtual FontStyle FontStyle
        {
            get { return fontStyle_Renamed; }
            set { fontStyle_Renamed = value; }
        }


        /// <summary>
        ///     获取fontWeight值
        /// </summary>
        public virtual object FontWeight
        {
            get { return fontWeight_Renamed; }
            set { fontWeight_Renamed = value; }
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual string color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual TextStyle color(string color)
        {
            color_Renamed = color;
            return this;
        }

        /// <summary>
        ///     获取decoration值
        /// </summary>
        public virtual string decoration()
        {
            return decoration_Renamed;
        }

        /// <summary>
        ///     设置decoration值     *
        /// </summary>
        /// <param name="decoration"> </param>
        public virtual TextStyle decoration(string decoration)
        {
            decoration_Renamed = decoration;
            return this;
        }

        /// <summary>
        ///     获取align值
        /// </summary>
        public virtual X align()
        {
            return align_Renamed;
        }

        /// <summary>
        ///     设置align值     *
        /// </summary>
        /// <param name="align"> </param>
        public virtual TextStyle align(X align)
        {
            align_Renamed = align;
            return this;
        }

        /// <summary>
        ///     获取fontSize值
        /// </summary>
        public virtual int? fontSize()
        {
            return fontSize_Renamed;
        }

        /// <summary>
        ///     设置fontSize值     *
        /// </summary>
        /// <param name="fontSize"> </param>
        public virtual TextStyle fontSize(int? fontSize)
        {
            fontSize_Renamed = fontSize;
            return this;
        }

        /// <summary>
        ///     获取fontFamily值
        /// </summary>
        public virtual string fontFamily()
        {
            return fontFamily_Renamed;
        }

        /// <summary>
        ///     设置fontFamily值     *
        /// </summary>
        /// <param name="fontFamily"> </param>
        public virtual TextStyle fontFamily(string fontFamily)
        {
            fontFamily_Renamed = fontFamily;
            return this;
        }

        /// <summary>
        ///     获取fontFamily2值
        /// </summary>
        public virtual string fontFamily2()
        {
            return fontFamily2_Renamed;
        }

        /// <summary>
        ///     设置fontFamily2值     *
        /// </summary>
        /// <param name="fontFamily2"> </param>
        public virtual TextStyle fontFamily2(string fontFamily2)
        {
            fontFamily2_Renamed = fontFamily2;
            return this;
        }

        /// <summary>
        ///     获取fontStyle值
        /// </summary>
        public virtual FontStyle fontStyle()
        {
            return fontStyle_Renamed;
        }

        /// <summary>
        ///     设置fontStyle值     *
        /// </summary>
        /// <param name="fontStyle"> </param>
        public virtual TextStyle fontStyle(FontStyle fontStyle)
        {
            fontStyle_Renamed = fontStyle;
            return this;
        }

        /// <summary>
        ///     获取fontWeight值
        /// </summary>
        public virtual object fontWeight()
        {
            return fontWeight_Renamed;
        }

        /// <summary>
        ///     设置fontWeight值     *
        /// </summary>
        /// <param name="fontWeight"> </param>
        public virtual TextStyle fontWeight(object fontWeight)
        {
            fontWeight_Renamed = fontWeight;
            return this;
        }
    }
}