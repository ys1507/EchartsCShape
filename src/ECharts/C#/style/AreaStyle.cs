using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.style
{
    /// <summary>
    ///     区域填充样式
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class AreaStyle
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     颜色
        /// </summary>
        private object color_Renamed;

        /// <summary>
        ///     填充样式，目前仅支持'default'(实填兿
        /// </summary>
        private object type_Renamed;

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual object Color
        {
            get { return color_Renamed; }
            set { color_Renamed = value; }
        }


        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual object Type
        {
            get { return type_Renamed; }
            set { type_Renamed = value; }
        }

        /// <summary>
        ///     获取color值
        /// </summary>
        public virtual object color()
        {
            return color_Renamed;
        }

        /// <summary>
        ///     设置color值     *
        /// </summary>
        /// <param name="color"> </param>
        public virtual AreaStyle color(object color)
        {
            color_Renamed = color;
            return this;
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual object type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual AreaStyle type(object type)
        {
            type_Renamed = type;
            return this;
        }

        /// <summary>
        ///     获取typeDefault值
        /// </summary>
        public virtual AreaStyle typeDefault()
        {
            type_Renamed = "default";
            return this;
        }
    }
}