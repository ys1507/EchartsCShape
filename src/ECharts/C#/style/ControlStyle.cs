using System;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts.style
{
    /// <summary>
    ///     时间轴控制器样式
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public class ControlStyle
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     高亮
        /// </summary>
        private Color emphasis_Renamed;

        /// <summary>
        ///     正常
        /// </summary>
        private Color normal_Renamed;

        /// <summary>
        ///     获取normal值
        /// </summary>
        public virtual Color normal()
        {
            if (normal_Renamed == null)
            {
                normal_Renamed = new Color(this);
            }
            return normal_Renamed;
        }

        /// <summary>
        ///     设置normal值     *
        /// </summary>
        /// <param name="normal"> </param>
        public virtual ControlStyle normal(Color normal)
        {
            normal_Renamed = normal;
            return this;
        }

        /// <summary>
        ///     获取emphasis值
        /// </summary>
        public virtual Color emphasis()
        {
            if (emphasis_Renamed == null)
            {
                emphasis_Renamed = new Color(this);
            }
            return emphasis_Renamed;
        }

        /// <summary>
        ///     设置emphasis值     *
        /// </summary>
        /// <param name="emphasis"> </param>
        public virtual ControlStyle emphasis(Color emphasis)
        {
            emphasis_Renamed = emphasis;
            return this;
        }

        /// <summary>
        ///     获取normal值
        /// </summary>
        public virtual Color getNormal()
        {
            return normal_Renamed;
        }

        /// <summary>
        ///     设置normal值     *
        /// </summary>
        /// <param name="normal"> </param>
        public virtual void setNormal(Color normal)
        {
            normal_Renamed = normal;
        }

        /// <summary>
        ///     获取emphasis值
        /// </summary>
        public virtual Color getEmphasis()
        {
            return emphasis_Renamed;
        }

        /// <summary>
        ///     设置emphasis值     *
        /// </summary>
        /// <param name="emphasis"> </param>
        public virtual void setEmphasis(Color emphasis)
        {
            emphasis_Renamed = emphasis;
        }

        public class Color
        {
            private readonly ControlStyle outerInstance;

            /// <summary>
            ///     时间轴控制器样式颜色
            /// </summary>
            internal string color_Renamed;

            public Color(ControlStyle outerInstance)
            {
                this.outerInstance = outerInstance;
            }

            /// <summary>
            ///     获取color值
            /// </summary>
            public virtual string color()
            {
                return color_Renamed;
            }

            /// <summary>
            ///     设置color值         *
            /// </summary>
            /// <param name="color"> </param>
            public virtual Color color(string color)
            {
                color_Renamed = color;
                return this;
            }

            /**
         * 获取color值
         */

            public String getColor()
            {
                return color_Renamed;
            }

            /**
             * 设置color值
             *
             * @param color
             */

            public void setColor(String color)
            {
                color_Renamed = color;
            }
        }
    }
}