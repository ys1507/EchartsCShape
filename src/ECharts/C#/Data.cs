namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     @author Yongjin.C
    /// </summary>
    public interface Data<T>
    {
        /// <summary>
        ///     添加元素
        /// </summary>
        /// <param name="values">
        ///     @return
        /// </param>
        T data(params object[] values);
    }
}