using System;
using Yongjin.Cshape.echarts.code;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     组件的基础籿 *
    ///     @author Yongjin.C
    /// </summary>
    [Serializable]
    public abstract class Basic<T> where T : class
    {
        private const long serialVersionUID = 1L;

        /// <summary>
        ///     标题背景颜色，默认透明
        /// </summary>
        private string backgroundColor_Renamed;

        /// <summary>
        ///     标题边框颜色
        /// </summary>
        private string borderColor_Renamed;

        /// <summary>
        ///     borderWidth
        /// </summary>
        private int? borderWidth_Renamed;

        /// <summary>
        ///     主副标题纵向间隔，单位px，默认为10
        /// </summary>
        private int? itemGap_Renamed;

        /// <summary>
        ///     标题内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距，同css，见下图
        /// </summary>
        private object padding_Renamed;

        /// <summary>
        ///     是否显示
        /// </summary>
        private bool? show_Renamed;

        /// <summary>
        ///     水平安放位置，默认为左侧，可选为＿center' | 'left' | 'right' | {number}（x坐标，单位px＿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.X
        /// </seealso>
        private object x_Renamed;

        /// <summary>
        ///     垂直安放位置，默认为全图顶端，可选为＿top' | 'bottom' | 'center' | {number}（y坐标，单位px＿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.code.Y
        /// </seealso>
        private object y_Renamed;

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? Show
        {
            get { return show_Renamed; }
            set { show_Renamed = value; }
        }


        /// <summary>
        ///     获取x值
        /// </summary>
        public virtual object X
        {
            get { return x_Renamed; }
            set { x_Renamed = value; }
        }


        /// <summary>
        ///     获取y值
        /// </summary>
        public virtual object Y
        {
            get { return y_Renamed; }
            set { y_Renamed = value; }
        }


        /// <summary>
        ///     获取backgroundColor值
        /// </summary>
        public virtual string BackgroundColor
        {
            get { return backgroundColor_Renamed; }
            set { backgroundColor_Renamed = value; }
        }


        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string BorderColor
        {
            get { return borderColor_Renamed; }
            set { borderColor_Renamed = value; }
        }


        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? BorderWidth
        {
            get { return borderWidth_Renamed; }
            set { borderWidth_Renamed = value; }
        }

        /// <summary>
        ///     获取itemGap值
        /// </summary>
        public virtual int? ItemGap
        {
            get { return itemGap_Renamed; }
            set { itemGap_Renamed = value; }
        }

        /// <summary>
        ///     获取show值
        /// </summary>
        public virtual bool? show()
        {
            return show_Renamed;
        }

        /// <summary>
        ///     设置show值     *
        /// </summary>
        /// <param name="show"> </param>
        public virtual T show(bool? show)
        {
            show_Renamed = show;
            return this as T;
        }

        /// <summary>
        ///     获取x值
        /// </summary>
        public virtual object x()
        {
            return x_Renamed;
        }

        /// <summary>
        ///     设置x值     *
        /// </summary>
        /// <param name="x"> </param>
        public virtual T x(object x)
        {
            x_Renamed = x;
            return this as T;
        }

        /// <summary>
        ///     获取y值
        /// </summary>
        public virtual object y()
        {
            return y_Renamed;
        }

        /// <summary>
        ///     设置y值     *
        /// </summary>
        /// <param name="y"> </param>
        public virtual T y(object y)
        {
            y_Renamed = y;
            return this as T;
        }

        /// <summary>
        ///     获取backgroundColor值
        /// </summary>
        public virtual string backgroundColor()
        {
            return backgroundColor_Renamed;
        }

        /// <summary>
        ///     设置backgroundColor值     *
        /// </summary>
        /// <param name="backgroundColor"> </param>
        public virtual T backgroundColor(string backgroundColor)
        {
            backgroundColor_Renamed = backgroundColor;
            return this as T;
        }

        /// <summary>
        ///     获取borderColor值
        /// </summary>
        public virtual string borderColor()
        {
            return borderColor_Renamed;
        }

        /// <summary>
        ///     设置borderColor值     *
        /// </summary>
        /// <param name="borderColor"> </param>
        public virtual T borderColor(string borderColor)
        {
            borderColor_Renamed = borderColor;
            return this as T;
        }

        /// <summary>
        ///     获取borderWidth值
        /// </summary>
        public virtual int? borderWidth()
        {
            return borderWidth_Renamed;
        }

        /// <summary>
        ///     设置borderWidth值     *
        /// </summary>
        /// <param name="borderWidth"> </param>
        public virtual T borderWidth(int? borderWidth)
        {
            borderWidth_Renamed = borderWidth;
            return this as T;
        }

        /// <summary>
        ///     获取padding值
        /// </summary>
        public virtual object padding()
        {
            return padding_Renamed;
        }

        /// <summary>
        ///     设置padding值     *
        /// </summary>
        /// <param name="padding"> </param>
        public virtual T padding(int? padding)
        {
            padding_Renamed = padding;
            return this as T;
        }

        /// <summary>
        ///     设置padding值     *
        /// </summary>
        /// <param name="padding"> </param>
        public virtual T padding(params int?[] padding)
        {
            if (padding != null && padding.Length > 4)
            {
                throw new Exception("padding属性最多可以接政个参敿");
            }
            padding_Renamed = padding;
            return this as T;
        }

        /// <summary>
        ///     获取itemGap值
        /// </summary>
        public virtual int? itemGap()
        {
            return itemGap_Renamed;
        }

        /// <summary>
        ///     设置itemGap值     *
        /// </summary>
        /// <param name="itemGap"> </param>
        public virtual T itemGap(int? itemGap)
        {
            itemGap_Renamed = itemGap;
            return this as T;
        }

        /// <summary>
        ///     设置x值     *
        /// </summary>
        /// <param name="x"> </param>
        public virtual T x(X x)
        {
            x_Renamed = x;
            return this as T;
        }

        /// <summary>
        ///     设置y值     *
        /// </summary>
        /// <param name="y"> </param>
        public virtual T y(Y y)
        {
            y_Renamed = y;
            return this as T;
        }


        /// <summary>
        ///     获取padding值
        /// </summary>
        public virtual object getPadding()
        {
            return padding_Renamed;
        }

        /// <summary>
        ///     设置padding值     *
        /// </summary>
        /// <param name="padding"> </param>
        public virtual void setPadding(int? padding)
        {
            padding_Renamed = padding;
        }


        public virtual void setPadding(object padding)
        {
            padding_Renamed = padding;
        }
    }
}