/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using System.Collections.Generic;
using System.Linq;
using Yongjin.Cshape.echarts.axis;
using Yongjin.Cshape.echarts.code;
using Yongjin.Cshape.echarts.style;

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     极坐栿 *
    ///     @author Yongjin.C
    /// </summary>
    public class Polar : AbstractData<Polar>, Component
    {
        /// <summary>
        ///     坐标轴文本标签，详见axis.axisLabel
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisLabel
        /// </seealso>
        private AxisLabel axisLabel_Renamed;

        /// <summary>
        ///     坐标轴线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisLine
        /// </seealso>
        private AxisLine axisLine_Renamed;

        /// <summary>
        ///     数值轴两端空白策略，数组内数值代表百分比，[原始数据最小值与最终最小值之间的差额，原始数据最大值与最终最大值之间的差额]
        /// </summary>
        private object[] boundaryGap_Renamed;

        /// <summary>
        ///     圆心坐标，支持绝对值（px）和百分比，百分比计算min(width, height) * 50%
        /// </summary>
        private object[] center_Renamed;

        /// <summary>
        ///     雷达指标列表，同时也是label内容
        /// </summary>
        private List<object> indicator_Renamed;

        /// <summary>
        ///     坐标轴名秿
        /// </summary>
        private Name name_Renamed;

        /// <summary>
        ///     整数精度，默认为100，个位和百位丿
        /// </summary>
        private int? power_Renamed;

        /// <summary>
        ///     小数精度，默认为0，无小数炿
        /// </summary>
        private int? precision_Renamed;

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        private object radius_Renamed;

        /// <summary>
        ///     脱离0值比例，放大聚焦到最终_min，_max区间
        /// </summary>
        private bool? scale_Renamed;

        /// <summary>
        ///     分隔区域，默认不显示，属性show控制显示与否，属性areaStyle（详见areaStyle）控制区域样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.SplitArea
        /// </seealso>
        private SplitArea splitArea_Renamed;

        /// <summary>
        ///     分隔线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.SplitLine
        /// </seealso>
        private SplitLine splitLine_Renamed;

        /// <summary>
        ///     分割段数，默认为5
        /// </summary>
        private int? splitNumber_Renamed;

        /// <summary>
        ///     开始角庿 有效输入范围：[-180,180]
        /// </summary>
        private int? startAngle_Renamed;

        /// <summary>
        ///     极坐标的形状＿polygon'|'circle' 多边形|圆形
        /// </summary>
        private PolarType type_Renamed;

        /// <summary>
        ///     获取boundaryGap值
        /// </summary>
        public virtual object[] BoundaryGap
        {
            get { return boundaryGap_Renamed; }
            set { boundaryGap_Renamed = value; }
        }


        /// <summary>
        ///     获取axisLine值
        /// </summary>
        public virtual AxisLine AxisLine
        {
            get { return axisLine_Renamed; }
            set { axisLine_Renamed = value; }
        }


        /// <summary>
        ///     获取axisLabel值
        /// </summary>
        public virtual AxisLabel AxisLabel
        {
            get { return axisLabel_Renamed; }
            set { axisLabel_Renamed = value; }
        }


        /// <summary>
        ///     获取splitArea值
        /// </summary>
        public virtual SplitArea SplitArea
        {
            get { return splitArea_Renamed; }
            set { splitArea_Renamed = value; }
        }


        /// <summary>
        ///     获取splitLine值
        /// </summary>
        public virtual SplitLine SplitLine
        {
            get { return splitLine_Renamed; }
            set { splitLine_Renamed = value; }
        }


        /// <summary>
        ///     获取indicator值
        /// </summary>
        public virtual List<object> Indicator
        {
            get { return indicator_Renamed; }
            set { indicator_Renamed = value; }
        }


        /// <summary>
        ///     获取center值
        /// </summary>
        public virtual object[] Center
        {
            get { return center_Renamed; }
            set { center_Renamed = value; }
        }


        /// <summary>
        ///     获取radius值
        /// </summary>
        public virtual object Radius
        {
            get { return radius_Renamed; }
            set { radius_Renamed = value; }
        }


        /// <summary>
        ///     获取startAngle值
        /// </summary>
        public virtual int? StartAngle
        {
            get { return startAngle_Renamed; }
            set { startAngle_Renamed = value; }
        }


        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? SplitNumber
        {
            get { return splitNumber_Renamed; }
            set { splitNumber_Renamed = value; }
        }


        /// <summary>
        ///     获取scale值
        /// </summary>
        public virtual bool? Scale
        {
            get { return scale_Renamed; }
            set { scale_Renamed = value; }
        }


        /// <summary>
        ///     获取precision值
        /// </summary>
        public virtual int? Precision
        {
            get { return precision_Renamed; }
            set { precision_Renamed = value; }
        }


        /// <summary>
        ///     获取power值
        /// </summary>
        public virtual int? Power
        {
            get { return power_Renamed; }
            set { power_Renamed = value; }
        }


        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual PolarType Type
        {
            get { return type_Renamed; }
            set { type_Renamed = value; }
        }

        /// <summary>
        ///     获取center值
        /// </summary>
        public virtual object[] center()
        {
            return center_Renamed;
        }

        /// <summary>
        ///     设置center值     *
        /// </summary>
        /// <param name="center"> </param>
        public virtual Polar center(object[] center)
        {
            center_Renamed = center;
            return this;
        }

        /// <summary>
        ///     设置values值     *
        /// </summary>
        /// <param name="values"> </param>
        public virtual Polar indicator(params object[] values)
        {
            if (values == null || values.Length == 0)
            {
                return this;
            }
            indicator().AddRange(values.ToList());
            return this;
        }

        /// <summary>
        ///     获取radius值
        /// </summary>
        public virtual object radius()
        {
            return radius_Renamed;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual Polar name(Name name)
        {
            name_Renamed = name;
            return this;
        }

        /// <summary>
        ///     获取boundaryGap值
        /// </summary>
        public virtual object[] boundaryGap()
        {
            return boundaryGap_Renamed;
        }

        /// <summary>
        ///     设置boundaryGap值     *
        /// </summary>
        /// <param name="boundaryGap"> </param>
        public virtual Polar boundaryGap(object[] boundaryGap)
        {
            boundaryGap_Renamed = boundaryGap;
            return this;
        }

        /// <summary>
        ///     设置axisLine值     *
        /// </summary>
        /// <param name="axisLine"> </param>
        public virtual Polar axisLine(AxisLine axisLine)
        {
            axisLine_Renamed = axisLine;
            return this;
        }

        /// <summary>
        ///     设置axisLabel值     *
        /// </summary>
        /// <param name="axisLabel"> </param>
        public virtual Polar axisLabel(AxisLabel axisLabel)
        {
            axisLabel_Renamed = axisLabel;
            return this;
        }

        /// <summary>
        ///     设置splitArea值     *
        /// </summary>
        /// <param name="splitArea"> </param>
        public virtual Polar splitArea(SplitArea splitArea)
        {
            splitArea_Renamed = splitArea;
            return this;
        }

        /// <summary>
        ///     设置splitLine值     *
        /// </summary>
        /// <param name="splitLine"> </param>
        public virtual Polar splitLine(SplitLine splitLine)
        {
            splitLine_Renamed = splitLine;
            return this;
        }

        /// <summary>
        ///     设置indicator值     *
        /// </summary>
        /// <param name="indicator"> </param>
        public virtual Polar indicator(List<object> indicator)
        {
            indicator_Renamed = indicator;
            return this;
        }

        /// <summary>
        ///     圆心坐标，支持绝对值（px）和百分比，百分比计算min(width, height) * 50%
        /// </summary>
        /// <param name="width"> </param>
        /// <param name="height">
        ///     @return
        /// </param>
        public virtual Polar center(object width, object height)
        {
            center_Renamed = new[] {width, height};
            return this;
        }

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        /// <param name="value">
        ///     @return
        /// </param>
        public virtual Polar radius(object value)
        {
            radius_Renamed = value;
            return this;
        }

        /// <summary>
        ///     半径，支持绝对值（px）和百分比，百分比计算比，min(width, height) / 2 * 75%＿     * 传数组实现环形图，[内半径，外半径]
        /// </summary>
        /// <param name="width"> </param>
        /// <param name="height">
        ///     @return
        /// </param>
        public virtual Polar radius(object width, object height)
        {
            radius_Renamed = new[] {width, height};
            return this;
        }

        /// <summary>
        ///     获取startAngle值
        /// </summary>
        public virtual int? startAngle()
        {
            return startAngle_Renamed;
        }

        /// <summary>
        ///     设置startAngle值     *
        /// </summary>
        /// <param name="startAngle"> </param>
        public virtual Polar startAngle(int? startAngle)
        {
            startAngle_Renamed = startAngle;
            return this;
        }

        /// <summary>
        ///     获取splitNumber值
        /// </summary>
        public virtual int? splitNumber()
        {
            return splitNumber_Renamed;
        }

        /// <summary>
        ///     设置splitNumber值     *
        /// </summary>
        /// <param name="splitNumber"> </param>
        public virtual Polar splitNumber(int? splitNumber)
        {
            splitNumber_Renamed = splitNumber;
            return this;
        }

        /// <summary>
        ///     坐标轴名秿
        /// </summary>
        public virtual Name name()
        {
            if (name_Renamed == null)
            {
                name_Renamed = new Name();
            }
            return name_Renamed;
        }

        /// <summary>
        ///     数值轴两端空白策略，数组内数值代表百分比，[原始数据最小值与最终最小值之间的差额，原始数据最大值与最终最大值之间的差额]
        /// </summary>
        public virtual Polar boundaryGap(object min, object max)
        {
            boundaryGap_Renamed = new[] {min, max};
            return this;
        }

        /// <summary>
        ///     获取scale值
        /// </summary>
        public virtual bool? scale()
        {
            return scale_Renamed;
        }

        /// <summary>
        ///     设置scale值     *
        /// </summary>
        /// <param name="scale"> </param>
        public virtual Polar scale(bool? scale)
        {
            scale_Renamed = scale;
            return this;
        }

        /// <summary>
        ///     获取precision值
        /// </summary>
        public virtual int? precision()
        {
            return precision_Renamed;
        }

        /// <summary>
        ///     设置precision值     *
        /// </summary>
        /// <param name="precision"> </param>
        public virtual Polar precision(int? precision)
        {
            precision_Renamed = precision;
            return this;
        }

        /// <summary>
        ///     获取power值
        /// </summary>
        public virtual int? power()
        {
            return power_Renamed;
        }

        /// <summary>
        ///     设置power值     *
        /// </summary>
        /// <param name="power"> </param>
        public virtual Polar power(int? power)
        {
            power_Renamed = power;
            return this;
        }

        /// <summary>
        ///     坐标轴线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisLine
        /// </seealso>
        public virtual AxisLine axisLine()
        {
            if (axisLine_Renamed == null)
            {
                axisLine_Renamed = new AxisLine();
            }
            return axisLine_Renamed;
        }

        /// <summary>
        ///     坐标轴文本标签，详见axis.axisLabel
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.AxisLabel
        /// </seealso>
        public virtual AxisLabel axisLabel()
        {
            if (axisLabel_Renamed == null)
            {
                axisLabel_Renamed = new AxisLabel();
            }
            return axisLabel_Renamed;
        }

        /// <summary>
        ///     分隔区域，默认不显示，属性show控制显示与否，属性areaStyle（详见areaStyle）控制区域样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.SplitArea
        /// </seealso>
        public virtual SplitArea splitArea()
        {
            if (splitArea_Renamed == null)
            {
                splitArea_Renamed = new SplitArea();
            }
            return splitArea_Renamed;
        }

        /// <summary>
        ///     分隔线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样弿     *
        /// </summary>
        /// <seealso cref= Yongjin.Cshape.echarts.axis.SplitLine
        /// </seealso>
        public virtual SplitLine splitLine()
        {
            if (splitLine_Renamed == null)
            {
                splitLine_Renamed = new SplitLine();
            }
            return splitLine_Renamed;
        }

        /// <summary>
        ///     获取type值
        /// </summary>
        public virtual PolarType type()
        {
            return type_Renamed;
        }

        /// <summary>
        ///     设置type值     *
        /// </summary>
        /// <param name="type"> </param>
        public virtual Polar type(PolarType type)
        {
            type_Renamed = type;
            return this;
        }

        /// <summary>
        ///     雷达指标列表，同时也是label内容
        /// </summary>
        public virtual List<object> indicator()
        {
            if (indicator_Renamed == null)
            {
                indicator_Renamed = new List<object>();
            }
            return indicator_Renamed;
        }

        /// <summary>
        ///     获取name值
        /// </summary>
        public virtual Name getName()
        {
            return name_Renamed;
        }

        /// <summary>
        ///     设置name值     *
        /// </summary>
        /// <param name="name"> </param>
        public virtual void setName(Name name)
        {
            name_Renamed = name;
        }


        public class Name
        {
            internal bool? show_Renamed;
            internal TextStyle textStyle_Renamed;

            /// <summary>
            ///     构造函敿
            /// </summary>
            public Name()
            {
                show(true);
                textStyle(new TextStyle());
                textStyle_Renamed.color("#333");
            }

            /// <summary>
            ///     获取show值
            /// </summary>
            public virtual bool? Show
            {
                get { return show_Renamed; }
                set { show_Renamed = value; }
            }


            /// <summary>
            ///     获取textStyle值
            /// </summary>
            public virtual TextStyle TextStyle
            {
                get { return textStyle_Renamed; }
                set { textStyle_Renamed = value; }
            }

            /// <summary>
            ///     获取show值
            /// </summary>
            public virtual bool? show()
            {
                return show_Renamed;
            }

            /// <summary>
            ///     设置show值         *
            /// </summary>
            /// <param name="show"> </param>
            public virtual Name show(bool? show)
            {
                show_Renamed = show;
                return this;
            }

            /// <summary>
            ///     获取textStyle值
            /// </summary>
            public virtual TextStyle textStyle()
            {
                if (textStyle_Renamed == null)
                {
                    textStyle_Renamed = new TextStyle();
                }
                return textStyle_Renamed;
            }

            /// <summary>
            ///     设置textStyle值         *
            /// </summary>
            /// <param name="textStyle"> </param>
            public virtual Name textStyle(TextStyle textStyle)
            {
                textStyle_Renamed = textStyle;
                return this;
            }
        }
    }
}