/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 abel533@gmail.com by  Yongjin.C,995292291@qq.com alter@C#
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Yongjin.Cshape.echarts
{
    /// <summary>
    ///     直角坐标系内绘图网格
    ///     @author Yongjin.C
    /// </summary>
    public class Grid : Basic<Grid>, Component
    {
        /// <summary>
        ///     直角坐标系内绘图网格（不含坐标轴）高度，默认为总宽庿- y - y2，数值单位px，指定height后将忽略y2，见下图?     * 支持百分比（字符串），如'50%'(显示区域一半的高度)
        /// </summary>
        private object height_Renamed;

        /// <summary>
        ///     直角坐标系内绘图网格（不含坐标轴）宽度，默认为总宽庿- x - x2，数值单位px，指定width后将忽略x2，见下图?     * 支持百分比（字符串），如'50%'(显示区域一半的宽度)
        /// </summary>
        private object width_Renamed;

        /// <summary>
        ///     直角坐标系内绘图网格左上角横坐标，数值单位px，支持百分比（字符串），妿50%'(显示区域横向中心)
        /// </summary>
        private object x2_Renamed;

        /// <summary>
        ///     直角坐标系内绘图网格左上角纵坐标，数值单位px，支持百分比（字符串），妿50%'(显示区域纵向中心)
        /// </summary>
        private object y2_Renamed;

        /// <summary>
        ///     获取x2值
        /// </summary>
        public virtual object X2
        {
            get { return x2_Renamed; }
            set { x2_Renamed = value; }
        }


        /// <summary>
        ///     获取y2值
        /// </summary>
        public virtual object Y2
        {
            get { return y2_Renamed; }
            set { y2_Renamed = value; }
        }


        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object Width
        {
            get { return width_Renamed; }
            set { width_Renamed = value; }
        }


        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual object Height
        {
            get { return height_Renamed; }
            set { height_Renamed = value; }
        }

        /// <summary>
        ///     获取x2值
        /// </summary>
        public virtual object x2()
        {
            return x2_Renamed;
        }

        /// <summary>
        ///     设置x2值     *
        /// </summary>
        /// <param name="x2"> </param>
        public virtual Grid x2(object x2)
        {
            x2_Renamed = x2;
            return this;
        }

        /// <summary>
        ///     获取y2值
        /// </summary>
        public virtual object y2()
        {
            return y2_Renamed;
        }

        /// <summary>
        ///     设置y2值     *
        /// </summary>
        /// <param name="y2"> </param>
        public virtual Grid y2(object y2)
        {
            y2_Renamed = y2;
            return this;
        }

        /// <summary>
        ///     获取width值
        /// </summary>
        public virtual object width()
        {
            return width_Renamed;
        }

        /// <summary>
        ///     设置width值     *
        /// </summary>
        /// <param name="width"> </param>
        public virtual Grid width(object width)
        {
            width_Renamed = width;
            return this;
        }

        /// <summary>
        ///     获取height值
        /// </summary>
        public virtual object height()
        {
            return height_Renamed;
        }

        /// <summary>
        ///     设置height值     *
        /// </summary>
        /// <param name="height"> </param>
        public virtual Grid height(object height)
        {
            height_Renamed = height;
            return this;
        }
    }
}